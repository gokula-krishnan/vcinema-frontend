import axios from 'axios';

export default axios.create({
    baseURL: 'http://52.74.62.158/api/v1',
    // baseURL: 'http://api.staging.vcinema.in/api/v1',
    // middleURL : 'http://localhost:3020/api/',
    headers: {
        'Content-type': 'application/json'
    }
})

