import axios from 'axios';

export default axios.create({
    baseURL: 'http://localhost:3020/api/',
    headers: {
        'Content-type': 'application/json'
    }
})
