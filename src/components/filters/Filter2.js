import './filters.scss';
import React from 'react';
import {connect} from 'react-redux';
import { Form, Button, Row, Col, ToggleButtonGroup, ToggleButton, Dropdown, DropdownButton } from 'react-bootstrap';
import { Search, Reload } from '../../svgIcons';
import Nouislider from 'react-nouislider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';
import Chips, { Chip } from 'react-chips';
import base from '../../api/base';
import {ccOptions } from '../../actions';

class MoviesFilter extends React.Component {
    state = {
        // suggestionsArray: null,
        profession: [],
        genre: [],
        zodiac: [],
        indus: [],
        stats: 'STATS',
        social: "SOCIAL",
        age: [0, 100],
        year: [1930, 2019],
        checkbox: [],
        expanded: false,

        filteractive: false
    }
    componentDidMount() {
        this.props.ccOptions()
        .then(res => this.setState({ profession: res.data.artistProfessions }))
    }
    filterActivated = () => {
        let { genre, age, year, zodiac, indus, stats, social, checkbox,filteractive } = this.state;
        //console.log(genre.length)
        if(!filteractive){
            this.setState({ filteractive: true });
        }
        else if(filteractive && (!genre.length) && (!zodiac.length) && (!indus.length) && (!checkbox.length) && (age[0] === 0) && (age[1] === 100) && (year[0] === 1930) && (year[1] === 2019)){
            // && !zodiac.length && !indus.length && !checkbox.length && (age[0] !== 0) && (age[1] !== 100) && (year[0] !== 1930) && (year[1] !== 2019)
            //console.log('di')
            this.setState({ filteractive: false });
        }
        setTimeout(() => {
            this.props.checkItemsLength(this.state);
        }, 500)
    }

    onChangeSlideRating = (age) => {
        this.setState({ age });      
        this.filterActivated();  
    }
    onChangeSlideYear = (year) => {
        this.setState({ year });   
        this.filterActivated();
    }
    handleChange = (genre) => {
        //console.log(genre[genre.length - 1])
        if(genre[genre.length - 1] === "More"){
           this.setState({ expanded : !this.state.expanded})
        } else {
            this.setState({ genre });
            this.filterActivated();
        }
    }
   
    selectChange = (zodiac) => {
        let val = zodiac;
        this.setState(({zodiac}) => {
            let newArry = (zodiac.includes(val) ? zodiac.filter( item => item !== val) : [...zodiac, val]);
            return { zodiac: newArry}
        })
        this.filterActivated();
    }
    indusChange = (ind) => {
        this.setState(({indus}) => {
            let newArry = (indus.includes(ind) ? indus.filter( item => item !== ind) : [...indus, ind]);
            return { indus: newArry }
        })
        this.filterActivated();
    }
    zodiacCName = (name) => {
        return this.state.zodiac.includes(name) ? "dropdown-item__active" : null;
    }
    indCName = (name) => {
        return this.state.indus.includes(name) ? "dropdown-item__active" : null;
    }

    handleToggle = (e) => {
        let val = e.target.value;
        this.setState(({checkbox}) => {
            let newArry = (checkbox.includes(val) ? checkbox.filter( item => item !== val) : [...checkbox, val]);
            return { checkbox: newArry}
        })
        this.filterActivated();
    }
    
    filterArray = (item, dataAtr) => {
        return(
            <div className="d-inline-block" key={item}>
                <p
                    className="px-3 mb-0 active-filter-item"
                    href="#"
                    data-atr={dataAtr}
                    onClick={event => {
                        event.preventDefault();
                        this.deactiveGenre(event, item);
                    }}
                >
                    {item}
                </p>
            </div>)
    }
    filterArrayA = (item, dataAtr) => {
        console.log(item)
        return(
            <div className="d-inline-block" key={item}>
                <p
                    className="px-3 mb-0 active-filter-item"
                    href="#"
                    data-atr={dataAtr}
                    onClick={event => {
                        event.preventDefault();
                        this.deactiveGenre(event);
                    }}
                >
                    {(dataAtr === "age") ? "Age" : "Year"} {Math.round(item[0])}-{Math.round(item[1])}
                </p>
            </div>)
    }

    deactiveGenre = (e, val) => {
        let dataAtr = e.target.attributes.getNamedItem('data-atr').value;
        if(dataAtr === 'genre'){            
            this.setState({genre: this.state.genre.filter(item => item !== val)})  
            this.filterActivated();
        }
        else if(dataAtr === 'zodiac'){
            this.setState({zodiac: this.state.zodiac.filter(item => item !== val)})  
            this.filterActivated();
        }
        else if(dataAtr === 'indus'){
            this.setState({indus: this.state.indus.filter(item => item !== val)})  
            this.filterActivated();
        }
        else if(dataAtr === 'checkbox'){
            this.setState({checkbox: this.state.checkbox.filter(item => item !== val)})  
            this.filterActivated();
        }
        else if(dataAtr === 'year'){
            this.setState({year: [1930, 2019]})  
            this.filterActivated();
        }
        else if(dataAtr === 'age'){
            this.setState({age: [0,100]})  
            this.filterActivated();
        }
        else if(dataAtr === 'stats'){
            this.setState({stats: "STATS"})  
            this.filterActivated();
        }
        else if(dataAtr === 'social'){
            this.setState({social: "SOCIAL"})  
            this.filterActivated();
        }
        else{
            return true;
        }
    }
    Reset = () => {
        this.setState({
            genre: [],
            zodiac: [],
            indus: [],
            stats: 'STATS',
            social: "SOCIAL",
            age: [0, 100],
            year: [1930, 2019],
            checkbox: [],
    
            filteractive: false
        })
        this.filterActivated();
    }
   
    render() {
        //console.log(this.state.profession);
        let { genre, age, year, zodiac, indus, stats, social, checkbox, expanded, profession } = this.state;
        return(
            <div className="container-fluid filters filter-cnc pl-0">
                <Form>
                    <Form.Group as={Row} controlId="formPlaintextInput">
                        <Col lg="5 profession-filter" className= {expanded ? "ccExp-one" : null}>
                        <ToggleButtonGroup
                                type="checkbox"
                                value={this.state.genre}
                                onChange={this.handleChange}
                            >
                            {/* value={this.state.value}
                            onChange={this.handleChange} */}
                                <span className="pt-2">PROFFESSION</span>
                                {profession.length ? 
                                    profession.map( (item,i) => {
                                        let itemFilter = item.includes("_") ? item.replace("_", " ").toLowerCase() : item.toLowerCase();
                                        if(i<4){
                                            return <ToggleButton value={item}>{itemFilter}</ToggleButton>
                                        } else {
                                            return <ToggleButton className="ccExp-hide" value={item}>{itemFilter}</ToggleButton>
                                        }
                                    }) : null}
                                    <ToggleButton className="ccExp-show" value="More">More</ToggleButton>
                                    <ToggleButton className="ccExp-hide" value="More">Less</ToggleButton>
                                
                            </ToggleButtonGroup>
                        </Col>
                        
                        <Col xs="6" lg="auto" className={expanded ? "stream-dropdown d-inline-block ccExp-two" : "stream-dropdown d-inline-block"}>
                        
                        <Dropdown>                               
                                <DropdownButton
                                    title="ZODIAC"
                                    id="dropdown-menu-align-right"
                                    onSelect={this.selectChange}
                                    >
                                    <Dropdown.Item eventKey="Aries" className={this.zodiacCName('Aries')}>Aries</Dropdown.Item>
                                    <Dropdown.Item eventKey="Tourus" className={this.zodiacCName('Taurus')}>Taurus</Dropdown.Item>
                                    <Dropdown.Item eventKey="Gemini" className={this.zodiacCName('Gemini')}>Gemini</Dropdown.Item>
                                    <Dropdown.Item eventKey="Cancer" className={this.zodiacCName('Cancer')}>Cancer</Dropdown.Item>
                                    <Dropdown.Item eventKey="Leo" className={this.zodiacCName('Leo')}>Leo</Dropdown.Item>
                                    <Dropdown.Item eventKey="Virgo" className={this.zodiacCName('Virgo')}>Virgo</Dropdown.Item>
                                    <Dropdown.Item eventKey="Libra" className={this.zodiacCName('Libra')}>Libra</Dropdown.Item>
                                    <Dropdown.Item eventKey="Scorpio" className={this.zodiacCName('Scorpio')}>Scorpio</Dropdown.Item>
                                    <Dropdown.Item eventKey="Sagittarius" className={this.zodiacCName('Sagittarius')}>Sagittarius</Dropdown.Item>
                                    <Dropdown.Item eventKey="Capricorn" className={this.zodiacCName('Capricorn')}>Capricorn</Dropdown.Item>
                                    <Dropdown.Item eventKey="Aquarius" className={this.zodiacCName('Aquarius')}>Aquarius</Dropdown.Item>
                                    <Dropdown.Item eventKey="Pisces" className={this.zodiacCName('Pisces')}>Pisces</Dropdown.Item>
                                </DropdownButton>
                            </Dropdown>
                        </Col>

                        <Col xs="6" lg="auto" className={expanded ? "stream-dropdown d-inline-block ccExp-three" : "stream-dropdown d-inline-block"}>
                            <Dropdown>                               
                                <DropdownButton
                                    title="INDUSTRIES"
                                    id="dropdown-menu-align-right"
                                    onSelect={this.indusChange}
                                    >
                                    <Dropdown.Item eventKey="Telugu" className={this.indCName('Telugu')}>Telugu</Dropdown.Item>
                                    <Dropdown.Item eventKey="English" className={this.indCName('English')}>English</Dropdown.Item>
                                    <Dropdown.Item eventKey="Tamil" className={this.indCName('Tamil')}>Tamil</Dropdown.Item>
                                    <Dropdown.Item eventKey="Hindi" className={this.indCName('Hindi')}>Hindi</Dropdown.Item>
                                    <Dropdown.Item eventKey="Malyalam" className={this.indCName('Malyalam')}>Malyalam</Dropdown.Item>
                                    <Dropdown.Item eventKey="Kannada" className={this.indCName('Kannada')}>Kannada</Dropdown.Item>
                                    <Dropdown.Item eventKey="Bengali" className={this.indCName('Bengali')}>Bengali</Dropdown.Item>
                                    <Dropdown.Item eventKey="Bhojpuri" className={this.indCName('Bhojpuri')}>Bhojpuri</Dropdown.Item>
                                    <Dropdown.Item eventKey="Marathi" className={this.indCName('Marathi')}>Marathi</Dropdown.Item>
                                    <Dropdown.Item eventKey="Punjabi" className={this.indCName('Punjabi')}>Punjabi</Dropdown.Item>
                                    <Dropdown.Item eventKey="Gujrati" className={this.indCName('Gujrati')}>Gujrati</Dropdown.Item>
                                    <Dropdown.Item eventKey="Assamese" className={this.indCName('Assamese')}>Assamese</Dropdown.Item>
                                </DropdownButton>
                            </Dropdown>
                        </Col>

                        <Col xs="6" lg="auto"  className={expanded ? "stream-dropdown d-inline-block ccExp-four" : "stream-dropdown d-inline-block"}>
                        <Dropdown>                               
                                <DropdownButton
                                    title={this.state.stats}
                                    id="dropdown-menu-align-right"
                                    onSelect={(stats) => {
                                        this.setState({ stats: stats.toUpperCase() });                                             
                                        this.filterActivated(); 
                                    }}
                                    >
                                    <Dropdown.Item eventKey="No of Movies">No of Movies</Dropdown.Item>
                                    <Dropdown.Item eventKey="Years Active">Years Active</Dropdown.Item>
                                    <Dropdown.Item eventKey="Success Ratio">Success Ratio</Dropdown.Item>
                                    <Dropdown.Item eventKey="Movies Per Year">Movies Per Year</Dropdown.Item>
                                </DropdownButton>
                            </Dropdown>
                        </Col>
                        
                        <Col xs="6" lg="auto" className={expanded ? "stream-dropdown d-inline-block ccExp-five" : "stream-dropdown d-inline-block"}>
                        <Dropdown>                               
                                <DropdownButton
                                    title={this.state.social}
                                    id="dropdown-menu-align-right"
                                    onSelect={(social) => {
                                        this.setState({ social });                                           
                                        this.filterActivated(); 
                                    }}
                                    >
                                    <Dropdown.Item eventKey="Facebook">Facebook</Dropdown.Item>
                                    <Dropdown.Item eventKey="Twitter">Twitter</Dropdown.Item>
                                    <Dropdown.Item eventKey="Instagram">Instagram</Dropdown.Item>
                                    <Dropdown.Item eventKey="Page Visits">Page Visits</Dropdown.Item>
                                    
                                </DropdownButton>
                            </Dropdown>
                        </Col>
                    </Form.Group>


                    <Form.Group as={Row} className={expanded ? "ccExp-six" : null}>
                        
                        <Col xs="12" sm="3" className=" rating-range">
                            <span className="pt-2">AGE</span>
                            <div className={((age[0] >= 5) || (age[1] <= 94)) ? "range-slider-rating range-slider ml-3 d-inline-block" : "range-slider ml-3 d-inline-block"}>     

                                <Nouislider
                                    connect
                                    start={[age[0], age[1]]}
                                    step={0.1}
                                    pips={{ mode: "count", values: 2 }}
                                    clickablePips
                                    range={{
                                        min: 0,
                                        max: 100
                                    }}
                                    tooltips = {[{to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}, {to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}]}

                                    onSlide={this.onChangeSlideRating}
                                />
                            </div>                        
                        </Col>

                        <Col xs="12" sm="4" className="ml-lg-4 year-range">
                            <span className="pt-2">YEAR</span>
                            <div className={((year[0] >= 1940) || (year[1] <= 2007)) ? "range-slider-year range-slider ml-3 d-inline-block" : "range-slider ml-3 d-inline-block"}>
                          
                                <Nouislider
                                    className="YearSlider"
                                    connect
                                    start={[year[0], year[1]]}
                                    step={1}
                                    pips={{ mode: "count", values: 2 }}
                                    clickablePips
                                    range={{
                                    min: 1930,
                                    max: 2019
                                    }}
                                    tooltips = {[{to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}, {to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}]}
                                    
                                    onSlide={this.onChangeSlideYear}
                                   // ref="NoUiSlider"
                                />
                            </div>                        
                        </Col>
                        <Col xs="12" sm="4" className="ml-lg-3">
                            <span className="pt-2">GENDER</span>
                            <div key="custom-inline-checkbox" className="d-inline-block checkbox-cont">
                                <Form.Check
                                    custom
                                    inline
                                    label="All"
                                    type="checkbox"
                                    id="custom-inline-checkbox-1"
                                    value="All"
                                   //checked={this.state.certificate1}
                                   onChange={this.handleToggle}
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="M"
                                    type="checkbox"
                                    id="custom-inline-checkbox-2"
                                    value="M"
                                    //checked={this.state.certificate2}
                                    onChange={this.handleToggle}
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="F"
                                    type="checkbox"
                                    id="custom-inline-checkbox-3"
                                    value="F"
                                   // checked={this.state.certificate3}
                                   onChange={this.handleToggle}
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="X"
                                    type="checkbox"
                                    id="custom-inline-checkbox-4"
                                    value="X"
                                   // checked={this.state.certificate4}
                                   onChange={this.handleToggle}
                                />
                            </div>
                            
                        </Col>
                    
                    </Form.Group>
                        

                    {/* <Button variant="primary" type="submit">
                        Submit
                    </Button> */}
                </Form>

                {this.state.filteractive ? (
                    <div className="active-filters mt-4">
                    FILTERED BY:
                    {genre.map( item => {
                        return (
                            (item === 'More') ? null : this.filterArray(item, "genre")
                        )
                    })}

                    {zodiac.map( item => {
                        return (
                            this.filterArray(item, "zodiac")
                        )
                    })}

                    {indus.map( item => {
                        return (
                            this.filterArray(item, "indus")
                        )
                    })}

                    {checkbox.map( item => {
                        return (
                            this.filterArray(item, "checkbox")
                        )
                    })}
                    {((year[0] !== 1930) || (year[1] !== 2019)) ? this.filterArrayA(year, "year") : null }

                    {((age[0] !== 0) || (age[1] !== 100)) ? this.filterArrayA(age, "age") : null}

                    {console.log(stats)}
                   
                    {(stats !== "STATS") ? this.filterArray(stats, "stats") : null}

                    {(social !== "SOCIAL") ? this.filterArray(social, "social") : null}
                    <div className="reset-active-filters d-inline-block float-right mr-3"
                         onClick={this.Reset}>
                        
                        <Reload />
                        RESET
                    </div>
                </div>
                ) : null}
              
                
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        castImg: state.castImg
    }
}

export default connect(mapStateToProps, {ccOptions})(MoviesFilter);

//    {this.state.filteractive ? (<div className="active-filters mt-4"  onChange={this.cheking}>
//                 {console.log('heyy')}
//                     FILTERED BY:
                     
                         
//                         {this.state.genre.map( genreItem => {
//                             return (
//                                 <>      
//                                 {console.log(genreItem)}                  
//                                 {(genreItem === 'More') ? null : (
//                                 <div className="d-inline-block" key={genreItem}>
//                                     <p
//                                         className="px-3 mb-0 active-filter-item"
//                                         href="#"
//                                         onClick={event => {
//                                             event.preventDefault();
//                                             //this.deactiveGenre(genreItem);
//                                         }}
//                                     >
//                                         {genreItem}
//                                     </p>
//                                 </div>)}
//                                 </>
//                             )
//                         })}

                       
//                         {this.state.zodiac ? (
//                             <div className="d-inline-block">
//                             <p
//                                 className="px-3 mb-0 active-filter-item"
//                                 href="#"
//                                 onClick={event => {
//                                     event.preventDefault();
//                                     this.setState({ zodiac: null })
//                                 }}
//                             >
//                                 {this.state.zodiac}
//                             </p>
//                         </div>
//                         ): null}
//                         {this.state.indus ? (
//                             <div className="d-inline-block">
//                             <p
//                                 className="px-3 mb-0 active-filter-item"
//                                 href="#"
//                                 onClick={event => {
//                                     event.preventDefault();
//                                     this.setState({ indus: null })
//                                 }}
//                             >
//                                 {this.state.indus}
//                             </p>
//                         </div>
//                         ): null}
//                         {this.state.stats ? (
//                             <div className="d-inline-block">
//                             <p
//                                 className="px-3 mb-0 active-filter-item"
//                                 href="#"
//                                 onClick={event => {
//                                     event.preventDefault();
//                                     this.setState({ stats: null })
//                                 }}
//                             >
//                                 {this.state.stats}
//                             </p>
//                         </div>
//                         ): null}
//                         {this.state.social ? (
//                             <div className="d-inline-block">
//                             <p
//                                 className="px-3 mb-0 active-filter-item"
//                                 href="#"
//                                 onClick={event => {
//                                     event.preventDefault();
//                                     this.setState({ social: null })
//                                 }}
//                             >
//                                 {this.state.social}
//                             </p>
//                         </div>
//                         ): null}

//                         {((this.state.age[0] !== 0) && (this.state.age[1] !== 100)) ? (
//                             <div className="d-inline-block">
//                             <p
//                                 className="px-3 mb-0 active-filter-item"
//                                 href="#"
//                                 onClick={event => {
//                                     event.preventDefault();
//                                     this.setState({ age: [0, 100] });
//                                 }}
//                             >
//                                 Age {Math.round(this.state.age[0])}-{Math.round(this.state.age[1])}
//                             </p>
//                         </div>
//                         ): null}

//                         {((this.state.year[0] !== 1930) && (this.state.year[1] !== 2019)) ? (
//                             <div className="d-inline-block">
//                             <p
//                                 className="px-3 mb-0 active-filter-item"
//                                 href="#"
//                                 onClick={event => {
//                                     event.preventDefault();
//                                     this.setState({ year: [1930, 2019] });
//                                 }}
//                             >
//                                 Year {Math.round(this.state.year[0])}-{Math.round(this.state.year[1])}
//                             </p>
//                         </div>
//                         ): null}

                        
//                         { this.state.certificate1 ? (
//                             <div className="d-inline-block">
//                                 <p
//                                     className="px-3 mb-0 active-filter-item"
//                                     href="#"
//                                     onClick={event => {
//                                         event.preventDefault();
//                                         this.setState({ certificate1: false, value1: null })
//                                     }}
//                                 >
//                                     {this.state.value1}
//                                 </p>
//                             </div>
                            
//                         ): null}
//                         { this.state.certificate2 ? (
//                             <div className="d-inline-block">
//                                 <p
//                                     className="px-3 mb-0 active-filter-item"
//                                     href="#"
//                                     onClick={event => {
//                                         event.preventDefault();
//                                         this.setState({ certificate2: false, value2: null })
//                                     }}
//                                 >
//                                     {this.state.value2}
//                                 </p>
//                             </div>
                            
//                         ): null}
//                         { this.state.certificate3 ? (
//                             <div className="d-inline-block">
//                                 <p
//                                     className="px-3 mb-0 active-filter-item"
//                                     href="#"
//                                     onClick={event => {
//                                         event.preventDefault();
//                                         this.setState({ certificate3: false, value3: null })
//                                     }}
//                                 >
//                                     {this.state.value3}
//                                 </p>
//                             </div>
                            
//                         ): null}
//                         { this.state.certificate4 ? (
//                             <div className="d-inline-block">
//                                 <p
//                                     className="px-3 mb-0 active-filter-item"
//                                     href="#"
//                                     onClick={event => {
//                                         event.preventDefault();
//                                         this.setState({ certificate4: false, value4: null })
//                                     }}
//                                 >
//                                     {this.state.value4}
//                                 </p>
//                             </div>
                            
//                         ): null} 


//                         <div className="reset-active-filters d-inline-block float-right mr-3"
//                         // onClick={() => refine(items)} disabled={!items.length}>
//                         >
//                         <Reload />
//                         RESET
//                     </div>
//                 </div>) : null} 
    // handleToggle1 = (e) => {
    //     console.log(e.target.value)
    //     this.setState({ certificate1: e.target.checked, value1: e.target.value })
    // }
    // handleToggle2 = (e) => {
    //     this.setState({ certificate2: e.target.checked, value2: e.target.value })
    // }
    // handleToggle3 = (e) => {
    //     this.setState({ certificate3: e.target.checked, value3: e.target.value })
    // }
    // handleToggle4 = (e) => {
    //     this.setState({ certificate4: e.target.checked, value4: e.target.value })
    // }
    // handleToggle5 = (e) => {
    //     this.setState({ certificate5: e.target.checked, value5: e.target.value })
    // }
   
    // deactiveGenre(genreItem){
    //     let array = this.state.genre,
    //     index = array.indexOf(genreItem);
    //     array.splice(index, 1)
    //     this.setState({
    //         gerne: array
    //     })
    // }
    // componentDidUpdate(){
    //     let dummyImgLists;
        
    //     if(!this.state.genre.length && !this.state.zodiac && !this.state.indus && !this.state.stats && !this.state.social && (!this.state.certificate1) && (!this.state.certificate2) && (!this.state.certificate3) && (!this.state.certificate4) && (this.state.age[0] === 0) && (this.state.age[1] === 100) && (this.state.year[0] === 1930) && (this.state.year[1] === 2019)){
    //         //console.log('this is it')
    //         dummyImgLists = null;
    //     }
    //     if(this.state.genre.length){
    //         this.state.genre.map( item => {
    //             if(item !== "More") {
    //                 dummyImgLists = this.props.castImg
    //                                 .filter((imgDetail) => {
    //                                     //console.log(imgDetail)
    //                                 return (imgDetail.proffession.toUpperCase() === item.toUpperCase());
    //                                 });
    //                             }
    //             })
            
    //     }
    //     if(this.state.zodiac) {
        
    //         //this.state.rating.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                return (imgDetail.zodiac === this.state.zodiac);
    //                             });
    //         //})
    //     }
        
    //     if(this.state.indus) {
    //         //this.state.rating.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                return (imgDetail.industries === this.state.indus);
    //                             });
    //         //})
    //     }
    //     if(this.state.stats) {
    //         //this.state.rating.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                return (imgDetail.stats === this.state.stats);
    //                             });
    //         //})
    //     }
    //     if(this.state.social) {
    //         //this.state.rating.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                return (imgDetail.social === this.state.social);
    //                             });
    //         //})
    //     }
        
    //     if((this.state.certificate1)){
    //             dummyImgLists = this.props.castImg
    //     }
    //     if((this.state.certificate2) ){
    //         //this.state.genre.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                return (imgDetail.gender.toUpperCase() === this.state.value2.toUpperCase());
    //                             });
    //         //})
    //     }
    //     if((this.state.certificate3) ){
    //         //this.state.genre.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                return (imgDetail.gender.toUpperCase() === this.state.value3.toUpperCase());
    //                             });
    //         //})
    //     }
    //     if((this.state.certificate4) ){
    //         //this.state.genre.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                return (imgDetail.gender.toUpperCase() === this.state.value4.toUpperCase());
    //                             });
    //         //})
    //     }
    //     if((this.state.certificate5) ){
    //         //this.state.genre.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                return (imgDetail.gender.toUpperCase() === this.state.value5.toUpperCase());
    //                             });
    //         //})
    //     }
    //     if((this.state.age[0] !== 0) && (this.state.age[1] !== 5)) {
    //         //this.state.rating.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                 return ((imgDetail.age >= Math.round(this.state.age[0])) && (imgDetail.age <= Math.round(this.state.age[1])));
    //                             });
    //         //})
    //     }
    //     if((this.state.year[0] !== 1930) && (this.state.year[1] !== 2019)) {
    //         //this.state.rating.map( item => {
    //             dummyImgLists = this.props.castImg
    //                             .filter((imgDetail) => {
    //                                 let year1 = Math.round(this.state.year[0]), year2 = Math.round(this.state.year[1]); 
    //                                 //console.log(imgDetail.year);
    //                                 //Math.round(parseInt(value))
    //                                 //console.log(imgRelease.getFullYear())
    //                                return ((imgDetail.year >= year1) && (imgDetail.year <= year2));
    //                             });
    //         //})
    //     }
    //     //console.log(dummyImgLists);
    //     if((dummyImgLists) && (!this.state.filteractive)) {
    //         this.setState({filteractive: true})
    //        } 
    //        if((!dummyImgLists) && (this.state.filteractive)) {
    //         this.setState({filteractive: false})
    //         }
    //     this.props.filterCallback(dummyImgLists);
    // }

    // onChange = (e) => {
    //    // console.log(e.target.parentNode)
    //     this.setState({
    //         inputValue: e.target.value
    //     });
    //     if(!e.target.value){
    //         console.log('noo')
    //         return false;
    //     }
    //     let a = document.createElement("DIV");
    //     a.setAttribute("id", this.id+ "autocomplete-list");
    //     a.setAttribute("class", "autocomplete-items");
        
    //     e.target.parentNode.appendChild(a);
    //     //console.log(this.props.imgLists.length)
    //     for(var i=0; i< this.props.imgLists.length; i++){
    //         //console.log(this.props.imgLists[i].title.substr(0, e.target.value.length).toUpperCase())
    //        if(this.props.imgLists[i].title.substr(0, e.target.value.length).toUpperCase() === e.target.value.toUpperCase()){
    //            //console.log('works')
    //             let b = document.createElement("DIV");
    //             //b.setAttribute("onClick", "autocomplete-items");
    //             b.innerHTML = "<string>"+this.props.imgLists[i].title.substr(0, e.target.value.length)+ "</strong>";

    //             b.innerHTML += this.props.imgLists[i].title.substr(e.target.value.length);

    //             b.innerHTML += "<input type='hidden' value='" + this.props.imgLists[i].title+ "'>";

    //             b.addEventListener("click", function() {
    //                 this.setState({
    //                     inputValue: this.getElementsByTagName("input")[0].value
    //                 });
    //                 console.log(this.getElementsByTagName("input")[0].value)
    //             })
    //             a.appendChild(b);
    //        }
    //     }
    // }



    
// const uiSlider = () => {
    
// var pipsSlider = document.getElementById('slider-pips');

// noUiSlider.create(pipsSlider, {
//     range: {
//         min: 0,
//         max: 100
//     },
//     start: [50],
//     pips: {mode: 'count', values: 5}
// });

// }

// var str1="acb", str2="bca", arry1=[], arry2=[];
// for(var i=0;i<str1.length;i++){
//   for(var j=0;j<str2.length;j++){
//     if(str2.includes(str1.charAt(i))){
//       arry1.push(str1.charAt(i));
//     }else{
//             arry2.push(str1.charAt(i));
//     }
//   }
// }
// console.log(arry2);
// console.log(arry2.length);
// if(arry2.length === 0 ){
//     console.log("is twin");
// }else{
// console.log("not twin");
// }
