// import './filters.scss';
import React from 'react';
import {connect} from 'react-redux';
import base from '../../api/base';
import { Form, Button, Row, Col, ToggleButtonGroup, ToggleButton, Dropdown, DropdownButton } from 'react-bootstrap';
//import { Search } from '../../svgIcons';
//import Nouislider from 'react-nouislider';
//import HitComponent from './HitComponent';//CustomHits
//import {CustomHits} from './HitComponent';//CustomHits
import {CustomRangeSlider} from './CustomRangeSlider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, Configure, Panel, RefinementList, MenuSelect, connectHits, connectSearchBox, Index } from 'react-instantsearch-dom';
import { fetchStreams, movieStatic } from '../../actions';
import MultiSearch from './MultiSearch';
import { CustomCurrentRefinements } from './ActiveFilters';


const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');
const index = searchClient.initIndex('AZ_ARTIST_SEARCH');
const mvIndex = searchClient.initIndex('AZ_MOVIE_SEARCH');

class MoviesFilter extends React.Component {
    state = {
        searchArray: [],
        selectedStream: "STREAMING ON",
        expanded: false,
        year: [1930, 2019],
        rating: [0, 5]
    }
    // apicall = (url) => {
    //     this.props.fetchStreams(url);
    // }
    componentDidMount(){
        // mvIndex.search("", {
        //     "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
        //     "facetFilters": ["genre:COMEDY"]
        // },
        // (err, { hits } = {}) => {
        //     if (err) throw err;  
        //     console.group(hits)
        // });
    }
    searchActive = (searchArray) => {
        this.setState({searchArray})
    }
    searchRefine = (name) => {
        this.setState(({searchArray}) => ({searchArray: searchArray.filter( item => item !==name )}));
    }
    streamOnSelect = (e) => {
        this.setState({selectedStream: e.toUpperCase()})
        //this.props.streamActive(e);
    }
    streamOnRefine = (e) => {
        this.setState({selectedStream: "STREAMING ON"})
    }
    gnreExpand = (expanded) => {
        //console.log(expanded)
        if(expanded && !this.state.expanded){
            this.setState({expanded})
        }
        else if(!expanded && this.state.expanded){
            this.setState({expanded})
        }
    }
    sliderHandler = (arr) => {
        //console.log(arr)
        if(arr[0] <= 5){
            this.setState({ rating: arr})
        }
        else(
            this.setState({year: arr})
        )
    }
    render() {
        //console.log(this.props.filterString)
        return(
            <div className="container-fluid filters pl-0">
                <InstantSearch searchClient={searchClient} indexName="AZ_MOVIE_SEARCH">
                <Index indexName="AZ_MOVIE_SEARCH" />
                <Index indexName="AZ_ARTIST_SEARCH" />
                   

                    <div className="content-wrapper">
                        <Facets 
                            update={this.props.update} 
                            searchActive={this.searchActive} 
                            searchArray={this.state.searchArray}
                            streamOnSelect={this.streamOnSelect}
                            selectedStream={this.state.selectedStream}
                            gnreExpand={this.gnreExpand}
                            expanded={this.state.expanded}
                            sliderHandler={this.sliderHandler}
                            year={this.state.year}
                            rating={this.state.rating}
                        /> 
                        <CustomHits />
                    </div> 
                  
                    <CustomCurrentRefinements 
                        searchRefine={this.searchRefine} 
                        searchArray={this.state.searchArray} 
                        selectedStream={this.state.selectedStream} 
                        streamOnRefine={this.streamOnRefine}
                        checkItemsLength={this.props.checkItemsLength}
                    />
                 
                </InstantSearch>
                {/* <InstantSearch searchClient={searchClient} indexName="AZ_MOVIE_SEARCH">
                    <Configure hitsPerPrating={18} /> 
                    <div className="content-wrapper">
                        <Facets 
                            update={this.props.update} 
                            searchActive={this.searchActive} 
                            searchArray={this.state.searchArray}
                            streamOnSelect={this.streamOnSelect}
                            selectedStream={this.state.selectedStream}
                            gnreExpand={this.gnreExpand}
                            expanded={this.state.expanded}
                            sliderHandler={this.sliderHandler}
                            year={this.state.year}
                            rating={this.state.rating}
                        /> 
                        <CustomHits />
                    </div>  
                    <CustomCurrentRefinements 
                        searchRefine={this.searchRefine} 
                        searchArray={this.state.searchArray} 
                        selectedStream={this.state.selectedStream} 
                        streamOnRefine={this.streamOnRefine}
                        checkItemsLength={this.props.checkItemsLength}
                    />
                </InstantSearch>                 */}
            </div>
        )
    }
}







const Hits = ({hits}) => {
    return true;
}

const CustomHits = connectHits(Hits);

const Facets = ({update, searchActive, searchArray,streamOnSelect, selectedStream, gnreExpand, expanded, sliderHandler, year, rating}) => {
    //console.log(year)
    return(
        <section className="movie-facet-wrapper">
            <section className="row mb-4">
                {/* <MultiSearch /> */}
                <Panel header={<span className="mr-2">CAST&CREW:</span>} className={expanded ? "expanded-one col-4" : "col-4"}>
                    <MultiSearch update={update} searchActive={searchActive} searchArray={searchArray}/>
                </Panel>


                <Panel header={<span>GENRE:</span>} className={expanded ? "expanded-two col-5" : "col-5"}>
                    <RefinementList attribute="genre" operator="and" limit={5} showMore 
                        //defaultRefinement="COMEDY"
                        translations={{
                            showMore(expanded) {
                                gnreExpand(expanded);
                               return expanded ? "Less" : "More";
                            },
                            noResults: 'No results',
                        }}
                        transformItems={items =>
                            items.map(item => ({
                            ...item,
                            label: item.label.toLowerCase()
                        }))
                    }
                        
                    />
                        {/* <CustomRefinementList attribute="genre" movieStatic={statCont}/> */}
                </Panel>

                <Panel className={expanded ? "expanded-three col-3" : "stream-dropdown col-3"}>
                    {/* <MenuSelect attribute="certificate" 
                        translations={{
                            seeAllOption: 'STREAMING ON',
                        }}llll
                    /> */}
                    <Dropdown>                               
                        <DropdownButton
                            title={selectedStream}
                            id="dropdown-menu-align-right"
                            onSelect={streamOnSelect}
                            >
                            {/* <Dropdown.Item eventKey="All">All</Dropdown.Item> */}
                            <Dropdown.Item eventKey="YouTube">YouTube</Dropdown.Item>
                            <Dropdown.Item eventKey="Amazon Prime">Amazon Prime</Dropdown.Item>
                            <Dropdown.Item eventKey="Hotstar">Hotstar</Dropdown.Item>
                            <Dropdown.Item eventKey="Netflix">Netflix</Dropdown.Item>
                            <Dropdown.Item eventKey="Sun Nxt">Sun Nxt</Dropdown.Item>
                            <Dropdown.Item eventKey="Zee5">Zee5</Dropdown.Item>
                            <Dropdown.Item eventKey="Jio">Jio</Dropdown.Item>
                            <Dropdown.Item eventKey="Yupp TV">Yupp TV</Dropdown.Item>
                            <Dropdown.Item eventKey="Viu">Viu</Dropdown.Item>
                            <Dropdown.Item eventKey="Eros Now">Eros Now</Dropdown.Item>
                        </DropdownButton>
                    </Dropdown>
                </Panel>
            </section>
            <section className="row">
                <Panel header={<span className="mr-2">RATING:</span>} className="col-3">
                    <CustomRangeSlider 
                        attribute="vcinemaRating"
                        min={0}
                        max={5}
                        values={[0, 5]}
                        tooltip = {[{to: function(value) {
                            return value;
                        }}, {to: function(value) {
                            return value;
                        }}]}
                        sliderHandler={sliderHandler}
                        rating={rating}
                    />
                </Panel>
                <Panel header={<span className="mr-2">YEAR:</span>} className="col-5">
                    <CustomRangeSlider 
                        attribute="year"
                        min={1930}
                        max={2019}
                        values={[1930,2019]}
                        tooltip = {[{to: function(value) {
                            return Math.round(parseInt(value));
                        }}, {to: function(value) {
                            return Math.round(parseInt(value));
                        }}]}
                        sliderHandler={sliderHandler}
                        year={year}
                    />
                </Panel>
                
                <Panel header={<span className="mr-2">CERTIFICATE:</span>} className="col-4">
                    <RefinementList attribute="certificate" operator="or" limit={5} />
                </Panel>
            </section>
        </section>
    )
}



const mapStateToProps = (state) => {
    return {
        movieStaticCont : state.movieStaticCont
    }
}
export default connect(mapStateToProps, {fetchStreams, movieStatic})(MoviesFilter);
