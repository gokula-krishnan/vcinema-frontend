import React from 'react';
import { connectCurrentRefinements} from 'react-instantsearch-dom';
import {Reload} from '../../svgIcons';

class CurrentGalleryRefinement extends React.Component {
    
    componentDidUpdate(){
        if(this.props.items.length || this.props.searchArray.length || (this.props.selectedStream !== "Audio Launch /Pre-Release")){
            
        }
        this.props.checkItemsLength(this.props.items, this.props.searchArray, this.props.selectedStream, this.props.selectedEvent, this.props.selectedTrend);
        //console.log(this.props.items)
    }
    searchArrayComp = (searchArray,searchRefine) => {
        return (searchArray.map( item => (
            <a
                className="px-3 active-filter-item"
                href="#"
                key={item}
                onClick={event => {
                    event.preventDefault();
                    searchRefine(item)
                }}
                >
                {item}
            </a>
        )))
    }
    streamOnComp = (item, eventOnRefine) => {
        return (
            <a
                className="px-3 active-filter-item"
                href="#"
                key={item}
                onClick={event => {
                    event.preventDefault();
                    eventOnRefine(item)
                }}
                >
                {item}
            </a>
            )
    }
    itemsComp = (item, refine) => {
       // console.log(item)
        return ( item.items ? (
            <React.Fragment>
                {item.items.map(nested => (
                    <a
                    className="px-3 active-filter-item"
                    href="#"
                    key={nested.label}
                    onClick={event => {
                        event.preventDefault();
                        refine(nested.value);
                    }}
                    >
                    {nested.label}
                    </a>
                ))}
            </React.Fragment>
        ) : (
            
            <a
                className="px-3 active-filter-item"
                href="#"
                onClick={event => {
                    event.preventDefault();
                    refine(item.value);
                }}
            >
                {item.attribute === "year" ? "YEAR" : "RATING"} {item.currentRefinement.min} - {item.currentRefinement.max}
            </a>
        ))
    }
    render(){
        let {items, refine, searchArray, searchRefine, selectedStream, streamOnRefine, selectedTrend, trendOnRefine, selectedEvent, eventOnRefine} = this.props;
        
        // console.log(selectedEvent);
        // console.log(selectedTrend);
        
        if(items.length && searchArray.length && (selectedEvent !== "Audio Launch /Pre-Release")){
           
            return (
             
                <section className="active-filters mt-4">
                    FILTERED BY:
                    {this.searchArrayComp(searchArray,searchRefine)}
                    {items.map(item => (
                        <div key={item.currentRefinement[0]} className="d-inline-block">
                            {this.itemsComp(item, refine)}
                        </div>
                    ))}
                    {this.streamOnComp(selectedEvent, eventOnRefine)}
                    <CustomClearRefinements /> 
                </section>)
        }

        else if((selectedStream !== "Audio Launch /Pre-Release") && searchArray.length){

            
            return (
            <section className="active-filters mt-4">
            FILTERED BY:
                <div className="d-inline-block">
                    {this.searchArrayComp(searchArray,searchRefine)}                    
                    {this.streamOnComp(selectedEvent, eventOnRefine)}
                </div>
                <CustomClearRefinements /> 
            </section>)
        }  

        else if(items.length && (selectedEvent !== "Audio Launch /Pre-Release")){

           
            return (
            <section className="active-filters mt-4">
                FILTERED BY:
                {items.map(item => (
                    <div key={item.currentRefinement[0]} className="d-inline-block">
                        {this.itemsComp(item, refine)}
                    </div>
                ))}
                {this.streamOnComp(selectedEvent, eventOnRefine)}
                <CustomClearRefinements /> 
            </section>)
        }
        else if(items.length && searchArray.length){
           
            return (
            <section className="active-filters mt-4">
                FILTERED BY:
                {this.searchArrayComp(searchArray,searchRefine)}
                {items.map(item => (
                    <div key={item.currentRefinement[0]} className="d-inline-block">
                        {this.itemsComp(item, refine)}
                    </div>
                ))}
                <CustomClearRefinements /> 
            </section>)
        }

        else if(items.length) {
            
            return (
            <section className="active-filters mt-4">
                FILTERED BY:
                {items.map(item => (
                    <div key={item.currentRefinement[0]} className="d-inline-block">
                        {this.itemsComp(item, refine)}
                    </div>
                ))}
                <CustomClearRefinements /> 
            </section>)
        }

        else if(searchArray.length){
            
            return (
            <section className="active-filters mt-4">
                FILTERED BY:
                    <div className="d-inline-block">
                        {this.searchArrayComp(searchArray,searchRefine)}                    
                    </div>
                <CustomClearRefinements /> 
            </section>)
        }

        else if(selectedEvent !== "Audio Launch /Pre-Release"){
           
            return (
                <section className="active-filters mt-4">
                    FILTERED BY:
                        <div className="d-inline-block">
                            {this.streamOnComp(selectedEvent, eventOnRefine)}
                        </div>
                    <CustomClearRefinements /> 
                </section>
            )
        }
        else {
            return true;
        }
    }
}

const ClearRefinements = ({ items, refine}) => {
    return(
        <div className="reset-active-filters d-inline-block float-right mr-3"
            onClick={() => refine(items)} disabled={!items.length}>
            <Reload />
            RESET
        </div>
    )
  };
  
  // 2. Connect the component using the connector
const CustomClearRefinements = connectCurrentRefinements(ClearRefinements);
  
  // 2. Connect the component using the connector

 export const CustomCurrentRefinements = connectCurrentRefinements(CurrentGalleryRefinement);

