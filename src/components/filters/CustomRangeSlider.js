

import React from 'react';
import { connectRange, connectHits } from 'react-instantsearch-dom';
// import Rheostat from 'rheostat'; 
import PropTypes from 'prop-types';
import './filters.scss';
import Nouislider from 'react-nouislider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';

let year = null, rating = null, calendar = null;

class Range extends React.Component {
static propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    currentRefinement: PropTypes.object,
    refine: PropTypes.func.isRequired,
    canRefine: PropTypes.bool.isRequired,
  };
  // state = { 
  //   year: [1930, 2019],
  //   age: [0, 100] };
  //state = { currentValues: { min: this.props.min, max: this.props.max } };

  // componentWillReceiveProps(sliderState) {
  //   if (sliderState.canRefine) {
  //     this.setState({
  //       currentValues: {
  //         min: sliderState.currentRefinement.min,
  //         max: sliderState.currentRefinement.max,
  //       },
  //     });
  //   }
  // }

  // onValuesUpdated = sliderState => {
  //   this.setState({
  //     currentValues: { min: sliderState[0], max: sliderState[1] },
  //   });
  // };

  onChange = (sliderState) => {
   //console.log(this.props.currentRefinement.min);
    var min = parseInt(sliderState[0]),
    max = parseInt(sliderState[1]);
    //console.log(min)
    if (
      this.props.currentRefinement.min !== min ||
      this.props.currentRefinement.max !== max
    ) {
    
    //console.log([min])
    // this.props.sliderHandler([min,max])

      this.props.refine({
        min: min,
        max: max,
      });
    }
  };
  classname = () => {
   
    if(this.props.year){
      year = this.props.year;
    } else if(this.props.rating){
      rating = this.props.rating;
    }
 
    if(this.props.calendar){
      calendar = this.props.calendar;
    }

    if(calendar){ 
      if((calendar[0] >= 1939) || (calendar[1] <= 2010)) {
        //console.log("year")
   
        return "range-slider range-slider-year";
      } 
      else{
      
        return "range-slider";
      }
    }

    if(year && rating){
      
      if((year[0] >= 1939) || (year[1] <= 2010)) {
        //console.log("year")
     
        return "range-slider range-slider-year";
      } 
      else if((rating[0] >= 1) || (rating[1] <= 4)) { 
      
        console.log(rating[0] >= 1)
        return "range-slider range-slider-rating";
      }
      else{
     
        return "range-slider";
      }

    }
    
  }
  render() {
    const { min, max, currentRefinement, tooltip, year, rating, calendar } = this.props;
    //console.log(tooltip)
    return min !== max ? (
      <div className={this.classname()}>
        <Nouislider
          connect
          start={[currentRefinement.min, currentRefinement.max]}
          step={0.1}
          pips={{ mode: "count", values: 2 }}
          //clickablePips
          range={{
          min: min,
          max: max
          }}
          tooltips = {tooltip}
          onSlide={this.onChange}

        />
        {/* <Rheostat
          min={min}
          max={max}
          values={[currentRefinement.min, currentRefinement.max]}
          onChange={this.onChange}
          onValuesUpdated={this.onValuesUpdated}
        />
        <div className="rheostat-values">
          <div>{currentValues.min}</div>
          <div>{currentValues.max}</div>
        </div> */}
      </div>
    ) : null;
  }
}

export const CustomRangeSlider = connectRange(Range);





// const RangeSlider = ({attribute, min, max, values}) => {
//     //console.log();
//     return(
//         <div className="range-slider w-75 ml-3 d-inline-block">                            
//         <Nouislider
//             connect
//             start={values}
//             step={0.1}
//             pips={{ mode: "count", values: 2 }}
//             clickablePips
//             range={{
//             min: 0,
//             max: 5
//             }}
//             tooltips = {[{to: function(value) {
//                 return value.toFixed(1);
//             }}, {to: function(value) {
//                 return value.toFixed(1);
//             }}]}

//             onSlide={onChangeSlideRating}
//         />
//     </div>                        
//     )
//     };

// const onChangeSlideRating = (e) => {
//     console.log(e) // logs the value this.refs.NoUiSlider.slider.get()
// }
  
//   // 2. Connect the component using the connector
// export const CustomRangeSlider = connectRange(RangeSlider);
  
  