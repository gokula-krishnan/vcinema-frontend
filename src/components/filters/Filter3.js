import './filters.scss';
import React from 'react';
import {connect} from 'react-redux';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Search, Reload } from '../../svgIcons';
import Nouislider from 'react-nouislider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';
import Chips, { Chip } from 'react-chips';
//import base from '../../api/base';
import algoliasearch from 'algoliasearch/lite';

const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');
const index = searchClient.initIndex('AZ_ARTIST_SEARCH');

const theme = {
    chipsContainer: {
      display: "flex",
      position: "relative",
      border: "1px solid #343a40",
      backgroundColor: '#343a40',
      font: "10px Arial",
      alignItems: "center",
      flexWrap: "wrap",
      borderRadius: 1,
      ':focus': {
          border: "1px solid #aaa",
      }
    },
    container:{
      flex: 1,
    },
    containerOpen: {
  
    },
    input: {
      border: 'none',
      outline: 'none',
      boxSizing: 'border-box',
      width: '100%',
      margin: 0,
      backgroundColor: '#343a40',
      color: '#fff'
    },
    suggestionsContainer: {
    },
    suggestionsList: {
      position: 'absolute',
      border: '1px solid #343a40',
      zIndex: 10,
      left: 0,
      top: '120%',
      width: '100%',
      backgroundColor: '#343a40',
      listStyle: 'none',
      padding: 0,
      margin: 0,
    },
    suggestion: {
      padding: '5px 15px'
    },
    suggestionHighlighted: {
      color: '#00c1ff'
    },
    sectionContainer: {
  
    },
    sectionTitle: {
  
    },
  }
  
const chipTheme = {
    chip: {
      padding: '2px 4px',
      background: "#1d181c",
      margin: "0px",
      borderRadius: 3,
      cursor: 'pointer',
      color: '#00c1ff',
    },
    chipSelected: {
      background: '#888',
    },
    chipRemove: {
      fontWeight: "bold",
      display: 'none',
      cursor: "pointer",
      ':hover': {
        color: 'red',
      }
    }
  }



class Filter3 extends React.Component {
    state = {
        inputValue: "", 
        chips: [],
        rating: [0, 5],
        year: [1930, 2019],

        filteractive: false
    }
    // componentDidMount() {
    //     this.response();
    // }
    onChangeSlideRating = (rating) => {
        this.setState({ rating }); 
        this.filterActivated();       
    }
    onChangeSlideYear = (year) => {
        this.setState({ year });
        this.filterActivated();
    }
    suggestionsClicked = (e) => {
        this.setState({ inputValue: "" });                
    }
    filterActivated = () => {
        let { chips, rating, year, filteractive } = this.state;
        //console.log(genre.length)
        if(!filteractive){
            this.setState({ filteractive: true });
        }
        else if(filteractive && (!chips.length) && (rating[0] === 0) && (rating[1] === 100) && (year[0] === 1930) && (year[1] === 2019)){
            // && !zodiac.length && !indus.length && !checkbox.length && (age[0] !== 0) && (age[1] !== 100) && (year[0] !== 1930) && (year[1] !== 2019)
            //console.log('di')
            this.setState({ filteractive: false });
        }
        setTimeout(() => {
            this.props.checkItemsLength(this.state);
        }, 500)
    }

    onChange = chips => {        
        this.setState({ chips });
        this.filterActivated();
    }
    
    fetchSuggCall(value, callback){
        this.setState({
            inputValue: value
        }); 
        //this.suggestions();
        index.search({
            "hitsPerPage": 5,
            query: value
            },
            (err, { hits } = {}) => {
              if (err) throw err;              
              //this.setState({ suggestionsArray:  hits.map(hit => hit.name) })
              callback(hits.map(hit => hit.name));
            }
          )        
    }
    deactiveFilter = (e, val) => {
        // let array = this.state.chips,
        // index = array.indexOf(chip);
        // array.splice(index, 1)
        // this.setState({
        //     chips: array
        // })
        let dataAtr = e.target.attributes.getNamedItem('data-atr').value;
        if(dataAtr === 'cc'){            
            this.setState({chips: this.state.chips.filter(item => item !== val)})  
            this.filterActivated();
        }
        else if(dataAtr === 'year'){
            this.setState({year: [1930, 2019]})  
            this.filterActivated();
        }
        else if(dataAtr === 'rating'){
            this.setState({rating: [0,5]})  
            this.filterActivated();
        }
    }
    filterArrayA = (item, dataAtr) => {
        return(
            <div className="d-inline-block" key={item}>
                <p
                    className="px-3 mb-0 active-filter-item"
                    href="#"
                    data-atr={dataAtr}
                    onClick={event => {
                        event.preventDefault();
                        this.deactiveFilter(event);
                    }}
                >
                    {(dataAtr === "year") ? "Year" : "Rating"} {Math.round(item[0])}-{Math.round(item[1])}
                </p>
            </div>)
    }
  
    Reset = () => {
        this.setState({ 
            chips: [],
            rating: [0, 5],
            year: [1930, 2019],
            filteractive: false
        })
        this.filterActivated();
    }
    render() {
     console.log(this.props.imgLists);
        let { chips, rating, year } = this.state;
        
        return(
            <div className="container-fluid filters pl-0">
                <Form className="music-filter">
                    <Form.Group as={Row} controlId="formPlaintextInput">
                        <Col sm="4">
                            <span className="pt-2">CAST & CREW</span>
                            <div className="d-inline-block ml-3 search-cast autocomplete">                                
                                <div className="selected-items pl-0">

                                    <Chips
                                        value={this.state.chips}
                                        onChange={this.onChange}
                                        //suggestions={this.state.suggestionsArray}
                                        placeholder="Search..."
                                        theme={theme}
                                        chipTheme={chipTheme}
                                        fetchSuggestionsThrushold={5}
                                        fetchSuggestions={(value, callback) => {
                                            this.fetchSuggCall(value, callback)
                                        }}
                                    />
                                </div>
                                    
                                <Button className="search-btn">
                                    <Search />
                                </Button>
                            </div>
                        </Col>
                        
                        <Col sm="4" className="ml-4 year-range">
                            <span className="pt-2">YEAR</span>
                            <div className="range-slider ml-3 d-inline-block">
                          
                                <Nouislider
                                    className="YearSlider"
                                    connect
                                    start={[year[0], year[1]]}
                                    step={1}
                                    pips={{ mode: "count", values: 2 }}
                                    clickablePips
                                    range={{
                                    min: 1930,
                                    max: 2020
                                    }}
                                    tooltips = {[{to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}, {to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}]}
                                    
                                    onSlide={this.onChangeSlideYear}
                                   // ref="NoUiSlider"
                                />
                            </div>                        
                        </Col>
                        <Col sm="3 rating-range">
                            <span className="pt-2">RATING</span>
                            <div className="range-slider ml-3 d-inline-block">                            
                                <Nouislider
                                    connect
                                    start={[rating[0], rating[1]]}
                                    step={0.1}
                                    pips={{ mode: "count", values: 2 }}
                                    clickablePips
                                    range={{
                                    min: 0,
                                    max: 5
                                    }}
                                    tooltips = {[{to: function(value) {
                                        return value;
                                    }}, {to: function(value) {
                                        return value;
                                    }}]}

                                    onSlide={this.onChangeSlideRating}
                                />
                            </div>                        
                        </Col>

                    </Form.Group>

                    {/* <Button variant="primary" type="submit">
                        Submit
                    </Button> */}
                </Form>


                {this.state.filteractive ? (
                    <div className="active-filters mt-4">
                    FILTERED BY:
                    {chips.map( item => {
                        return (
                            <div className="d-inline-block" key={item}>
                                <p
                                    className="px-3 mb-0 active-filter-item"
                                    href="#"
                                    data-atr="cc"
                                    onClick={event => {
                                        event.preventDefault();
                                        this.deactiveFilter(event, item);
                                    }}
                                >
                                    {item}
                                </p>
                            </div>
                        )
                    })}

                    {((year[0] !== 1930) || (year[1] !== 2019)) ? this.filterArrayA(year, "year") : null }

                    {((rating[0] !== 0) || (rating[1] !== 5)) ? this.filterArrayA(rating, "rating") : null}

                   
                    <div className="reset-active-filters d-inline-block float-right mr-3"
                         onClick={this.Reset}>                        
                        <Reload />
                        RESET
                    </div>
                </div>
                ) : null}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        castImg: state.castImg,
        musicImg: state.musicImg,
    }
}

export default connect(mapStateToProps)(Filter3);

//componentDidUpdate(){
    // let dummyImgLists;
    // if(!this.state.chips.length && (this.state.rating[0] === 0) && (this.state.rating[1] === 5) && (this.state.year[0] === 1930) && (this.state.year[1] === 2019)){
    //     //console.log('this is it')
    //     dummyImgLists = null;
    // }
    // if(this.state.chips.length){
    //     this.state.chips.map( chip => {
    //         dummyImgLists = this.props.musicImg
    //                         .filter((imgDetail) => {
    //                            return (imgDetail.cast.includes(chip));
    //                         });
    //     })
    // }
    // if((this.state.rating[0] !== 0) && (this.state.rating[1] !== 5)) {
    //     //this.state.rating.map( item => {
    //         dummyImgLists = this.props.musicImg
    //                         .filter((imgDetail) => {
    //                            return ((imgDetail.rating >= this.state.rating[0]) && (imgDetail.rating <= this.state.rating[1]));
    //                         });
    //     //})
    // }
    // if((this.state.year[0] !== 1930) && (this.state.year[1] !== 2019)) {
    //     //this.state.rating.map( item => {
    //         dummyImgLists = this.props.musicImg
    //                         .filter((imgDetail) => {
    //                             let year1 = Math.round(this.state.year[0]), year2 = Math.round(this.state.year[1]), imgRelease = new Date(imgDetail.release); 
    //                             //console.log(Math.round(this.state.year[0]));
    //                             //Math.round(parseInt(value))
    //                             //console.log(imgRelease.getFullYear())
    //                            return ((imgRelease.getFullYear() >= year1) && (imgRelease.getFullYear() <= year2));
    //                         });
    //     //})
    // }
    // //console.log(dummyImgLists)
    // if((dummyImgLists) && (!this.state.filteractive)) {
    //     this.setState({filteractive: true})
    //    } 
    //    if((!dummyImgLists) && (this.state.filteractive)) {
    //     this.setState({filteractive: false})
    //     }
    // this.props.filterCallback(dummyImgLists);
//}