import './filters.scss';
import React from 'react';
import {connect} from 'react-redux';
import { Form, Button, Row, Col, ToggleButtonGroup, ToggleButton, Dropdown, DropdownButton } from 'react-bootstrap';
import { Search, Reload } from '../../svgIcons';
import Nouislider from 'react-nouislider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';
import Chips, { Chip } from 'react-chips';
import base from '../../api/base';

const theme = {
    chipsContainer: {
      display: "flex",
      position: "relative",
      border: "1px solid #343a40",
      backgroundColor: '#343a40',
      font: "10px Arial",
      alignItems: "center",
      flexWrap: "wrap",
      borderRadius: 1,
      ':focus': {
          border: "1px solid #aaa",
      }
    },
    container:{
      flex: 1,
    },
    containerOpen: {
  
    },
    input: {
      border: 'none',
      outline: 'none',
      boxSizing: 'border-box',
      width: '100%',
      margin: 0,
      backgroundColor: '#343a40',
      color: '#fff'
    },
    suggestionsContainer: {
    },
    suggestionsList: {
      position: 'absolute',
      border: '1px solid #343a40',
      zIndex: 10,
      left: 0,
      top: '120%',
      width: '100%',
      backgroundColor: '#343a40',
      listStyle: 'none',
      padding: 0,
      margin: 0,
    },
    suggestion: {
      padding: '5px 15px'
    },
    suggestionHighlighted: {
      color: '#00c1ff'
    },
    sectionContainer: {
  
    },
    sectionTitle: {
  
    },
  }
  
const chipTheme = {
    chip: {
      padding: '2px 4px',
      background: "#1d181c",
      margin: "0px",
      borderRadius: 3,
      cursor: 'pointer',
      color: '#00c1ff',
    },
    chipSelected: {
      background: '#888',
    },
    chipRemove: {
      fontWeight: "bold",
      display: 'none',
      cursor: "pointer",
      ':hover': {
        color: 'red',
      }
    }
  }


  let dummyImgLists;

class MoviesFilter extends React.Component {
    state = {
        inputValue: "", 
        chips: [],
        // suggestionsArray: null,
        genre: [],
        streamOn: null,
        rating: [0, 5],
        year: [1930, 2019],
        certificate1: false,
        value1: null,
        certificate2: false,
        value2: null,
        certificate3: false,
        value3: null,
        certificate4: false,
        value4: null,
        certificate5: false,
        value5: null,
        filteractive: false
    }
    // componentDidMount() {
    //     this.response();
    // }
    onChangeSlideRating = (rating) => {
        this.setState({ rating });        
    }
    onChangeSlideYear = (year) => {
        this.setState({ year });
    }
    suggestionsClicked = (e) => {
        this.setState({
            inputValue: ""
        });                
    }

    suggestions = () => {
        const arr = [];
        for( var i=0; i< this.props.castImg.length; i++){
                if(this.props.castImg[i].title.substr(0, this.state.inputValue.length).toUpperCase() === this.state.inputValue.toUpperCase()){ 
                    arr.push(this.props.castImg[i].title);
                }
        }
        this.setState({
            suggestionsArray: arr
        });
        return true;
    }

    // response =  async () => {
    //     const res = await base.get('/options?option=movieGenres&key=true');
    //     //console.log(res.data.movieGenres);
    //     this.setState({
    //         suggestionsArray: res.data.movieGenres
    //     })
    // }
    onChange = chips => {        
        this.setState({ 
            chips
        });
    }
    handleChange = (genre) => {
        this.setState({ genre })
        
    }
    fetchSuggCall(value, callback){
        this.setState({
            inputValue: value
        }); 
        this.suggestions();
        callback(this.state.suggestionsArray);
    }

    streamOnSelect = (streamOn) => {
        //console.log(streamOn)
        this.setState({ streamOn })
    }
    handleToggle1 = (e) => {
        this.setState({ certificate1: e.target.checked, value1: e.target.value })
    }
    handleToggle2 = (e) => {
        this.setState({ certificate2: e.target.checked, value2: e.target.value })
    }
    handleToggle3 = (e) => {
        this.setState({ certificate3: e.target.checked, value3: e.target.value })
    }
    handleToggle4 = (e) => {
        this.setState({ certificate4: e.target.checked, value4: e.target.value })
    }
    handleToggle5 = (e) => {
        this.setState({ certificate5: e.target.checked, value5: e.target.value })
    }
    deactiveFilter(chip) {
        //console.log(chip)
        let array = this.state.chips,
        index = array.indexOf(chip);
        array.splice(index, 1)
        this.setState({
            chips: array
        })

    }
    deactiveGenre(genreItem){
        let array = this.state.genre,
        index = array.indexOf(genreItem);
        array.splice(index, 1)
        this.setState({
            gerne: array
        })
    }
    componentDidUpdate(){
        //let dummyImgLists;
        if(!this.state.chips.length && !this.state.genre.length && !this.state.streamOn && (!this.state.certificate1) && (!this.state.certificate2) && (!this.state.certificate3) && (!this.state.certificate4) && (!this.state.certificate5) && (this.state.rating[0] === 0) && (this.state.rating[1] === 5) && (this.state.year[0] === 1930) && (this.state.year[1] === 2019)){
            //console.log('this is it')
            dummyImgLists = null;
        }
        if(this.state.chips.length){
            this.state.chips.map( chip => {
                dummyImgLists = this.props.movieImgdetails
                                .filter((imgDetail) => {
                                   return (imgDetail.cast.includes(chip));
                                });
            })
        }
        if(this.state.genre.length){
            this.state.genre.map( item => {
                if(item !== "More") {
                    dummyImgLists = this.props.movieImgdetails
                                    .filter((imgDetail) => {
                                       return (imgDetail.genre.toUpperCase() === item.toUpperCase());
                                    });
                }
            })
        }
        
        if((this.state.certificate1) || (this.state.certificate2) || (this.state.certificate3) || (this.state.certificate4) || (this.state.certificate5)){
            //this.state.genre.map( item => {
                dummyImgLists = this.props.movieImgdetails
                                .filter((imgDetail) => {
                                   return (imgDetail.certificate.toUpperCase() === this.state.value1.toUpperCase());
                                });
            //})
        }
        if((this.state.rating[0] !== 0) && (this.state.rating[1] !== 5)) {
            //this.state.rating.map( item => {
                dummyImgLists = this.props.movieImgdetails
                                .filter((imgDetail) => {
                                   return ((imgDetail.rating >= this.state.rating[0]) && (imgDetail.rating <= this.state.rating[1]));
                                });
            //})
        }
        if(this.state.streamOn) {
            //this.state.rating.map( item => {
                if(this.state.streamOn === 'All'){
                    dummyImgLists = this.props.movieImgdetails
                }
                else{
                    dummyImgLists = this.props.movieImgdetails
                                    .filter((imgDetail) => {
                                       return (imgDetail.movieLink === 'yes');
                                    });
                }
            //})
        }
        if((this.state.year[0] !== 1930) && (this.state.year[1] !== 2019)) {
            //this.state.rating.map( item => {
                dummyImgLists = this.props.movieImgdetails
                                .filter((imgDetail) => {
                                    let year1 = Math.round(this.state.year[0]), year2 = Math.round(this.state.year[1]), imgRelease = new Date(imgDetail.release); 
                                    //console.log(Math.round(this.state.year[0]));
                                    //Math.round(parseInt(value))
                                    //console.log(imgRelease.getFullYear())
                                   return ((imgRelease.getFullYear() >= year1) && (imgRelease.getFullYear() <= year2));
                                });
            //})
        }
       // console.log(dummyImgLists)


    
       if((dummyImgLists) && (!this.state.filteractive)) {
        this.setState({filteractive: true})
       } 
       if((!dummyImgLists) && (this.state.filteractive)) {
        this.setState({filteractive: false})
        }
        this.props.filterCallback(dummyImgLists);
    }
    
    render() {
        //console.log(this.props.items);
        let { rating, year } = this.state;
        return(
            <div className="container-fluid filters pl-0">
                <Form>
                    <Form.Group as={Row} controlId="formPlaintextInput">
                        <Col lg="4">
                            <span className="pt-2">CAST & CREW</span>
                            <div className="d-inline-block ml-3 search-cast autocomplete">                                
                                <div className="selected-items pl-0">

                                    <Chips
                                        value={this.state.chips}
                                        onChange={this.onChange}
                                        //suggestions={this.state.suggestionsArray}
                                        placeholder="Search..."
                                        theme={theme}
                                        chipTheme={chipTheme}
                                        fetchSuggestionsThrushold={5}
                                        fetchSuggestions={(value, callback) => {
                                            this.fetchSuggCall(value, callback)
                                        }}
                                    />
                                    {/* <div className="d-inline-block">
                                        <Form.Control type="text" value={this.state.inputValue} onChange={ (e) => { this.setState({ inputValue: e.target.value}) } } />
                                    </div> */}
                                    {/* <div className="d-inline-block">
                                        <Form.Control type="text" value={this.state.inputValue} onChange={ (e) => { this.setState({ inputValue: e.target.value}) } } />
                                    </div> */}
                                </div>
                                    
                                <Button className="search-btn">
                                    <Search />
                                </Button>
                            </div>
                        </Col>
                        
                        <Col lg="5" className="text-center">
                            <ToggleButtonGroup
                                type="checkbox"
                                value={this.state.genre}
                                onChange={this.handleChange}
                            >
                            {/* value={this.state.value}
                            onChange={this.handleChange} */}
                                <span className="pt-2">GENRE</span>
                                <ToggleButton value="Action">Action</ToggleButton>
                                <ToggleButton value="Comedy">Comedy</ToggleButton>
                                <ToggleButton value="Romance">Romance</ToggleButton>
                                <ToggleButton value="Drama">Drama</ToggleButton>
                                <ToggleButton value="Thriller">Thriller</ToggleButton>
                                <ToggleButton value="More">More</ToggleButton>
                                {/* <ToggleButton value="Adventure">Adventure</ToggleButton>
                                <ToggleButton value="Crime">Crime</ToggleButton>
                                <ToggleButton value="Horror">Horror</ToggleButton> */}
                            </ToggleButtonGroup>
                        </Col>
                        <Col lg="3" className="stream-dropdown text-center">
                            <Dropdown>                               
                                <DropdownButton
                                    title="STREAMING ON"
                                    id="dropdown-menu-align-right"
                                    onSelect={this.streamOnSelect}
                                    >
                                    <Dropdown.Item eventKey="All">All</Dropdown.Item>
                                    <Dropdown.Item eventKey="YouTube">YouTube</Dropdown.Item>
                                    <Dropdown.Item eventKey="Amazon Prime">Amazon Prime</Dropdown.Item>
                                    <Dropdown.Item eventKey="Hotstar">Hotstar</Dropdown.Item>
                                    <Dropdown.Item eventKey="Netflix">Netflix</Dropdown.Item>
                                    <Dropdown.Item eventKey="Sun Nxt">Sun Nxt</Dropdown.Item>
                                    <Dropdown.Item eventKey="Zee5">Zee5</Dropdown.Item>
                                    <Dropdown.Item eventKey="Jio">Jio</Dropdown.Item>
                                    <Dropdown.Item eventKey="Yupp TV">Yupp TV</Dropdown.Item>
                                    <Dropdown.Item eventKey="Viu">Viu</Dropdown.Item>
                                    <Dropdown.Item eventKey="Eros Now">Eros Now</Dropdown.Item>
                                </DropdownButton>
                            </Dropdown>
                        </Col>
                    </Form.Group>


                    <Form.Group as={Row}>
                        
                        <Col lg="3 rating-range">
                            <span className="pt-2">RATING</span>
                            <div className="range-slider ml-3 d-inline-block">                            
                                <Nouislider
                                    connect
                                    start={[rating[0], rating[1]]}
                                    step={0.1}
                                    pips={{ mode: "count", values: 2 }}
                                    clickablePips
                                    range={{
                                    min: 0,
                                    max: 5
                                    }}
                                    tooltips = {[{to: function(value) {
                                        return value;
                                    }}, {to: function(value) {
                                        return value;
                                    }}]}

                                    onSlide={this.onChangeSlideRating}
                                />
                            </div>                        
                        </Col>

                        <Col lg="4" className="ml-lg-4 year-range">
                            <span className="pt-2">YEAR</span>
                            <div className="range-slider ml-3 d-inline-block">
                          
                                <Nouislider
                                    className="YearSlider"
                                    connect
                                    start={[year[0], year[1]]}
                                    step={1}
                                    pips={{ mode: "count", values: 2 }}
                                    clickablePips
                                    range={{
                                    min: 1930,
                                    max: 2020
                                    }}
                                    tooltips = {[{to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}, {to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}]}
                                    
                                    onSlide={this.onChangeSlideYear}
                                   // ref="NoUiSlider"
                                />
                            </div>                        
                        </Col>
                        <Col lg="4" className="ml-3">
                            <span className="pt-2">CERTIFICATE</span>
                            <div key="custom-inline-checkbox" className="d-inline-block checkbox-cont">
                                <Form.Check
                                    custom
                                    inline
                                    label="All"
                                    type="checkbox"
                                    id="custom-inline-checkbox-1"
                                    value="All"
                                    checked={this.state.certificate1}
                                    onChange={this.handleToggle1}
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="U"
                                    type="checkbox"
                                    id="custom-inline-checkbox-2"
                                    value="U"
                                    checked={this.state.certificate2}
                                    onChange={this.handleToggle2}
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="U/A"
                                    type="checkbox"
                                    id="custom-inline-checkbox-3"
                                    value="U/A"
                                    checked={this.state.certificate3}
                                    onChange={this.handleToggle3}
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="A"
                                    type="checkbox"
                                    id="custom-inline-checkbox-4"
                                    value="A"
                                    checked={this.state.certificate4}
                                    onChange={this.handleToggle4}
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="S"
                                    type="checkbox"
                                    id="custom-inline-checkbox-5"
                                    value="S"
                                    checked={this.state.certificate5}
                                    onChange={this.handleToggle5}
                                />
                            </div>
                            
                        </Col>
                    
                    </Form.Group>
                        

                    {/* <Button variant="primary" type="submit">
                        Submit
                    </Button> */}
                </Form>

                    {this.state.filteractive ? (<div className="active-filters mt-4" onChange={this.cheking}>
                    FILTERED BY:
                        {this.state.chips.map( chip => {
                            return (
                                <div className="d-inline-block" key={chip}>
                                    <p
                                        className="px-3 mb-0 active-filter-item"
                                        href="#"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            this.deactiveFilter(chip);
                                        }
                                        }
                                    >
                                        {chip}
                                    </p>
                                </div>
                            )
                        })}

                        {this.state.genre.map( genreItem => {
                            return (   
                                <>                        
                                {(genreItem === 'More') ? null : (
                                <div className="d-inline-block" key={genreItem}>
                                    <p
                                        className="px-3 mb-0 active-filter-item"
                                        href="#"
                                        onClick={event => {
                                            event.preventDefault();
                                            this.deactiveGenre(genreItem);
                                        }}
                                    >
                                        {genreItem}
                                    </p>
                                </div>)}
                                </>
                            )
                        })}

                        {this.state.streamOn ? (
                            <div className="d-inline-block">
                            <p
                                className="px-3 mb-0 active-filter-item"
                                href="#"
                                onClick={event => {
                                    event.preventDefault();
                                    this.setState({ streamOn: null })
                                }}
                            >
                                {this.state.streamOn}
                            </p>
                        </div>
                        ): null}
                        {((this.state.rating[0] !== 0) && (this.state.rating[1] !== 5)) ? (
                            <div className="d-inline-block">
                            <p
                                className="px-3 mb-0 active-filter-item"
                                href="#"
                                onClick={event => {
                                    event.preventDefault();
                                    this.setState({ rating: [0, 5] });
                                }}
                            >
                             Rating {parseFloat(rating[0])}-{parseFloat(rating[1])}
                               
                            </p>
                        </div>
                        ): null}

                        {((this.state.year[0] !== 1930) && (this.state.year[1] !== 2019)) ? (
                            <div className="d-inline-block">
                            <p
                                className="px-3 mb-0 active-filter-item"
                                href="#"
                                onClick={event => {
                                    event.preventDefault();
                                    this.setState({ year: [1930, 2019] });
                                }}
                            >
                                Year {Math.round(this.state.year[0])}-{Math.round(this.state.year[1])}
                            </p>
                        </div>
                        ): null}

                        
                        { this.state.certificate1 ? (
                            <div className="d-inline-block">
                            <p
                                className="px-3 mb-0 active-filter-item"
                                href="#"
                                onClick={event => {
                                    event.preventDefault();
                                    this.setState({ certificate1: false, value1: null });
                                }}
                            >
                                {this.state.value1}
                            </p>
                        </div>
                        ): null}

                        { this.state.certificate2 ? (
                            <div className="d-inline-block">
                            <p
                                className="px-3 mb-0 active-filter-item"
                                href="#"
                                onClick={event => {
                                    event.preventDefault();
                                    this.setState({ certificate2: false, value2: null })
                                }}
                            >
                                {this.state.value2}
                            </p>
                        </div>
                        ): null}

                        { this.state.certificate3 ? (
                            <div className="d-inline-block">
                            <p
                                className="px-3 mb-0 active-filter-item"
                                href="#"
                                onClick={event => {
                                    event.preventDefault();
                                    this.setState({ certificate3: false, value3: null })
                                }}
                            >
                                {this.state.value3}
                            </p>
                        </div>
                        ): null}

                        { this.state.certificate4 ? (
                            <div className="d-inline-block">
                            <p
                                className="px-3 mb-0 active-filter-item"
                                href="#"
                                onClick={event => {
                                    event.preventDefault();
                                    this.setState({ certificate4: false, value4: null })
                                }}
                            >
                                {this.state.value4}
                            </p>
                        </div>
                        ): null}

                        { this.state.certificate5 ? (
                            <div className="d-inline-block">
                            <p
                                className="px-3 mb-0 active-filter-item"
                                href="#"
                                onClick={event => {
                                    event.preventDefault();
                                    this.setState({ certificate5: false, value5: null })
                                }}
                            >
                                {this.state.value5}
                            </p>
                        </div>
                        ): null}
                        
                        <div className="reset-active-filters d-inline-block float-right mr-3"
                        // onClick={() => refine(items)} disabled={!items.length}>
                        >
                        <Reload />
                        RESET
                    </div>
                </div>) : null}
                
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        castImg: state.castImg,
        movieImgdetails: state.imgdetails,
    }
}

export default connect(mapStateToProps)(MoviesFilter);


    // onChange = (e) => {
    //    // console.log(e.target.parentNode)
    //     this.setState({
    //         inputValue: e.target.value
    //     });
    //     if(!e.target.value){
    //         console.log('noo')
    //         return false;
    //     }
    //     let a = document.createElement("DIV");
    //     a.setAttribute("id", this.id+ "autocomplete-list");
    //     a.setAttribute("class", "autocomplete-items");
        
    //     e.target.parentNode.appendChild(a);
    //     //console.log(this.props.imgLists.length)
    //     for(var i=0; i< this.props.imgLists.length; i++){
    //         //console.log(this.props.imgLists[i].title.substr(0, e.target.value.length).toUpperCase())
    //        if(this.props.imgLists[i].title.substr(0, e.target.value.length).toUpperCase() === e.target.value.toUpperCase()){
    //            //console.log('works')
    //             let b = document.createElement("DIV");
    //             //b.setAttribute("onClick", "autocomplete-items");
    //             b.innerHTML = "<string>"+this.props.imgLists[i].title.substr(0, e.target.value.length)+ "</strong>";

    //             b.innerHTML += this.props.imgLists[i].title.substr(e.target.value.length);

    //             b.innerHTML += "<input type='hidden' value='" + this.props.imgLists[i].title+ "'>";

    //             b.addEventListener("click", function() {
    //                 this.setState({
    //                     inputValue: this.getElementsByTagName("input")[0].value
    //                 });
    //                 console.log(this.getElementsByTagName("input")[0].value)
    //             })
    //             a.appendChild(b);
    //        }
    //     }
    // }



    
// const uiSlider = () => {
    
// var pipsSlider = document.getElementById('slider-pips');

// noUiSlider.create(pipsSlider, {
//     range: {
//         min: 0,
//         max: 100
//     },
//     start: [50],
//     pips: {mode: 'count', values: 5}
// });

// }

// var str1="acb", str2="bca", arry1=[], arry2=[];
// for(var i=0;i<str1.length;i++){
//   for(var j=0;j<str2.length;j++){
//     if(str2.includes(str1.charAt(i))){
//       arry1.push(str1.charAt(i));
//     }else{
//             arry2.push(str1.charAt(i));
//     }
//   }
// }
// console.log(arry2);
// console.log(arry2.length);
// if(arry2.length === 0 ){
//     console.log("is twin");
// }else{
// console.log("not twin");
// }
