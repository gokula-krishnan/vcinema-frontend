import React from 'react';
import {connect} from 'react-redux';
import base from '../../api/base';
import { Form, Button, Row, Col, ToggleButtonGroup, ToggleButton, Dropdown, DropdownButton } from 'react-bootstrap';

import {CustomRangeSlider} from './CustomRangeSlider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch,Index, SearchBox,Configure, Panel, RefinementList, MenuSelect, connectHits,  connectSearchBox } from 'react-instantsearch-dom';
import { fetchStreams, movieStatic } from '../../actions';
//import MultiSearch from './MultiSearch';
 import MultiSearch from './videoMultiSearch';
import { CustomCurrentRefinements } from './ActiveGalleryFilter';


const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');
const index = searchClient.initIndex('AZ_ARTIST_SEARCH');
const mvindex = searchClient.initIndex('AZ_MOVIE_SEARCH');
const videoIndex = searchClient.initIndex('AZ_VIDEO_SEARCH');


class VideoFilter extends React.Component {

    state = {
        searchArray: [],
        expanded: false,
        calendar: [1930, 2019],
        rating: [0, 5],
        selectedEvent : 'Audio Launch /Pre-Release',
        selectedTrend : 'Trending'
    }


    searchActive = (searchArray) => {
        this.setState({searchArray})
    }
    trendOnSelect = (e) => {
        this.setState({selectedTrend: e.toUpperCase()})
        //this.props.streamActive(e);
    }
    sliderHandler = (arr) => {
       console.log(arr)
      
            this.setState({calendar: arr})
      
    }

    eventOnSelect = (e) => {
        this.setState({selectedEvent: e.toUpperCase()})
        //this.props.streamActive(e);
    }

    searchRefine = (name) => {
        this.setState(({searchArray}) => ({searchArray: searchArray.filter( item => item !==name )}));
    }

    eventOnRefine = (e) => {
        this.setState({selectedEvent: "AUDIO LAUNCH /PRE-RELEASE"})
    }

    trendOnRefine = (e) => {
        this.setState({selectedTrend: "TRENDING"})
    }

    render() {
       
        return(
            <div className="container-fluid filters pl-0">

        
                <InstantSearch searchClient={searchClient} indexName="AZ_MOVIE_SEARCH">

                    <div className="content-wrapper">
                        <Facets 
                            update={this.props.update} 
                            searchActive={this.searchActive} 
                            searchArray={this.state.searchArray}
                            eventOnSelect={this.eventOnSelect}
                            selectedEvent={this.state.selectedEvent}
                            trendOnSelect={this.trendOnSelect}
                            selectedTrend={this.state.selectedTrend}
                            expanded={this.state.expanded}
                            sliderHandler={this.sliderHandler}
                            calendar={this.state.calendar}
                        />  
                        <CustomHits />
                    </div> 
                    <CustomCurrentRefinements 
                        searchRefine={this.searchRefine} 
                        searchArray={this.state.searchArray} 
                        selectedEvent={this.state.selectedEvent} 
                        selectedTrend={this.state.selectedTrend} 
                        eventOnRefine={this.eventOnRefine}
                        trendOnRefine={this.trendOnRefine}
                        checkItemsLength={this.props.checkItemsLength}
                    />
                </InstantSearch>
               
            </div>
        )
    }

}


const Hits = ({hits}) => (
  //  console.log(hits)
    <ol>
    {hits.map(hit => (
      <li key={hit.objectID}>{hit.name}</li>
    ))}
  </ol>
)



const CustomHits = connectHits(Hits);


const Facets = ({update, searchActive, searchArray,trendOnSelect,eventOnSelect, selectedTrend, selectedEvent, gnreExpand, expanded, sliderHandler, calendar}) => {
    //console.log(year)
    return(
        <section className="movie-facet-wrapper movie-video-filter container-fluid">
            
            <div className="row">

            <Panel header={<span className="mr-2">SEARCH:</span>} className={expanded ? "expanded-one col-4" : "col-4"}>
                <MultiSearch update={update} searchActive={searchActive} searchArray={searchArray}/>
            </Panel>

                <Panel header={<span className="mr-2">CALENDAR:</span>} className="col-md-4">
                    <CustomRangeSlider
                        attribute="calendar"
                        min={1930}
                        max={2019}
                        values={[1930,2019]}
                        tooltip = {[{to: function(value) {
                            return Math.round(parseInt(value));
                        }}, {to: function(value) {
                            return Math.round(parseInt(value));
                        }}]}
                        sliderHandler={sliderHandler}
                        calendar={calendar}
                    />
                </Panel>

                <Panel className={expanded ? "expanded-three col-3" : "stream-dropdown col-md-2"}>
                    <Dropdown>                               
                        <DropdownButton
                            title={selectedEvent}
                            id="dropdown-menu-align-right"
                            onSelect={eventOnSelect}
                            >
                            <Dropdown.Item eventKey="Audio Launch /Pre-Release">Audio Launch /Pre-Release</Dropdown.Item>
                            <Dropdown.Item eventKey="Press Meet/ Promotions">Press Meet/ Promotions</Dropdown.Item>
                            <Dropdown.Item eventKey="Personal">Personal</Dropdown.Item>
                            <Dropdown.Item eventKey="Branding">Branding</Dropdown.Item>
                            <Dropdown.Item eventKey="Performances / Concert">Performances / Concert</Dropdown.Item>
                            <Dropdown.Item eventKey="Page 3">Page 3</Dropdown.Item>
                            <Dropdown.Item eventKey="Awards">Awards</Dropdown.Item>
                            <Dropdown.Item eventKey="Others">Others</Dropdown.Item>
                        </DropdownButton>
                    </Dropdown>
                </Panel>
                <Panel className={expanded ? "expanded-three col-3" : "stream-dropdown col-1"}> 
                    <Dropdown>                               
                        <DropdownButton
                            title={selectedTrend}
                            id="dropdown-menu-align-right"
                            onSelect={trendOnSelect}
                            >
                            <Dropdown.Item eventKey="Trending">Trending</Dropdown.Item>
                            <Dropdown.Item eventKey="Latest">Latest</Dropdown.Item>
                            <Dropdown.Item eventKey="Most Viewed">Most Viewed</Dropdown.Item>
                           
                        </DropdownButton>
                    </Dropdown>
                </Panel>
            </div>
        </section>
    )
}

const mapStateToProps = (state) => {

    console.log(state);
    return {
       
    }
}
export default connect(mapStateToProps, {})(VideoFilter);
