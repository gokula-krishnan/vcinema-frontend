import React from 'react'
import ReactDOM from 'react-dom'
import ReactTags from 'react-tag-autocomplete'
import Slider from 'react-rangeslider'
import './filters.scss';
import {connect} from 'react-redux';

class FilterSearch extends React.Component {    
  constructor (props) {
    super(props)

    this.state = {
      tags: [
        { id: 1, name: "Apples" },
        { id: 2, name: "Pears" }
      ],
      suggestions: [
        { id: 3, name: "Bananas" },
        { id: 4, name: "Mangos" },
        { id: 5, name: "Lemons" },
        { id: 6, name: "Apricots" }
      ]
    }
  }
  

  handleDelete (i) {
    const tags = this.state.tags.slice(0)
    tags.splice(i, 1)
    this.setState({ tags })
  }

  handleAddition (tag) {
    const tags = [].concat(this.state.tags, tag)
    this.setState({ tags })
    console.log(tags)
  }

  suggestions (i) {
    const suggestions = [
        { id: 3, name: 'Bananas' },
        { id: 4, name: 'Mangos' },
        { id: 5, name: 'Lemons' },
        { id: 6, name: 'Apricots', disabled: true }
      ]
  }
  
  render () {
    return (
        <section className="movie-facet-wrapper movie-video-filter container-fluid">            
            <div className="row">
                <div className="col-12 col-md-3 input-group input-group-sm">
                    <label htmlFor="ccSearch" className="text-uppercase input-group-prepend mr-2">Search:</label>
                    <ReactTags
                        tags={this.state.tags}
                        suggestions={this.state.suggestions}
                        handleDelete={this.handleDelete.bind(this)}
                        handleAddition={this.handleAddition.bind(this)}
                        placeholder="Search for videos" id="ccSearch" 
                    />
                </div>
                <div className="col-12 col-md-5">
                    <Slider />
                </div>
            </div>
        </section>      
    )
  }
}

const mapStateToProps = (state) => {

    console.log(state);
    return {
       
    }
}

export default connect(mapStateToProps, {})(FilterSearch);
