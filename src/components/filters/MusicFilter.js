import './filters.scss';
import React from 'react';
import {connect} from 'react-redux';
//import { Form, Button, Row, Col, ToggleButtonGroup, ToggleButton, Dropdown, DropdownButton } from 'react-bootstrap';
//import { Search } from '../../svgIcons';
//import Nouislider from 'react-nouislider';
//import HitComponent from './HitComponent';//CustomHits
import {CustomHits} from './HitComponent';//CustomHits
import {CustomRangeSlider} from './CustomRangeSlider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, SearchBox, Configure, Panel, RefinementList, MenuSelect } from 'react-instantsearch-dom';
import { fetchStreams } from '../../actions';
import {CustomMultiSearch} from './MultiSearch';
import {CustomCurrentRefinements} from './ActiveFilters';

const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');


class MusicFilter extends React.Component {
        
    // onChangeSlideRating = (e) => {
    //     console.log(e) // logs the value this.refs.NoUiSlider.slider.get()
    // }
    // onChangeSlideYear = (e) => {
    //     console.log(e) // logs the value
    // }
    apicall = (url) => {
        //console.log('api')
        this.props.fetchStreams(url);
    }
    render() {
        return(
            <div className="container-fluid filters">
                <InstantSearch searchClient={searchClient} indexName="AZ_ALBUM_SEARCH">
                    <Configure hitsPerPage={16} /> 
                    <div className="content-wrapper">
                        <Facets /> 
                        <CustomHits apicall={this.apicall} fixedNavlink='album'/>
                    </div>
                    <CustomCurrentRefinements checkItemsLength={this.props.checkItemsLength}/>                   
                </InstantSearch>                
            </div>
        )
    }
}


const Facets = () => {
    return(
        <section className="music-facet-wrapper">
            <section className="row mb-2">
                <Panel header={<span className="mr-2">CAST&CREW:</span>} className="col-4">
                    {/* <CustomMultiSearch attribute="name" operator="or" /> */}
                    {/* <SearchBox /> */}
                </Panel>
                <Panel header={<span className="mr-2">CALENDER:</span>} className="">
                    <CustomRangeSlider 
                        attribute="name"
                        min={1930}
                        max={2019}
                        values={[1930, 2019]}
                        tooltip = {[{to: function(value) {
                            return Math.round(parseInt(value));
                        }}, {to: function(value) {
                            return Math.round(parseInt(value));
                        }}]}
                    />
                </Panel>
                <Panel header={<span className="mr-2">RATING:</span>} className="">
                    <CustomRangeSlider 
                        attribute="name"
                        min={0}
                        max={5}
                        values={[0, 5]}
                        tooltip = {[{to: function(value) {
                            return value;
                        }}, {to: function(value) {
                            return value;
                        }}]}
                    />
                </Panel>
            </section>
            {/* <section className="row mb-2">
                <Panel header={<span className="mr-2">AGE:</span>} className="col-3">
                    <CustomRangeSlider 
                        attribute="age"
                        min={0}
                        max={100}
                        values={[0, 100]}
                        tooltip = {[{to: function(value) {
                            return Math.round(parseInt(value));
                        }}, {to: function(value) {
                            return Math.round(parseInt(value));
                        }}]}
                    />
                </Panel>
                <Panel header={<span className="mr-2">YEAR:</span>} className="col-5">
                    <CustomRangeSlider 
                        attribute="age"
                        min={1930}
                        max={2019}
                        values={[1930,2019]}
                        tooltip = {[{to: function(value) {
                            return Math.round(parseInt(value));
                        }}, {to: function(value) {
                            return Math.round(parseInt(value));
                        }}]}
                    />
                </Panel>
            
                <Panel header={<span className="mr-2">GENDER:</span>} className="col-4">
                    <RefinementList attribute="gender" operator="or" />
                </Panel>
            </section> */}

        </section>
    )
}

export default connect(null, {fetchStreams})(MusicFilter);

