import React from 'react';
import {connect} from 'react-redux';
import base from '../../api/base';
import { Form, Button, Row, Col, ToggleButtonGroup, ToggleButton, Dropdown, DropdownButton } from 'react-bootstrap';

import {CustomRangeSlider} from './CustomRangeSlider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, Configure, Panel, RefinementList, MenuSelect, connectHits, connectSearchBox } from 'react-instantsearch-dom';
import { fetchStreams, movieStatic } from '../../actions';
import MultiSearch from './MultiSearch';
import { CustomCurrentRefinements } from './ActiveFilters';


const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');
const index = searchClient.initIndex('AZ_ARTIST_SEARCH');
const mvindex = searchClient.initIndex('AZ_MOVIE_SEARCH');
const videoIndex = searchClient.initIndex('AZ_VIDEO_SEARCH');


class VideoFilter extends React.Component {

    state = {
        searchArray: [],
        expanded: false,
        year: [1930, 2019],
        rating: [0, 5]
    }


    searchActive = (searchArray) => {
        this.setState({searchArray})
    }
  
    streamOnSelect = (e) => {
        this.setState({selectedStream: e.toUpperCase()})
        //this.props.streamActive(e);
    }

    render() {
       
        return(
            <div className="container-fluid filters">
                <InstantSearch searchClient={searchClient} indexName="AZ_MOVIE_SEARCH">
                    <div className="content-wrapper">
                        <Facets 
                            update={this.props.update} 
                            searchActive={this.searchActive} 
                            searchArray={this.state.searchArray}
                            streamOnSelect={this.streamOnSelect}
                            selectedStream={this.state.selectedStream}
                            expanded={this.state.expanded}
                            sliderHandler={this.sliderHandler}
                            year={this.state.year}
                        /> 
                        <CustomHits />
                    </div> 
                    {/* <CustomCurrentRefinements 
                        searchRefine={this.searchRefine} 
                        searchArray={this.state.searchArray} 
                        selectedStream={this.state.selectedStream} 
                        streamOnRefine={this.streamOnRefine}
                        checkItemsLength={this.props.checkItemsLength}
                    /> */}
                </InstantSearch>
               
            </div>
        )
    }

}


const Hits = ({hits}) => {
    return true;
}

const CustomHits = connectHits(Hits);


const Facets = ({update, searchActive, searchArray,streamOnSelect, selectedStream, gnreExpand, expanded, sliderHandler, year, rating}) => {
    //console.log(year)
    return(
        <section className="movie-facet-wrapper">
            
            <section className="row">

            <Panel header={<span className="mr-2">CAST &amp; CREW:</span>} className={expanded ? "expanded-one col-5" : "col-md-3"}>
                <MultiSearch update={update} searchActive={searchActive} searchArray={searchArray}/>
            </Panel>

                <Panel header={<span className="mr-0">CALENDAR:</span>} className="col-md-4">
                    <CustomRangeSlider 
                        attribute="year"
                        min={1930}
                        max={2019}
                        values={[1930,2019]}
                        tooltip = {[{to: function(value) {
                            return Math.round(parseInt(value));
                        }}, {to: function(value) {
                            return Math.round(parseInt(value));
                        }}]}
                        sliderHandler={sliderHandler}
                        year={year}
                    />
                </Panel>
                <Panel className={expanded ? "expanded-three col-3" : "stream-dropdown col-md-3"}>
                    <Dropdown>                               
                        <DropdownButton
                            title={selectedStream}
                            id="dropdown-menu-align-right"
                            >
                            <Dropdown.Item eventKey="YouTube">Audio Launch</Dropdown.Item>
                            <Dropdown.Item eventKey="Amazon Prime">Amazon Prime</Dropdown.Item>
                            <Dropdown.Item eventKey="Hotstar">Hotstar</Dropdown.Item>
                            <Dropdown.Item eventKey="Netflix">Netflix</Dropdown.Item>
                            <Dropdown.Item eventKey="Sun Nxt">Sun Nxt</Dropdown.Item>
                            <Dropdown.Item eventKey="Zee5">Zee5</Dropdown.Item>
                            <Dropdown.Item eventKey="Jio">Jio</Dropdown.Item>
                            <Dropdown.Item eventKey="Yupp TV">Yupp TV</Dropdown.Item>
                            <Dropdown.Item eventKey="Viu">Viu</Dropdown.Item>
                            <Dropdown.Item eventKey="Eros Now">Eros Now</Dropdown.Item>
                        </DropdownButton>
                    </Dropdown>
                </Panel>
                <Panel className={expanded ? "expanded-three col-3" : "stream-dropdown col-md-2"}>
                    <Dropdown>                               
                        <DropdownButton
                            title={selectedStream}
                            id="dropdown-menu-align-right"
                            >
                            <Dropdown.Item eventKey="YouTube">Trending</Dropdown.Item>
                            <Dropdown.Item eventKey="Amazon Prime">Latest</Dropdown.Item>
                            <Dropdown.Item eventKey="Hotstar">Most Viewed</Dropdown.Item>
                           
                        </DropdownButton>
                    </Dropdown>
                </Panel>
            </section>
        </section>
    )
}

const mapStateToProps = (state) => {
    return {
       
    }
}
export default connect(mapStateToProps, {})(VideoFilter);
