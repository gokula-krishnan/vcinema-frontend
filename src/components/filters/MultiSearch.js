
import React from 'react';
import base from '../../api/base';
import {Search} from '../../svgIcons';
import Chips, { Chip } from 'react-chips';
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, connectHits, connectSearchBox, Configure } from 'react-instantsearch-dom';
import {CustomHitsComp} from './ReactChips';
const theme = {
    chipsContainer: {
      display: "flex",
      position: "relative",
      border: "1px solid #343a40",
      backgroundColor: '#343a40',
      font: "10px Arial",
      alignItems: "center",
      flexWrap: "wrap",
      padding: "2px",
      borderRadius: 1,
      ':focus': {
          border: "1px solid #aaa",
      }
    },
    container:{
      flex: 1,
    },
    containerOpen: {
  
    },
    input: {
      border: 'none',
      outline: 'none',
      boxSizing: 'border-box',
      width: '100%',
      padding: 2,
      margin: 0,
      backgroundColor: '#343a40',
      color: '#fff'
    },
    suggestionsContainer: {
    },
    suggestionsList: {
      position: 'absolute',
      border: '1px solid #343a40',
      zIndex: 10,
      left: 0,
      top: '120%',
      width: '100%',
      backgroundColor: '#343a40',
      listStyle: 'none',
      padding: 0,
      margin: 0,
    },
    suggestion: {
      padding: '5px 15px'
    },
    suggestionHighlighted: {
      color: '#00c1ff'
    },
    sectionContainer: {
  
    },
    sectionTitle: {
  
    },
  }
  
const chipTheme = {
    chip: {
      padding: '2px 4px',
      background: "#1d181c",
      margin: "0px",
      borderRadius: 3,
      cursor: 'pointer',
      color: '#00c1ff',
    },
    chipSelected: {
      background: '#888',
    },
    chipRemove: {
      fontWeight: "bold",
      display: 'none',
      cursor: "pointer",
      ':hover': {
        color: 'red',
      }
    }
  }

const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');
const index = searchClient.initIndex('AZ_ARTIST_SEARCH');
 
class MultiSearch extends React.Component {
 state = {
    inputVal: null,
    suggestionsArray: null,
    suggHits: null,
    Hits: []
  }

  onChange = (chips) => {    
    this.props.update(chips);
    this.props.searchActive(chips);
  }

  fetchSuggCall = (hits) => { 
    this.setState({ 
      suggestionsArray: hits.map( hit => hit.name), 
      suggHits: hits 
    })
  }
  inpUpdate = (val) => {
    this.setState({inputVal: val})
    //console.log(this.state.suggestionsArray)
  }
  callbackFunc = (callback) => {
    setTimeout(() => {
      return callback(this.state.suggestionsArray)
    }, 1000)
  }
  // onChipAdd = (val) => {
  //   return (<span>{val}</span>);
  // }
  // onChipRemove = (val) => {

  // }
  render() {
    let { suggestionsArray, inputVal } = this.state;
    let {searchArray} = this.props;
    return (
      <>
        {/* <InstantSearch searchClient={searchClient} indexName="AZ_ARTIST_SEARCH">
          <CustomSearchBox chips={chips} sugg={suggestionsArray} onChange={this.onChange} />
          <CustomHitsComp fetchSuggCall={this.fetchSuggCall} />
        </InstantSearch>  */}
        <InstantSearch searchClient={searchClient} indexName="AZ_ARTIST_SEARCH">
          <Configure hitsPerPage={5} /> 
          <CustomSearchBox 
          onChipRemove={this.onChipRemove} 
          onChipAdd={this.onChipAdd} 
          chips={searchArray} 
          sugg={suggestionsArray} 
          onChange={this.onChange} 
          inpUpdate={this.inpUpdate} 
          callbackFunc={this.callbackFunc}/>

          <CustomHitsComp 
          fetchSuggCall={this.fetchSuggCall} 
          inpVal={inputVal} />
        </InstantSearch>
      </>
    );
  }
}

//const Hit = ({ hit }) => fetchSuggCall;

// const Hits = ({hits, fetchSuggCall,  currentRefinement}) => {
//   console.log(currentRefinement)
//   fetchSuggCall(hits)
//   return true;
// };

// const CustomHits = connectHits(Hits);

const SearchBox = ({chips, sugg, onChange,  refine,inpUpdate, callbackFunc, onChipAdd, onChipRemove}) => {
  //console.log(sugg)
    return (
    <div className="facet-wrapper-search">
      <Chips
          value={chips}
          //renderChip={onChipAdd}
          onChange={onChange} 
         //onRemove={onChipRemove}
          //suggestions={sugg}          
          theme={theme}
          chipTheme={chipTheme} 
          //fetchSuggestionsThrushold={10}
          placeholder="Search..."
          fetchSuggestions={(value, callback) =>{
            refine(value);
            inpUpdate(value);
            callbackFunc(callback);
          }}
          //suggestionsFilter={ value => console.log(value)}
        />
        <Search />
    </div>
  )
};


const CustomSearchBox = connectSearchBox(SearchBox);

export default MultiSearch;
