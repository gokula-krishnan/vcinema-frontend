import './filters.scss';
import React from 'react';
import {connect} from 'react-redux';
import { Form, Button, Row, Col, ToggleButtonGroup, ToggleButton, Dropdown, DropdownButton } from 'react-bootstrap';
import { Search } from '../../svgIcons';
import Nouislider from 'react-nouislider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';



class MoviesFilter extends React.Component {
    state = {
        inputValue: ""
    }
    
    onChangeSlideRating = (e) => {
        console.log(e) // logs the value this.refs.NoUiSlider.slider.get()
    }
    onChangeSlideYear = (e) => {
        console.log(e) // logs the value
    }
    onChange = (e) => {
       // console.log(e.target.parentNode)
        this.setState({
            inputValue: e.target.value
        });
        if(!e.target.value){
            console.log('noo')
            return false;
        }
        let a = document.createElement("DIV");
        a.setAttribute("id", this.id+ "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        
        e.target.parentNode.appendChild(a);
        //console.log(this.props.imgLists.length)
        for(var i=0; i< this.props.imgLists.length; i++){
            //console.log(this.props.imgLists[i].title.substr(0, e.target.value.length).toUpperCase())
           if(this.props.imgLists[i].title.substr(0, e.target.value.length).toUpperCase() === e.target.value.toUpperCase()){
               //console.log('works')
                let b = document.createElement("DIV");
                //b.setAttribute("onClick", "autocomplete-items");
                b.innerHTML = "<string>"+this.props.imgLists[i].title.substr(0, e.target.value.length)+ "</strong>";

                b.innerHTML += this.props.imgLists[i].title.substr(e.target.value.length);

                b.innerHTML += "<input type='hidden' value='" + this.props.imgLists[i].title+ "'>";

                b.addEventListener("click", function() {
                    this.setState({
                        inputValue: this.getElementsByTagName("input")[0].value
                    });
                    console.log(this.getElementsByTagName("input")[0].value)
                })
                a.appendChild(b);
           }
        }
    }
    suggestionsClicked = (e) => {
        this.setState({
            inputValue: ""
        });                
    }

    suggestions = () => {
        const arr = [];
        for( var i=0; i< this.props.imgLists.length; i++){
                if(this.props.imgLists[i].title.substr(0, this.state.inputValue.length).toUpperCase() === this.state.inputValue.toUpperCase()){ 
                    arr.push(this.props.imgLists[i].title);
                }
        }
        
        return arr.map( sugg => {
            return(
                <div key={sugg} onClick={this.suggestionsClicked}>
                    {/* <strong>{sugg.substr(0, this.state.inputValue.length)}</strong>{sugg.substr(this.state.inputValue.length)} */}
                    {sugg}
                    <input type='hidden' value={sugg}></input>
                </div>
            )
        })
    }
    render() {
        //console.log(this.props.imgLists);
        return(
            <div className="container-fluid filters">
                <Form>
                    <Form.Group as={Row} controlId="formPlaintextInput">
                        <Col sm="4">
                            <span className="pt-2">CAST & CREW</span>
                            <div className="d-inline-block ml-3 search-cast autocomplete">
                                
                                <div id="autocomplete-list" className="autocomplete-items">
                                    {this.state.inputValue ? this.suggestions() : null}
                                </div>
                                
                                
                                
                                <div className="selected-items pl-0">
                                    <div className="d-inline-block">
                                        <Form.Control type="text" value={this.state.inputValue} onChange={ (e) => { this.setState({ inputValue: e.target.value}) } } />
                                    </div>
                                    {/* <div className="d-inline-block">
                                        <Form.Control type="text" value={this.state.inputValue} onChange={ (e) => { this.setState({ inputValue: e.target.value}) } } />
                                    </div> */}
                                </div>
                                    
                                <Button className="search-btn">
                                    <Search />
                                </Button>
                            </div>
                        </Col>
                        
                        <Col sm="5" className="text-center">
                            <ToggleButtonGroup
                                type="checkbox"
                            >
                            {/* value={this.state.value}
                            onChange={this.handleChange} */}
                                <span className="pt-2">GENRE</span>
                                <ToggleButton value="Action">Action</ToggleButton>
                                <ToggleButton value="Comedy">Comedy</ToggleButton>
                                <ToggleButton value="Romance">Romance</ToggleButton>
                                <ToggleButton value="Drama">Drama</ToggleButton>
                                <ToggleButton value="Thriller">Thriller</ToggleButton>
                                <ToggleButton value="More">More</ToggleButton>
                                {/* <ToggleButton value="Adventure">Adventure</ToggleButton>
                                <ToggleButton value="Crime">Crime</ToggleButton>
                                <ToggleButton value="Horror">Horror</ToggleButton> */}
                            </ToggleButtonGroup>
                        </Col>
                        <Col sm="3" className="stream-dropdown text-center">
                            <Dropdown>
                               
                                <DropdownButton
                                    title="STREAMING ON"
                                    id="dropdown-menu-align-right"
                                    >
                                    <Dropdown.Item eventKey="1">All</Dropdown.Item>
                                    <Dropdown.Item eventKey="2">YouTube</Dropdown.Item>
                                    <Dropdown.Item eventKey="3">Amazon Prime</Dropdown.Item>
                                    <Dropdown.Item eventKey="4">Hotstar</Dropdown.Item>
                                    <Dropdown.Item eventKey="5">Netflix</Dropdown.Item>
                                    <Dropdown.Item eventKey="6">Sun Nxt</Dropdown.Item>
                                    <Dropdown.Item eventKey="7">Zee5</Dropdown.Item>
                                    <Dropdown.Item eventKey="8">Jio</Dropdown.Item>
                                    <Dropdown.Item eventKey="9">Yupp TV</Dropdown.Item>
                                    <Dropdown.Item eventKey="10">Viu</Dropdown.Item>
                                    <Dropdown.Item eventKey="11">Eros Now</Dropdown.Item>
                                </DropdownButton>
                            </Dropdown>
                        </Col>
                    </Form.Group>


                    <Form.Group as={Row}>
                        
                        <Col sm="3">
                            <span className="pt-2">RATING</span>
                            <div className="range-slider w-75 ml-3 d-inline-block">                            
                                <Nouislider
                                    connect
                                    start={[0, 5]}
                                    step={0.1}
                                    pips={{ mode: "count", values: 2 }}
                                    clickablePips
                                    range={{
                                    min: 0,
                                    max: 5
                                    }}
                                    tooltips = {[{to: function(value) {
                                        return value.toFixed(1);
                                    }}, {to: function(value) {
                                        return value.toFixed(1);
                                    }}]}

                                    onSlide={this.onChangeSlideRating}
                                />
                            </div>                        
                        </Col>

                        <Col sm="4" className="ml-4">
                            <span className="pt-2">YEAR</span>
                            <div className="range-slider ml-3 d-inline-block">
                          
                                <Nouislider
                                    className="YearSlider"
                                    connect
                                    start={[1930, 2020]}
                                    step={1}
                                    pips={{ mode: "count", values: 2 }}
                                    clickablePips
                                    range={{
                                    min: 1930,
                                    max: 2020
                                    }}
                                    tooltips = {[{to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}, {to: function(value) {
                                        return Math.round(parseInt(value));
                                    }}]}
                                    
                                    onSlide={this.onChangeSlideYear}
                                   // ref="NoUiSlider"
                                />
                            </div>                        
                        </Col>
                        <Col sm="4" className="ml-3">
                            <span className="pt-2">CERTIFICATE</span>
                            <div key="custom-inline-checkbox" className="d-inline-block checkbox-cont">
                                <Form.Check
                                    custom
                                    inline
                                    label="All"
                                    type="checkbox"
                                    id="custom-inline-checkbox-1"
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="U"
                                    type="checkbox"
                                    id="custom-inline-checkbox-2"
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="U/A"
                                    type="checkbox"
                                    id="custom-inline-checkbox-3"
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="A"
                                    type="checkbox"
                                    id="custom-inline-checkbox-4"
                                />
                                <Form.Check
                                    custom
                                    inline
                                    label="S"
                                    type="checkbox"
                                    id="custom-inline-checkbox-5"
                                />
                            </div>
                            
                        </Col>
                    
                    </Form.Group>
                        

                    {/* <Button variant="primary" type="submit">
                        Submit
                    </Button> */}
                </Form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        imgLists: state.imgLists
    }
}

export default connect(mapStateToProps)(MoviesFilter);


// const uiSlider = () => {
    
// var pipsSlider = document.getElementById('slider-pips');

// noUiSlider.create(pipsSlider, {
//     range: {
//         min: 0,
//         max: 100
//     },
//     start: [50],
//     pips: {mode: 'count', values: 5}
// });

// }

// var str1="acb", str2="bca", arry1=[], arry2=[];
// for(var i=0;i<str1.length;i++){
//   for(var j=0;j<str2.length;j++){
//     if(str2.includes(str1.charAt(i))){
//       arry1.push(str1.charAt(i));
//     }else{
//             arry2.push(str1.charAt(i));
//     }
//   }
// }
// console.log(arry2);
// console.log(arry2.length);
// if(arry2.length === 0 ){
//     console.log("is twin");
// }else{
// console.log("not twin");
// }
