import React from 'react';
import { connectHits } from 'react-instantsearch-dom';
 

class HitsComp extends React.Component {
    state = {
        hits: []
    }
    componentDidMount(){
       
        this.props.fetchSuggCall(this.props.hits);      
    }
    componentDidUpdate(){
        let{fetchSuggCall, hits} = this.props;

        console.log(fetchSuggCall);
        console.log(hits)
        if (JSON.stringify(hits) !== JSON.stringify(this.state.hits)) {
           
            fetchSuggCall(this.props.hits);  
            this.setState({ hits })
        }   
        // if(hits.length !== this.state.hits.length){
        //     this.setState({ hits})
        //     fetchSuggCall(hits);        
        // }
    }
    render() {
        return true;
    }
} 
export const CustomHitsComp = connectHits(HitsComp);