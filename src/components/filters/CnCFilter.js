import './filters.scss';
import React from 'react';
import {connect} from 'react-redux';
//import { Form, Button, Row, Col, ToggleButtonGroup, ToggleButton, Dropdown, DropdownButton } from 'react-bootstrap';
//import { Search } from '../../svgIcons';
//import Nouislider from 'react-nouislider';
//import HitComponent from './HitComponent';//CustomHits
//import {CustomHits} from './HitComponent';//CustomHits
import {CustomRangeSlider} from './CustomRangeSlider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, connectHits, Panel, RefinementList, MenuSelect } from 'react-instantsearch-dom';
import { fetchStreams } from '../../actions';
import {CustomCurrentRefinements} from './ActiveFilters';

const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');


class CnCFilter extends React.Component {
    
    // onChangeSlideRating = (e) => {
    //     console.log(e) // logs the value this.refs.NoUiSlider.slider.get()
    // }
    // onChangeSlideYear = (e) => {
    //     console.log(e) // logs the value
    // }
    // apicall = (url) => {
    //     //console.log('api')
    //     this.props.fetchStreams(url);
    // }
    render() {
        return(
            <div className="container-fluid filters">
                <InstantSearch searchClient={searchClient} indexName="AZ_ARTIST_SEARCH">
                    {/* <SearchBox /> */}
                    {/* <Hits hitComponent={HitComponent}/>   */}

                    <div className="content-wrapper">
                        <Facets /> 
                        <CustomHits apicall={this.apicall} fixedNavlink='artists'/>
                    </div>
                    {/* <CustomCurrentRefinements checkItemsLength={this.props.checkItemsLength}/>                    */}
                </InstantSearch>                
            </div>
        )
    }
}

const Hits = () => {
    return true;
}

const CustomHits = connectHits(Hits);

const Facets = () => {
    return(
        <section className="cnc-facet-wrapper">
            <section className="row mb-4">
                <Panel header={<span>PROFFESSION:</span>}>
                    <RefinementList attribute="industries" operator="or" limit={5} showMore 
                        translations={{
                            showMore(expanded) {
                            return expanded ? 'Less' : 'More';
                            },
                            noResults: 'No results',
                        }}
                        transformItems={items =>
                            items.map(item => ({
                            ...item,
                            label: item.label.toLowerCase(),
                        }))}                        
                        />
                </Panel>

                <Panel>
                    <MenuSelect attribute="sun_sign" 
                    translations={{
                        seeAllOption: 'ZODIAC',
                    }}
                    />
                </Panel>
                <Panel >
                    <MenuSelect attribute="industries" 
                    translations={{
                        seeAllOption: 'INDUSTRIES',
                    }}
                    />
                </Panel>
                <Panel>
                    <MenuSelect attribute="sun_sign" 
                    translations={{
                        seeAllOption: 'STATS',
                    }}
                    />
                </Panel>
                <Panel>
                    <MenuSelect attribute="sun_sign" 
                    translations={{
                        seeAllOption: 'SOCIAL',
                    }}
                    />
                </Panel>
            </section>
            <section className="row mb-2">
                <Panel header={<span className="mr-2">AGE:</span>} className="col-3">
                    <CustomRangeSlider 
                        attribute="age"
                        min={0}
                        max={100}
                        values={[0, 100]}
                        tooltip = {[{to: function(value) {
                            return Math.round(parseInt(value));
                        }}, {to: function(value) {
                            return Math.round(parseInt(value));
                        }}]}
                    />
                </Panel>
                <Panel header={<span className="mr-2">YEAR:</span>} className="col-5">
                    <CustomRangeSlider 
                        attribute="age"
                        min={1930}
                        max={2019}
                        values={[1930,2019]}
                        tooltip = {[{to: function(value) {
                            return Math.round(parseInt(value));
                        }}, {to: function(value) {
                            return Math.round(parseInt(value));
                        }}]}
                    />
                </Panel>
            
                <Panel header={<span className="mr-2">GENDER:</span>} className="col-4">
                    <RefinementList attribute="gender" operator="or" />
                </Panel>
            </section>

        </section>
    )
}

export default connect(null, {fetchStreams})(CnCFilter);

