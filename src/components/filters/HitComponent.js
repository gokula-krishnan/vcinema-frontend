
import React from 'react';
import { connectHits, connectInfiniteHits } from 'react-instantsearch-dom';
import Nouislider from 'react-nouislider';
import 'nouislider';
import 'nouislider/distribute/nouislider.css';


let checkStr;

const getData = (apicall, uuidArry, fixedNavlink, filterString) => {
    let str = uuidArry.join(), 
    url;
    
    if((str !== checkStr) && (fixedNavlink === 'movies')){
        filterString(str);
    }
}

const Hits = ({hits, apicall, fixedNavlink, filterString}) => {
    //console.log(hits);
    const uuidArry = [];
    if(hits.length){
        for(var i=0;i<hits.length;i++){
            uuidArry.push(hits[i].uuid);
        }
        getData(apicall, uuidArry, fixedNavlink, filterString);
    }
    return true;
}

export const CustomHits = connectHits(Hits);

// const CurrentRefinement = ({items, refine, checkItemsLength}) => {
    
//     return(
//         items.length ?
//         (<section className="active-filters mt-4">
//         {checkItemsLength(items)}
//                 FILTERED BY:
//             {items.map(item => (
//                 <div key={item.currentRefinement[0]} className="d-inline-block">
//                 {item.items ? (
//                     <React.Fragment>
//                     {/* {console.log("items")} */}
//                         {item.items.map(nested => (
//                             <a
//                             className="px-3 active-filter-item"
//                             href="#"
//                             onClick={event => {
//                                 event.preventDefault();
//                                 refine(nested.value);
//                             }}
//                             >
//                             {nested.label}
//                             </a>
//                         ))}
//                     </React.Fragment>
//                 ) : (
                    
//                     <a
//                     className="px-3 active-filter-item"
//                     href="#"
//                     onClick={event => {
//                         event.preventDefault();
//                         refine(item.value);
//                     }}
//                     >
//                     {/* {console.log("item")} */}
//                     {item.attribute === "year" ? "YEAR" : "RATING"} {item.currentRefinement.min} - {item.currentRefinement.max}
//                     </a>
//                 )}
//                 </div>
//             ))}

//             <CustomClearRefinements /> 
//             </section>) : null
//     )
// }
// const ClearRefinements = ({ items, refine}) => {
//     return(
//         <div className="reset-active-filters d-inline-block float-right mr-3"
//         onClick={() => refine(items)} disabled={!items.length}>
//             RESET
//         </div>
//     )
//   };
  
//   // 2. Connect the component using the connector
// const CustomClearRefinements = connectCurrentRefinements(ClearRefinements);

// export const CustomCurrentRefinements = connectCurrentRefinements(CurrentRefinement);



// const Range = ({min, max, currentRefinement, tooltip}) => {
//     return (
//         min !== max ? (
//             <div className="range-slider">
//               <Nouislider
//                 connect
//                 start={[currentRefinement.min, currentRefinement.max]}
//                 step={0.1}
//                 pips={{ mode: "count", values: 2 }}
//                 clickablePips
//                 range={{
//                 min: min,
//                 max: max
//                 }}
//                 tooltips = {tooltip}      
//                 onSlide={this.onChange}                
//               />
//             </div>
//           ) : null
//     )
// }

// export const CustomRangeSlider = connectRange(Range);


// let counter = [];
// const count = (items) => {
//     if(!counter.length){
//         counter = items;        
//     }
// }
// const isRefined = (label, e) => {
//     //ais-RefinementList-item--selected
//     counter.find((o, i) => {
//         if (o.label === label) {
//             counter[i].isRefined = !(counter[i].isRefined);
//             return true; // stop searching
//         }
//     });
// }
// const RefinementList = ({ items, currentRefinement, refine, movieStatic, attribute }) => (
    
//     <div>
//         { count(items) }
//       {/* <div>Current refinement: {currentRefinement.join(', ')}</div> */}
//      {/* {console.log(counter)} */}
//       <ul className="ais-RefinementList-list">
//         {counter.length ? (counter.map(item => (
//           <li key={item.label} className="ais-RefinementList-item">
//             <label class="ais-RefinementList-label">
//                  <input class="ais-RefinementList-checkbox" type="checkbox" value={item.label} onClick={e => {
//                      e.preventDefault();
//                      refine(item.value);
//                      isRefined(item.label, e);
//                  }}/>
//                  <span class="ais-RefinementList-labelText">{item.label}</span> 
//              </label>
//           </li>
//         ))) : null}
//       </ul>
//     </div>
//   );

  
// export const CustomRefinementList = connectRefinementList(RefinementList);




// const Hits = ({ hits }) => (
//   <ol>
//     {hits.map(hit => (
//       <li key={hit.objectID}>{hit.name}</li>
//     ))}
//   </ol>
// );
// class Hits extends React.Component{
//     getData = () => {
//         let str = uuidArry.join(), 
//         url = `/movies?include=wallpapers,userRatings,photos&azSearch=${str}`;
//         this.props.apicall(url);
//     }
//     render() {
//         let { hits } = this.props;
//         if(hits.length){
//             for(var i=0;i<hits.length;i++){
//                 uuidArry.push(hits[i].uuid);
//             }
//             this.getData();
//         }
//         // return hits.map( hit => {            
//         //     return(
//         //         <div key={hit.objectID}>{hit.name}</div>
//         //     )
//         // })
//     }
//}


// const RefinementList = ({ items, currentRefinement, refine }) => {
//     return (        
//         <ul class="ais-RefinementList-list">
//             <li class="ais-RefinementList-item">
//                 <label class="ais-RefinementList-label">
//                     <input class="ais-RefinementList-checkbox" type="checkbox" value="adventure" onClick={e => { 
//                         e.preventDefault(); 
//                         refine(e.target.value);
//                     }}/>

//                     <span class="ais-RefinementList-labelText">adventure</span> 
//                     <span class="ais-RefinementList-count">3</span>
//                 </label>
//             </li>
//             <li class="ais-RefinementList-item">
//                 <label class="ais-RefinementList-label">
//                     <input class="ais-RefinementList-checkbox" type="checkbox" value="animation" onClick={ e => {
//                         e.preventDefault();
//                         refine(e.target.value);
//                     }}/>

//                     <span class="ais-RefinementList-labelText">animation</span> 
//                     <span class="ais-RefinementList-count">3</span>
//                 </label>
//             </li>
//             <li class="ais-RefinementList-item">
//                 <label class="ais-RefinementList-label">
//                     <input class="ais-RefinementList-checkbox" type="checkbox" value="biopic" onClick={ e => {
//                         e.preventDefault();
//                         refine(e.target.value);
//                     }}/>

//                     <span class="ais-RefinementList-labelText">biopic</span> 
//                     <span class="ais-RefinementList-count">2</span>
//                 </label>
//             </li>
//             <li class="ais-RefinementList-item">
//                 <label class="ais-RefinementList-label">
//                     <input class="ais-RefinementList-checkbox" type="checkbox" value="drama" onClick={ e => {
//                         e.preventDefault();
//                         refine(e.target.value)
//                     }}/>

//                     <span class="ais-RefinementList-labelText">drama</span>
//                     <span class="ais-RefinementList-count">2</span>
//             </label>
//             </li>
//             <li class="ais-RefinementList-item">
//                 <label class="ais-RefinementList-label">
//                     <input class="ais-RefinementList-checkbox" type="checkbox" value="fantasy" onClick={ e => {
//                         e.preventDefault();
//                         refine(e.target.value)
//                     }}/>

//                     <span class="ais-RefinementList-labelText">fantasy</span> 
//                     <span class="ais-RefinementList-count">2</span>
//             </label>
//             </li>
//             <li class="ais-RefinementList-item">
//                 <label class="ais-RefinementList-label">
//                     <input class="ais-RefinementList-checkbox" type="checkbox" value="historical" onClick={ e => {
//                         e.preventDefault();
//                         refine(e.target.value)
//                     }}/>

//                     <span class="ais-RefinementList-labelText">historical</span> 
//                     <span class="ais-RefinementList-count">2</span>
//             </label>
//             </li>
//             <li class="ais-RefinementList-item">
//                 <label class="ais-RefinementList-label">
//                     <input class="ais-RefinementList-checkbox" type="checkbox" value="horror" onClick={ e => {
//                         e.preventDefault();
//                         refine(e.target.value)
//                     }}/>

//                     <span class="ais-RefinementList-labelText">horror</span> 
//                     <span class="ais-RefinementList-count">2</span>
//             </label>
//             </li>
//             <li class="ais-RefinementList-item">
//                 <label class="ais-RefinementList-label">
//                     <input class="ais-RefinementList-checkbox" type="checkbox" value="kids" onClick={ e => {
//                         e.preventDefault();
//                         refine(e.target.value)
//                     }}/>

//                     <span class="ais-RefinementList-labelText">kids</span> 
//                     <span class="ais-RefinementList-count">2</span>
//             </label>
//             </li>
//             <li class="ais-RefinementList-item">
//                 <label class="ais-RefinementList-label">
//                     <input class="ais-RefinementList-checkbox" type="checkbox" value="action" onClick={ e => {
//                         e.preventDefault();
//                         refine(e.target.value)
//                     }}/>

//                     <span class="ais-RefinementList-labelText">action</span> 
//                     <span class="ais-RefinementList-count">2</span>
//             </label>
//             </li>
//         </ul>
//     )
// }