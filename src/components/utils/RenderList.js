import React from 'react';
import PopoverStickOnHover from './ImageContprop';
import { selectedImgLists } from '../../actions';
import ImageFilter from 'react-image-filter';
import _ from 'lodash';
import $ from 'jquery'; //**********MUST NOT USE JQUERY IN REACT PROJECT***********//
import { connect } from 'react-redux';
import {Play, Eye} from '../../svgIcons';
import movie1 from '../../img/movie12.jpg';
import MovieIcon from '../../svgIcons/video-cam.svg';
import profile from '../../img/movie-icon.png';
import actor1 from '../../img/actor1.jpg';
import user from '../../svgIcons/star.svg';
import music3 from '../../img/music3.jpg';
import music from '../../svgIcons/musical-note.svg';
import video1 from '../../img/video1.jpg';
import VideoIcon from '../../svgIcons/play-button.svg';
import YoutubeVideo from '../utils/YoutubeVideoPlay'; 

import 'video-react/styles/scss/video-react.scss';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import ReactPlayer from 'react-player';
import { ControlBar } from 'video-react';


import Modal from 'react-modal';
import Plyr from 'react-plyr';

import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


import { Close } from '../../svgIcons';

// import '../content/plyr.css';

let timeoutFunc, classname, ulclassname, 

month = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];

const Video = ( props ) => {
    return (
        <YoutubeVideo />
    )
   };

class RenderList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isToggleOn: false,
            width: 0,
            height: 0,
            displayVideo: false,
            videoId : ""
        };
    
        // This binding is necessary to make `this` work in the callback
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
      }

    // state = {
     
    // }
    componentDidMount() {
        this.props.selectedImgLists(this.props.imgLists);
    }
    onMouseEnter = (item) => {
        let id = item.currentTarget.id,
        parent = item.currentTarget.parentNode.id;
        
        if(this.props.fixedNavLink === 'video' ){
            timeoutFunc = setTimeout(function(){
                if(document.getElementById(id) !== null){
                    document.getElementById(id).classList.add("video-item-hovered");
                    $('#'+parent).addClass('parentZ');
                    $('#'+parent+' #'+id).nextAll().addClass('next-sibling');
                    $('#'+parent+' #'+id).prevAll().addClass('prev-sibling');
                }
            }, 200);
        }
        else{
            this.setState({ 
                width: this.refs.listItem.getBoundingClientRect().width, 
                height: this.refs.listItem.getBoundingClientRect().height
            });
            timeoutFunc = setTimeout(function(){
                if(document.getElementById(id) !== null){
                document.getElementById(id).classList.add("item-hovered");
                document.getElementById(parent).classList.add("parentZ");
                $('#'+parent+' #'+id).nextAll().addClass('next-sibling');
                $('#'+parent+' #'+id).prevAll().addClass('prev-sibling');
                }
            }, 1000);
        }
    }
    onMouseLeave = () => {
        //console.log(this.state.width)
        clearTimeout(timeoutFunc);
        $('ul').removeClass('parentZ');
        if(this.props.fixedNavLink === 'video'){
            $('ul li').removeClass('video-item-hovered next-sibling prev-sibling');
        }
        else{
            $('ul li').removeClass('item-hovered next-sibling prev-sibling');
        }
    }
    name = (name) => {
        if(this.props.fixedNavLink === 'movies' || this.props.fixedNavLink === 'cc' ){
            return name;
        }
        if(this.props.fixedNavLink === 'music'){
            return name.split("|")[1];
        }
    }
    image = (wallpapers, bannerImages, cover) => {
        // if(this.props.fixedNavLink === 'movies' ){
        //     return (((wallpapers) && (wallpapers.data.length)) ? wallpapers.data[0].attachments.data[0].uri : movie1);
        // }
        if(this.props.fixedNavLink === 'movies' ){
            if (wallpapers.data.length) {
                if (wallpapers.data[0].attachments.data.length) {
                    // return (((wallpapers) && (wallpapers.data.length)) ? wallpapers.data[0].attachments.data[0].uri : movie1);
                    return( wallpapers.data[0].attachments.data[0].uri);
                }
                else {
                    return(MovieIcon);
                }
            }
            else {
                return(MovieIcon);
            }
        }
        else if(this.props.fixedNavLink === 'cc'){
            // return (((wallpapers) && (wallpapers.data.length)) ? wallpapers.data[0].attachments.data[0].uri : actor1);
            if((wallpapers) && (wallpapers.data.length)) {
                return(wallpapers.data[0].attachments.data[0].uri);
            }
            else {
                return(user);
            }
        }
        else if(this.props.fixedNavLink === 'music'){
            // return ((bannerImages) && (bannerImages.data.length) ? bannerImages.data[0].attachments.data[0].uri : music3);
           
            
           
            if(bannerImages.data.length) {
                if (bannerImages.data[0].attachments.data.length) {
                    // return (((bannerImages) && (bannerImages.data.length)) ? bannerImages.data[0].attachments.data[0].uri : music3);                    
                    return(bannerImages.data[0].attachments.data[0].uri);
                }
                else {
                    return(music);
                }
            }
            else {
                return(music);
            }
        }
        else if(this.props.fixedNavLink === 'video'){
            console.log()
            if(wallpapers){
                return `http://img.youtube.com/vi/${wallpapers.split("v=")[1]}/0.jpg`;
            }
            return VideoIcon;
        }
        
    }
    videoSectionSelector = (section) => {
        if((section === "MOVIE_VIDEO_EVENT_AUDIO_LAUNCH") || 
        (section === "MOVIE_VIDEO_EVENT_PRE_RELEASE") || 
        (section === "MOVIE_VIDEO_EVENT_PRESS_MEET") || 
        (section === "MOVIE_VIDEO_EVENT_OTHERS") || 
        (section === "ARTIST_VIDEO_EVENT_PERSONAL") || 
        (section === "ARTIST_VIDEO_EVENT_AUDIO_LAUNCH") || 
        (section === "ARTIST_VIDEO_EVENT_PERFORMANCE") || 
        (section === "ARTIST_VIDEO_EVENT_BRANDING") || 
        (section === "ARTIST_VIDEO_EVENT_AWARDS") || 
        (section === "ARTIST_VIDEO_EVENT_PAGE_3") || 
        (section === "ARTIST_VIDEO_EVENT_OTHERS") || 
        (section === "ARTIST_VIDEO_EVENT_PRESS_MEET")){
            return <p className="events">Events</p>;
        }
        else if((section === "MOVIE_VIDEO_SONG") || (section === "ARTIST_VIDEO_SONGS")){
            return <p className="songs">Songs</p>;
        }
        else if((section === "ARTIST_VIDEO_BEHIND_SCENES") || (section === "ARTIST_VIDEO_BEHIND_SCENES")){
            return <p className="behind-scenes">Behind scenes</p>;
        }
        else if((section === "ARTIST_VIDEO_SOCIAL")){
            return <p className="social">Social</p>;
        }
        else if((section === "MOVIE_VIDEO_TRAILER") || (section === "ARTIST_VIDEO_TRAILER")){
            return <p className="trailer">Trailer</p>;
        }
    }

    playVideo = (video) => {
     
      console.log(video);
    }

    videoRelease = (release) => {   
        let releaseArry = release.split("/");
        return <span className="video-release">{month[releaseArry[1]]} {releaseArry[0]} {releaseArry[2]}</span>;
    }

    handleOpenModal(imgdetail){
    
        var uri = imgdetail.attachments.data[0].uri;
        var splitUri = uri.split("=");
        this.setState({ isToggleOn: true, videoId: uri});
    }

    closeModal() {
        // alert()
      this.setState({isToggleOn: false});
    }


    render() {        
        const settings = {
            dots: true,
            infinite: true,
            speed: 600,
            slidesToShow: 7,
            slidesToScroll: 7,
            initialSlide: 0,
            lazyLoad: 'ondemand',
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  initialSlide: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
          };

        if(this.props.fixedNavLink === 'movies' || this.props.fixedNavLink === 'cc' ){
            classname = 'list-inline-item';
            ulclassname = 'list-inline';
        }
        if(this.props.fixedNavLink === 'music'){
            classname = "list-inline-item music-active";
            ulclassname = 'list-inline';
        }
        if(this.props.fixedNavLink === 'video'){
            classname = "list-inline-item video-active";
            ulclassname = 'list-inline video-ulclass';
        }
        const chunksList = _.chunk(this.props.imgLists, this.props.items);
        if(this.props.imgLists === undefined) {
            return <p className="text-center" style={{ color: "rgba(255, 255, 255, 0.4)"}}>None Found.</p>
        }
        else if(this.state.displayVideo){
            // <YoutubeVideo videoId={}/>
        }
        return chunksList.map((chunkList) => {
            //console.log(chunkList)
            const result = chunkList.map((imgdetail) => {
                return(
                    <li className={classname} ref="listItem" key={imgdetail.uuid} id={imgdetail.uuid} onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}  onClick ={() =>this.handleOpenModal(imgdetail)}>
                        {(this.props.fixedNavLink !== 'video') ? (
                            <PopoverStickOnHover
                                component={React.cloneElement(this.props.children, {imgdetail: imgdetail.uuid})}
                                placement="auto"
                                onMouseEnter={() => { }}
                                delay={1220}
                                maxWidth={this.state.width}
                                maxHeight={this.state.height}
                                navlink={this.props.fixedNavLink}>

                                <div className="only-image">
                                    <div className="inner-shadow"></div>
                                    <div className="movie-title">
                                        <p className="text-center">{this.name(imgdetail.name)}</p>
                                    </div>
                                    <ImageFilter
                                        image={this.image(imgdetail.wallpapers, imgdetail.bannerImages)}
                                        filter={ [0, 0.9, 0, 0, 0,
                                            0, 0.9, 0, 0, 0,
                                            0, 0.9, 0, 0, 0,
                                            0, 0, 0, 0.6, 0,] } 
                                    />
                                </div>
                            </PopoverStickOnHover>
                        ) : (
                            console.log('image details are', imgdetail),
                            <div className="only-image" onClick={() => { this.playVideo(imgdetail) }}>
                                <div className="inner-shadow"></div>
                                
                                <div className="movie-title" >
                                    <div className="video-play-button text-center mb-3">
                                        <Play/>
                                        <p>{imgdetail.duration}</p>
                                    </div>
                                   
                                    {this.videoSectionSelector(imgdetail.section)}

                                    {/* <p className={imgdetail.genre}>
                                        {this.videoSectionSelector(imgdetail.section)}
                                    </p> */}
                                    {/* <YoutubeVideo /> */}
                                    {(imgdetail.name.length >45) ? <p className="video-title">{imgdetail.name.substr(0, 45)}...</p> : <p className="video-title">{imgdetail.name}</p>}
                                    
                                    <p className="video-title__hover mb-0 mr-4">{imgdetail.name}</p>
                                    {/* <p className="video-release">{imgdetail.release}</p> */}
                                    <p className="float-left">{this.videoRelease(imgdetail.release)}</p>
                                    <p className="float-right video-views"><Eye/><span>200</span></p>
                                </div>
                                <ImageFilter
                                    image={this.image(imgdetail.attachments.data[0].uri)}
                                    filter={ [0, 0.9, 0, 0, 0,
                                        0, 0.9, 0, 0, 0,
                                        0, 0.9, 0, 0, 0,
                                        0, 0, 0, 0.6, 0,] } 
                                />

                                <Modal
                                    isOpen={this.state.isToggleOn}
                                    // onAfterOpen={this.afterOpenModal}
                                    onRequestClose={this.closeModal}
                                    // style={customStyles}
                                    contentLabel="Video Modal"
                                    ariaHideApp={false}
                                    closeTimeoutMS={2000}
                                    >
                                    <button onClick ={ () =>this.closeModal()} className="modal-close btn close">
                                        <Close width="25" fill="#fff"/>
                                    </button>
                                    {/* <Plyr
                                      type="youtube" // or "vimeo"
                                      videoId={this.state.videoId}
                                      iconUrl="https://image.flaticon.com/icons/svg/149/149125.svg"
                                      data-plyr-provider="youtube"
                                      data-plyr-embed-id="DaXrjJg7eBs"
                                        // hideControls="true"
                                        // controls="['play-large', 'rewind', 'play', 'fast-forward', 'progress', 'current-time', 'volume', 'pip', 'fullscreen']"
                                    /> */}
                                 
                                    <ReactPlayer 
                                        url={this.state.videoId}                                     
                                        controls = {true}
                                        width='100%'
                                        height='95%'
                                    />
                                    <Slider {...settings} className="video-carousel">
                                        <div>
                                            <img src={video1}/>                                            
                                            <a href="#" className="video-play">
                                                <Play width="10%" fill="#fff"/>
                                            </a>
                                        </div>
                                        <div>
                                            <img src={video1} />                                            
                                            <a href="#" className="video-play">
                                                <Play width="10%" fill="#fff"/>
                                            </a>
                                        </div>
                                        <div>
                                            <img src={video1} />                                            
                                            <a href="#" className="video-play">
                                                <Play width="10%" fill="#fff"/>
                                            </a>
                                        </div>
                                        <div>
                                            <img src={video1} />                                            
                                            <a href="#" className="video-play">
                                                <Play width="10%" fill="#fff"/>
                                            </a>
                                        </div>
                                        <div>
                                            <img src={video1} />                                            
                                            <a href="#" className="video-play">
                                                <Play width="10%" fill="#fff"/>
                                            </a>
                                        </div>
                                        <div>
                                            <img src={video1} />                                            
                                            <a href="#" className="video-play">
                                                <Play width="10%" fill="#fff"/>
                                            </a>
                                        </div>
                                        <div>
                                            <img src={video1} />                                            
                                            <a href="#" className="video-play">
                                                <Play width="10%" fill="#fff"/>
                                            </a>
                                        </div>
                                        <div>
                                            <img src={video1} />                                            
                                            <a href="#" className="video-play">
                                                <Play width="10%" fill="#fff"/>
                                            </a>
                                        </div>
                                        <div>
                                            <img src={video1} />                                            
                                            <a href="#" className="video-play">
                                                <Play width="10%" fill="#fff"/>
                                            </a>
                                        </div>
                                    </Slider>
                                    {/* <Carousel>
                                        <div>
                                            <img src={video1} />
                                            <p className="legend">Legend 1</p>
                                        </div>
                                        <div>
                                            <img src={movie1} />
                                            <p className="legend">Legend 2</p>
                                        </div>
                                        <div>
                                            <img src={music3} />
                                            <p className="legend">Legend 3</p>
                                        </div>
                                        <div>
                                            <img src={actor1} />
                                            <p className="legend">Legend 4</p>
                                        </div>
                                        <div>
                                            <img src={movie1} />
                                            <p className="legend">Legend 5</p>
                                        </div>
                                        <div>
                                            <img src={music3} />
                                            <p className="legend">Legend 6</p>
                                        </div>
                                    </Carousel> */}
                                </Modal>
                            </div>
                        )}
                    </li>
                )
                
            });
            return (
                    <ul className={ulclassname} key={chunksList.indexOf(chunkList)} id={chunksList.indexOf(chunkList)}>
                        {result.length ? result : "None found"}
                    </ul>
                            
                    );
        });
    }
} 

const mapStateToProps = state => {
    return {
        items: state.items
    }
}
Modal.setAppElement('body');
export default connect(mapStateToProps,{selectedImgLists})(RenderList);