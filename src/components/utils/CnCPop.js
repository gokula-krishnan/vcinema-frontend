import React from 'react';
import { Movies, Play, Gallery } from '../../svgIcons';
import base from '../../api/base';

let month = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
class CnCPop extends React.Component {
    state={
        imgdetail: null,
        uuid: null
    }
    componentDidMount(){
        //console.log('redner')
        if(this.state.uuid !== this.props.imgdetail){
            this.response(this.props.imgdetail);
        }
            
    }
    response =  async (uuid) => {
        //console.log(uuid)
        const res = await base.get(`/artists?include=movies,photos,videos&azSearch=${uuid}`);///artists?include=wallpapers
        this.setState({
            imgdetail: res.data.data[0],
            uuid: uuid
        })
        //console.log(res.data.data[0]);
    }

    createMarkup = (imgdetail) => {
       if(imgdetail.short_description){
           return {__html: `${imgdetail.short_description.substr(0, 200)}<a href="#Home">...Read More</a> </p>`};
       }
       else{
           return {__html: `<p>Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur ipsum&nbsp; dolor sit amet<a href="#Home">...Read More</a> </p>`}
       }
   }


    render(){
        let classname = null;
        let{imgdetail} = this.state;
        let releaseStr, releaseArry;
        if(imgdetail){
            //console.log('render')
            
            releaseStr = imgdetail.dob;
            releaseArry = releaseStr.split("/");
            if(imgdetail.artist_rank > 0){
                classname = 'item ranking';
            } else {
                classname = 'item d-none';        
            }
        }
    return(
        this.state.imgdetail ? (
        <>
        <div className="details" data-id={imgdetail.uuid}>
            <div className="detail-cont">               

                <div className={classname}>
                    <div className="radial-inner-bg">
                        <div className="text">#{imgdetail.ranking}</div>                    
                    </div>
                </div>

                <h6 className="mb-1">{imgdetail.name}</h6>

                <p className="detail-cont-occptn">
                    {imgdetail.occupation.split(",").map( (item, index) => {
                        return <span key={index}>{item}</span>;
                    })}
                </p>

                <p className="mb-1">Born : &nbsp;<span>{releaseArry[0]} {month[releaseArry[1]-1]} {releaseArry[2]} Hyderabad, Andhrapradesh</span></p>

                <p>Spouse : &nbsp;<span>{imgdetail.spouse}</span></p>

                <div dangerouslySetInnerHTML={this.createMarkup(imgdetail)}>
                    
                </div>
                {/* <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, 
                    consectetur adipisicing elit, sed do eiusmod tempor. 
                    Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet...
                    <a href="#Home">Read More</a>
                </p> */}
                
                <div className="popoverFooter">
                    <hr />
                    <p>
                        <span className="mr-2"><Movies /></span>MOVIES({imgdetail.movies.data.length})
                        <span className="ml-4 mr-1"><Play /> </span>VIDEOS({imgdetail.videos.data.length})
                        <span className="ml-4 mr-1"><Gallery /></span>IMAGES({imgdetail.photos.data.length})
                    </p>
                </div>
            </div> 
        </div>
        <div className="overLay"></div>
        </>
        ) : null
    )
    }
    
}

export default CnCPop;