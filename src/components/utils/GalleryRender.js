import React from 'react';
import { connect } from 'react-redux';
import { selectedImgLists, galleryDetails } from '../../actions';
import _ from 'lodash';
import ImageFilter from 'react-image-filter';
// import LazyLoad from 'react-lazy-load';

// import Gallery from "react-photo-gallery";
// import { photos } from "./photos";
import Masonry from 'react-masonry-component';
import Modal from 'react-modal';
import "react-image-gallery/styles/css/image-gallery.css";
import PropTypes from 'prop-types';

import ImageGallery from 'react-image-gallery';

import { Close } from '../../svgIcons';


let timeoutFunc, classname, ulclassname;

const masonryOptions = {
    transitionDuration: 0,
    gutter: 1,
    columnWidth: 300,
    fitWidth: true,
    horizontalOrder: true
};

const SIZES = [200, 100];

const randomSize = () => SIZES[Math.floor(Math.random() * SIZES.length)];

class GalleryRenderList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isToggleOn: false,
            images : []
        };
    
        // This binding is necessary to make `this` work in the callback
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
      }


      renderFullscreenButton = () => {
        alert("asf");
    }

    componentDidMount() {
        console.log(this.props.imgLists)
        this.props.selectedImgLists(this.props.imgLists);
    }

    getGalleryDetails = (element) => {

        if(element.type == "MOVIE"){
            var type = 'movie';
        }
        else{
            var type = 'artist';
        }
      
        this.props.galleryDetails(type, element.id, element.section)
        .then(res =>
                // this.setState({
                //  images: res.data.data,
                // })
                {
                    let imgLists = res.data.data;
                    let arrLists = [];
            
                    for(var i=0; i<imgLists.length;i++){
                        arrLists.push({ original : imgLists[i].attachments.data[0].uri, height :4, 
                            description   :imgLists[i].imgTypeDetails.data[0].name,
                            thumbnail: imgLists[i].attachments.data[0].uri})
                    }

                    this.setState({
                        images : arrLists
                    })
                }
            );
        
        
    }
    handleOpenModal(element){

        this.setState({ isToggleOn: true });
        this.getGalleryDetails(element);
        
    }

    closeModal() {
      this.setState({isToggleOn: false});
    }

    
    photoSectionSelector = (section) => { 

        if((section === "MOVIE_PHOTO_EVENT_AUDIO_LAUNCH") ||
        (section === "MOVIE_PHOTO_EVENT_PRE_RELEASE") || 
        (section === "MOVIE_PHOTO_EVENT_PRESS_MEET") || 
        (section === "MOVIE_PHOTO_EVENT_OTHERS") || 
        (section === "ARTIST_PHOTO_EVENT_PERSONAL") || 
        (section === "ARTIST_PHOTO_EVENT_AUDIO_LAUNCH") || 
        (section === "ARTIST_PHOTO_EVENT_PERFORMANCE") || 
        (section === "ARTIST_PHOTO_EVENT_BRANDING") || 
        (section === "ARTIST_PHOTO_EVENT_AWARDS") || 
        (section === "ARTIST_PHOTO_EVENT_PAGE_3") || 
        (section === "ARTIST_PHOTO_EVENT_OTHERS") || 
        (section === "ARTIST_PHOTO_EVENT_PRESS_MEET")
        ){
          return  <p className="events-title mb-0">Events</p> 
        }
        else if( (section === "ARTIST_PHOTO")){
            return  <p className="artist-title mb-0">Stills</p> 
        }
        else if( (section === "MOVIE_PHOTO")){
            return  <p className="posters-title mb-0">Posters</p> 
        }
        else if( (section === "ARTIST_PHOTO_BEHIND_SCENES") ||
        (section === "MOVIE_PHOTO_BEHIND_SCENES")
        ){
            return  <p className="behind-scenes-title mb-0">Behind Scenes</p> 
        }
        else if( (section === "MOVIE_WALLPAPER") ||
        (section === "ARTIST_WALLPAPERS")
        ){
            return  <p className="wallpaper-title mb-0">Wallpapers</p> 
        }
        else if( (section === "ARTIST_PHOTO_SOCIAL") ||
        (section === "MOVIE_PHOTO_SOCIAL")
        ){
            return  <p className="social-title mb-0">Social</p> 
        }
        else{
            return <p className="other-title mb-0">Others</p> 
        }
    }
    FullscreenButton(onClick, isFullscreen) {

        console.log(onClick)
        return (
          <button
            type='button'
            className={
              `image-gallery-fullscreen-button${isFullscreen ? ' active' : ''}`}
            onClick={() => this.renderFullscreenButton()}
          />
        );
      }

    render() {
          
        const imagesLoadedOptions = { background: '.my-bg-image-el' }
        const chunksList = _.chunk(this.props.imgLists, this.props.items);
        const childElements = this.props.imgLists.map((element) => {
           
            return (                
                 <li className="image-element-class gallery-image-container" key={element.uuid}  onClick ={ () =>this.handleOpenModal(element)} >
                     {/* <div class="only-image"><div class="inner-shadow"></div><div class="movie-title"><p class="text-center">Maharshi Mahesh Babu</p></div><div class="ImageFilter " style="position: relative;"></div></div> */}
                     {/* <div class="inner-shadow"></div> */}
                     <div className="inner-shadow"></div>
                     <img src={element.src} />
                     <div className="only-image">
                        <div className="inner-shadow"></div>
                        <div className="movie-title" >                            

                            {/* <YoutubeVideo /> */}
                            {this.photoSectionSelector(element.section)}
                      
                            <p className="video-title__hover mb-0 mr-4">{element.name}</p>
                        </div>                        
                     </div>
                 </li>
             );
         });
          
        
         return (
             <>
            <Masonry
                className={'gallery-layout'} // default ''
                elementType={'ul'} // default 'div'
                options={masonryOptions} // default {}
                disableImagesLoaded={false} // default false
                updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                // imagesLoadedOptions={imagesLoadedOptions} // default {}
            >
            
                {childElements}
            </Masonry>

            <Modal
                isOpen={this.state.isToggleOn}
                // onAfterOpen={this.afterOpenModal}
                onRequestClose={this.closeModal}
                // style={customStyles}
                ariaHideApp={false}
                contentLabel="Gallery Modal"
                closeTimeoutMS={2000}
                >
        
            <button onClick={this.closeModal} className="modal-close btn close">
                <Close width="25" fill="#fff"/>
            </button>
                    <ImageGallery
                     items={this.state.images}  
                     showIndex={true}
                     showPlayButton={false}
                     slideOnThumbnailOver={true} 
                     FullscreenButton={this.FullscreenButton}
                     disableThumbnailScroll={false}
                     sizes="(width: 400px) 400px, 100vw"
                    />
               
            </Modal>
            </>
                //  <ul className={ulclassname} key={chunksList.indexOf(chunkList)} id={chunksList.indexOf(chunkList)}>
                //     {result.length ? result : "None found"}
                // </ul>
                        
           
        );
    // });
    }

}


const mapStateToProps = state => {
    return {
        items: state.items,
        // header: state.selectedHeader,
    }
}

// GalleryRenderList.prototype = {
//     header: PropTypes.string.isRequired,
// }

export default connect(mapStateToProps,{selectedImgLists, galleryDetails})(GalleryRenderList);