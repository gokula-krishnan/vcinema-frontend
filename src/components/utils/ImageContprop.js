
import React from 'react';
import PropTypes from 'prop-types';
import { Overlay, Popover } from 'react-bootstrap';
import $ from 'jquery';

let styleAttr;
export default class PopoverStickOnHover extends React.Component {
  
  state = {
    showPopover: false,
  }
  handleMouseEnter = () => {
    const { delay, onMouseEnter } = this.props;
    //console.log(item.currentTarget.offsetParent.id)
    this.setTimeoutConst = setTimeout(() => {
      this.setState({ showPopover: true }, () => {
        if (onMouseEnter) {
          onMouseEnter()
        }
      })
    }, delay);
  }

  handleMouseLeave = () => {
    clearTimeout(this.setTimeoutConst);
    this.setState({ showPopover: false });
  }
  onMouseEnter = (item) => {
    //console.log(item.target.children);
    $('#'+item.currentTarget.lastChild.firstElementChild.attributes["data-id"].nodeValue).addClass('still-hovered');
    this.setState({ showPopover: true })
  }
  onMouseLeave = () => {
    $('ul li').removeClass('still-hovered');
    clearTimeout(this.setTimeoutConst);
    this.setState({ showPopover: false });
  }
  componentWillUnmount() {  
    if (this.setTimeoutConst) {
      clearTimeout(this.setTimeoutConst)
    }
  }

  render() {
    //console.log(this.props);    
    if(this.props.navlink === "movies" || this.props.navlink === "cc"){
      styleAttr = {
        minWidth: (this.props.maxWidth*2.07), 
        maxWidth: (this.props.maxWidth*2.07), 
        height: (this.props.maxHeight*1.578)
      }
    }
    if(this.props.navlink === "music"){
      styleAttr = {
        maxWidth: (this.props.maxWidth*1.6), 
        height: (this.props.maxHeight*1.338)//1.3, 1.138
      }
    }
    let { component, children, placement } = this.props;
    //console.log(component)
    const child = React.Children.map(children, (child) => (
      React.cloneElement(child, {
        onMouseEnter: this.handleMouseEnter,
        onMouseLeave: this.handleMouseLeave,
        ref: (node) => {
          this._child = node
          const { ref } = child
          if (typeof ref === 'function') {
            ref(node);
          }
        }
      })
    ))[0]

    return(
      <React.Fragment>
        {child}
        <Overlay
          show={this.state.showPopover}
          placement={placement}
          target={this._child}
          shouldUpdatePosition={true}
        >
          <Popover
            onMouseEnter={this.onMouseEnter}
            onMouseLeave={this.onMouseLeave}
            id='popover'
            style={styleAttr}
          >
            {component}
          </Popover>
        </Overlay>
      </React.Fragment>
    )
  }
}

PopoverStickOnHover.defaultProps = {
  delay: 0
}

PopoverStickOnHover.propTypes = {
  delay: PropTypes.number,
  onMouseEnter: PropTypes.func,
  children: PropTypes.element.isRequired,
  component: PropTypes.node.isRequired
}