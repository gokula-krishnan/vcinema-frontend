import React from 'react';
import { selectedImgLists } from '../../actions';
import ImageFilter from 'react-image-filter';
import _ from 'lodash';
import $ from 'jquery'; //**********MUST NOT USE JQUERY IN REACT PROJECT***********//
import { connect } from 'react-redux';
import nandi from '../../img/nandi.jpg';

let timeoutFunc;

class AwardsRender extends React.Component {
    state = {
        width: 0,
        height: 0
    }
    componentDidMount() {
        this.props.selectedImgLists(this.props.imgLists);
    }
    

    onMouseEnter = (item) => {
        let id = item.currentTarget.id,
        parent = item.currentTarget.parentNode.id;
        
            this.setState({ 
                width: this.refs.listItem.getBoundingClientRect().width, 
                height: this.refs.listItem.getBoundingClientRect().height
            });
            timeoutFunc = setTimeout(function(){
                if(document.getElementById(id) !== null){
                document.getElementById(id).classList.add("item-hovered");
                document.getElementById(parent).classList.add("parentZ");
                $('#'+parent+' #'+id).nextAll().addClass('next-sibling');
                $('#'+parent+' #'+id).prevAll().addClass('prev-sibling');
                }
            }, 1000);
    }
    onMouseLeave = () => {
        clearTimeout(timeoutFunc);
        $('ul').removeClass('parentZ');
        $('ul li').removeClass('item-hovered next-sibling prev-sibling');
    }
    name = () => {
        return `list-inline awards-ulclass ${this.props.class}`;
    }
    render() {
        let { img } = this.props;
        const chunksList = _.chunk(this.props.imgLists, this.props.items);
        if(this.props.imgLists === undefined) {
            return <p className="text-center" style={{ color: "rgba(255, 255, 255, 0.4)"}}>None Found.</p>
        }
        return chunksList.map((chunkList) => {
            //console.log(chunkList)
            const result = chunkList.map((imgdetail) => {
                let year = imgdetail.date.split('/');
                return(
                    <li className="list-inline-item awards-active" ref="listItem" key={imgdetail.uuid} id={imgdetail.uuid} onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave} >
                        <div className="only-image">
                            <div className="movie-title">
                                {imgdetail.series ? <p>{imgdetail.series}<sup>th</sup></p> : null}
                                <h5 className="text-uppercase mb-0">{imgdetail.name}</h5>
                                <h2>{year[2]}</h2>
                            </div>
                            <ImageFilter
                                image={img ? img : nandi}
                                filter={ 
                                    [0, 0.9, 0, 0, 0,
                                    0, 0.9, 0, 0, 0,
                                    0, 0.9, 0, 0, 0,
                                    0, 0, 0, 0.2, 0,] 
                                }
                            />
                        </div>
                    </li>
                )
                
            });
            return (<ul className={this.name()} key={chunksList.indexOf(chunkList)} id={chunksList.indexOf(chunkList)}>
                        {result.length ? result : "None found"}
                    </ul>);
        });
    }
} 

const mapStateToProps = state => {
    return {
        items: state.items
    }
}
export default connect(mapStateToProps,{selectedImgLists})(AwardsRender);