import React from 'react';
import RenderList from './RenderList';
import MoviePop from './MoviePop';
import { connect } from 'react-redux';

class FilterServices extends React.Component{
    state={
        filteredArry: null,
        names : null
    }
    componentDidMount(){
        //console.log("mount")
        //console.log(this.props.fetchstream)
       // this.filterStrm();
    }
    filterStrm = () => {
        let {uuidArry, fetchstream} = this.props;
        setTimeout(() => {
            //console.log(this.props.uuidArry)
            let filter = fetchstream
                         .filter( item => {
                             let abc = item.professions.data.some((prof) => {
                               let efg = prof.artist.some(cast => {
                                    return (uuidArry.includes(cast.name));
                                })    
                                return efg;
                             })
                             return abc;
                         })
           this.setState({ filteredArry: filter, names: uuidArry})
        }, 500)
    }
    anotherOne = () => {
        console.log(this.props.uuidArry);
        
    }
    componentDidUpdate(){
        if(JSON.stringify(this.state.names) !== JSON.stringify(this.props.uuidArry)){
            this.anotherOne();
        }
        //this.filterStrm();
    }
    render() {
        return(
            <RenderList imgLists={this.state.filteredArry ? this.state.filteredArry : this.props.fetchstream} fixedNavLink="movies">
                <MoviePop />
            </RenderList>
        )
    }
}

const mapStateToProps = state => {
    return{
        fetchstream: state.fetchstream,
    }
}
export default connect(mapStateToProps)(FilterServices);