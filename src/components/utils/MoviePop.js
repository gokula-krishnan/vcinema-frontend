import React from 'react';
import { Music, Play, Gallery } from '../../svgIcons';
import base from '../../api/base';

let d = new Date(), 
releaseText = null,
classname = null,
month = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

class MoviePop extends React.Component {
    state={
        imgdetail: null,
        uuid: null
    }
    componentDidMount(){
        if(this.state.uuid !== this.props.imgdetail){
            this.response(this.props.imgdetail);
        }
            
    }
    
    response =  async (uuid) => {
        await base.get(`/movies?include=photos,songs,videos,story,professions&azSearch=${uuid}`)
        .then(res => this.setState({
            imgdetail: res.data.data[0],
            uuid: uuid
        }))
        .catch(err => console.warn(err))
        
        
    }
    createMarkup = (imgdetail) => {
         {/* {imgdetail.story.data.length ? imgdetail.story.data[0].value.substr(0, 200) : "Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur ipsum&nbsp; dolor sit amet "} */}
        if(imgdetail.story.data.length){
            return {__html: `${imgdetail.story.data[0].value.substr(0, 300)}<a href="#Home">...Read More</a>`};
        }
        else{
            return {__html: `<p>Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur ipsum&nbsp; dolor sit amet </p><a href="#Home">...Read More</a>`}
        }
    }
    render(){
        let{imgdetail} = this.state;
        let releaseStr, releaseArry, releaseDate;
        console.log(this.state.imgdetail);
        if(imgdetail){
            //console.log('render')
            
            releaseStr = imgdetail.release;
            releaseArry = releaseStr.split("/");
            releaseDate = new Date(releaseArry[2], releaseArry[1] -1, releaseArry[0]);
           //console.log(releaseArry)
            if(releaseDate > d) {
                releaseText = 'IN THEATERS'; 
                classname = 'item d-none';
            } else {
                releaseText = 'RELEASE DATE';
                classname = `item progress-${(((imgdetail.rating ? imgdetail.rating :  imgdetail.criticRating)*100)/5)}`;
            }
        }
        return( 
            this.state.imgdetail ? (
            <>
                <div className="details" data-id={imgdetail.uuid}>
                    <div className="detail-cont">
                        <p>{releaseText} - {releaseArry[0]} {month[releaseArry[1]-1]} {releaseArry[2]}</p> 

                        <div className={classname}>
                            <div className="radial-inner-bg">
                                <div className="text">{imgdetail.rating ? imgdetail.rating :  imgdetail.criticRating}</div>
                            </div>
                        </div>

                        <h6 className=" mb-1">{imgdetail.title ? imgdetail.title : imgdetail.name }</h6>

                        <p className="detail-cont-genre">
                        {imgdetail.genre.map( (item, index) => {
                            //console.log(item.substr(0, 1))
                                if(index < 5){
                                    return <span key={index}>{item.substr(0, 1)}{item.substr(1).toLowerCase()}</span>;
                                }
                                if(index === 5){
                                    return <span key={index}>...</span>;
                                }
                        })}
                        {/* <span>Science Fiction</span><span>Action</span> */}
                        </p>

                        <p className="detail-cont-directors mb-1">
                            DIRECTORS : &nbsp;
                            {imgdetail.professions.data.map( (item, index) => {
                                if(item.profession === "PRODUCER"){
                                    return <span key={index}>{item.artist[0].name}</span>;
                                }
                            })}
                        </p>

                        <p className="detail-cont-actors">
                            ACTORS : &nbsp;
                            {imgdetail.professions.data.map( (item, index) => {
                                if((item.profession === "HERO") || (item.profession === "HEROINE") || (item.profession === "VILLAIN") || (item.profession === "COMEDIAN")){
                                    return <span key={index}>{item.artist[0].name}</span>;
                                }
                            })}
                        </p>

                        
                        <div dangerouslySetInnerHTML={this.createMarkup(imgdetail)} className="detail-cont-detail">
                            {/* {console.log(imgdetail.story.data[0].value.substr(0, 200))} */}
                            {/* {imgdetail.story.data.length ? imgdetail.story.data[0].value.substr(0, 200) : "Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur ipsum&nbsp; dolor sit amet "} */}
                            {/* <a href="#Home">...Read More</a> */}
                            
                        </div>

                        <div className="popoverFooter">
                            <hr />
                            <p>
                                <span className="mr-2"><Music /></span>TRAILERS({imgdetail.videos ? imgdetail.videos.data.length: "0"})
                                <span className="ml-4 pl-2 mr-1"><Play /> </span>AUDIO({imgdetail.songs ? imgdetail.songs.data.length: "0"})
                                <span className="ml-4 pl-2 mr-1"><Gallery /></span>IMAGES({imgdetail.photos ? imgdetail.photos.data.length : "0"})
                            </p>
                        </div>
                    </div> 
                </div>
                <div className="overLay"></div>
            </>
        ) : (
            <div className="details">
                <div className="detail-cont">
                </div>
            </div>
        )
            
        )
    }
}
// const MoviePop = ({imgdetail}) => {
//     let d = new Date(), 
//     imgRelease = new Date(imgdetail.release), 
//     releaseText = null,
//     classname = null;

//     if(imgRelease > d){
//         releaseText = 'IN THEATERS'; 
//         classname = 'item d-none';
//     } else {
//         releaseText = 'RELEASE DATE';
//         classname = `item progress-${(((imgdetail.rating ? imgdetail.rating :  imgdetail.criticRating)*100)/5)}`;
//     }
//     return(
//         <>
//         <div className="details" data-id={imgdetail.id ? imgdetail.id : imgdetail.uuid}>
//             <div className="detail-cont">
//                 <p>{releaseText} - {imgdetail.release}</p> 

//                 <div className={classname}>
//                     <div className="radial-inner-bg">
//                         <div className="text">{imgdetail.rating ? imgdetail.rating :  imgdetail.criticRating}</div>
//                     </div>
//                 </div>

//                 <h6>{imgdetail.title ? imgdetail.title : imgdetail.name }</h6>

//                 <p className="detail-cont-genre">
//                 {/* {imgdetail.genre ? ( imgdetail.genre.map( item => {
//                         return <span>{item}</span>;
//                 }) ) : (<><span>Science Fiction</span>/<span>Action</span></>)} */}
//                 <span>Science Fiction</span><span>Action</span>
//                 </p>

//                 <p>DIRECTORS : &nbsp;<span>Boyapati Srinu</span></p>

//                 <p>ACTORS : &nbsp;<span>Prabhas, Shradha Kapoor</span></p>

//                 <p>Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, 
//                     consectetur adipisicing elit, sed do eiusmod tempor. 
//                     Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet...
//                     <a href="#Home">Read More</a>
//                 </p>

//                 <div className="popoverFooter">
//                     <hr />
//                     <p>
//                         <span className="mr-2"><Music /></span>TRAILERS(4) 
//                         <span className="ml-4 mr-1"><Play /> </span>AUDIO(8)
//                         <span className="ml-4 mr-1"><Gallery /></span>IMAGES(26)
//                     </p>
//                 </div>
//             </div> 
//         </div>
//         <div className="overLay"></div>
//         </>
//     )
// }

export default MoviePop;