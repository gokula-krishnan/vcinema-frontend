import React from 'react';
import { Music, Play, Gallery } from '../../svgIcons';
import base from '../../api/base';

let d = new Date(), 
releaseText = null,
classname = null,
month = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

const renderHTML = (rawHTML) => React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });



  
class MusicPop extends React.Component {
    state={
        imgdetail: null,
        uuid: null
    }
    componentDidMount(){
        if(this.state.uuid !== this.props.imgdetail){
           // console.log('redne1r')
            this.response(this.props.imgdetail);
        }
            
    }
    
    response =  async (uuid) => {
        const res = await base.get(`/albums?include=profession,movie&azSearch=${uuid}`);
        this.setState({
            imgdetail: res.data.data[0],
            uuid: uuid
        })
    
    }
    createMarkup = (imgdetail) => {
        {/* {imgdetail.story.data.length ? imgdetail.story.data[0].value.substr(0, 200) : "Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur ipsum&nbsp; dolor sit amet "} */}
       if(imgdetail.movie.description.length){
           return {__html: `${imgdetail.movie.description.substr(0, 300)}<a href="#Home">...Read More</a>`};
       }
       else{
           return {__html: `<p>Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur ipsum&nbsp; dolor sit amet </p><a href="#Home">...Read More</a>`}
       }
   }

    render() {
        let {imgdetail} = this.state;
        let releaseStr, releaseArry, releaseDate;
        console.log(this.state.imgdetail)
        if(imgdetail){
            //console.log(imgdetail.release)            
            releaseStr = imgdetail.release;
            releaseArry = releaseStr.split("/");
            releaseDate = new Date(releaseArry[2], releaseArry[1] -1, releaseArry[0]);
           //console.log(releaseArry)
            if(releaseDate > d) {
                releaseText = 'RELEASING ON'; 
                classname = 'item d-none';
            } else {
                releaseText = 'RELEASED';
                classname = `item progress-${(((imgdetail.rating ? imgdetail.rating :  imgdetail.criticRating)*100)/5)}`;
            }
        }
        return(
            this.state.imgdetail ? (
            <>
            <div className="details" data-id={imgdetail.uuid}>
                <div className="detail-cont">
    
                    <p className="pb-2 m-0">{releaseText} {month[releaseArry[1]-1]} {releaseArry[0]}, {releaseArry[2]}</p>
    
                    <h5 className="p-0 m-0">{imgdetail.name.split("|")[1]}</h5>
    
                    <ul>
                        <li>{imgdetail.label}</li>
                        <li>{imgdetail.total_songs} Songs</li>
                        <li>{imgdetail.total_duration}</li>
                    </ul>
                    <p className="mb-0">MUSIC DIRECTORS : &nbsp;
                        {imgdetail.profession.data.map( (item, index) => {
                            if(item.profession === "MUSIC_DIRECTOR"){
                                return <span key={index}>{item.artist[0].name}</span>;
                            }
                        })}
                    </p>
    
                    <p className="mb-0">DIRECTORS : &nbsp;
                        {imgdetail.profession.data.map( (item, index) => {
                            if(item.profession === "DIRECTOR"){
                                return <span key={index}>{item.artist[0].name}</span>;
                            }
                        })}
                    </p>
    
                    <p>ACTORS : &nbsp;
                        {imgdetail.profession.data.map( (item, index) => {
                            if((item.profession === "HERO") || (item.profession === "HEROINE") || (item.profession === "VILLAIN") || (item.profession === "COMEDIAN")){
                                return <span key={index}>{item.artist[0].name} </span>;
                            }
                        })}
                    </p>
    
                    <div dangerouslySetInnerHTML={this.createMarkup(imgdetail)} className="detail-cont-detail">
                    </div>
                        
                    {/* <p>{imgdetail.movie.description}
                        <a href="#Home">Read More</a>
                    </p> */}
    
                    <div className="popoverFooter">
                        <hr />
                        <p className="mb-3">
                            <span className="mr-2"><Play /></span>PLAY
                            <span className="ml-4 pl-4 mr-1"><Music /> </span>SHARE
                            <span className="ml-4 pl-4 mr-2"><Play /></span>ADD PLAYLIST
                        </p>
                    </div>
                </div> 
            </div>
            <div className="overLay"></div>
            </>) : null
        )
    }  
    
}

export default MusicPop;