import React from 'react';
import PopoverStickOnHover from './ImageContprop';
import { selectedImgLists } from '../../actions';
import ImageFilter from 'react-image-filter';
import _ from 'lodash';
import $ from 'jquery';
import { connect } from 'react-redux';
import actor1 from '../../img/actor1.jpg';
import actor2 from '../../img/actor2.jpg';
import actor3 from '../../img/actor3.jpg';
import actor4 from '../../img/actor4.jpg';
import actor5 from '../../img/actor5.jpg';
import actor6 from '../../img/actor6.jpg';
import actor7 from '../../img/actor7.jpg';
import music1 from '../../img/music1.jpg';
import music2 from '../../img/music2.jpg';
import music3 from '../../img/music3.jpg';
import music4 from '../../img/music4.jpg';
//import { TimelineLite, CSSPlugin } from "react-gsap";

let timeoutFunc, classname;  

class RenderList extends React.Component {
    state = {
        width: 0,
        height: 0
    }
    componentDidMount() {
        this.props.selectedImgLists(this.props.imgLists);
       // console.log(this.props.imgLists);
    }
    onMouseEnter = (item) => {
        let id = item.currentTarget.id,
        parent = item.currentTarget.parentNode.id;
        //console.log(this.refs.listItem.getBoundingClientRect())
        //console.log(parent);
        if(this.refs.listItem){
            this.setState({ 
                width: this.refs.listItem.getBoundingClientRect().width, 
                height: this.refs.listItem.getBoundingClientRect().height
            });
        }
        timeoutFunc = setTimeout(function(){
            if(document.getElementById(id) !== null){
                document.getElementById(id).classList.add("item-hovered");
                $('#'+parent).addClass('parentZ');
                $('#'+parent+' #'+id).nextAll().addClass('next-sibling');
                $('#'+parent+' #'+id).prevAll().addClass('prev-sibling');
            }            
            //$('#'+id).addClass('item-hovered');
        }, 1000);
       // console.log(this.refs.listItem.getBoundingClientRect())
    }

    onMouseLeave = () => {
        //console.log(this.state.width)
        clearTimeout(timeoutFunc);
        $('ul li').removeClass('item-hovered next-sibling prev-sibling');
        $('ul').removeClass('parentZ');
    }

    render() {
        //console.log(this.state.height);
        if(this.props.fixedNavLink === 'movies' || this.props.fixedNavLink === 'cc' ){
            classname = 'list-inline-item';
        }
        if(this.props.fixedNavLink === 'music'){
            classname = "list-inline-item music-active";
        }
        const chunksList = _.chunk(this.props.imgLists, this.props.items);
        if(this.props.imgLists === undefined) {
            return <p className="text-center" style={{ color: "rgba(255, 255, 255, 0.4)"}}>None Found.</p>
        }
        return true;
        return chunksList.map((chunkList) => {
            const result = chunkList.map((imgdetail) => {
                //console.log(imgdetail.wallpapers.data[0].attachments.data[0].uri)
                //return true;
                return (
                    <li className={classname} ref="listItem" key={imgdetail.uuid} id={imgdetail.uuid} onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave} >
                        <PopoverStickOnHover
                            component={React.cloneElement(this.props.children, { imgdetail })}
                            placement="auto"
                            onMouseEnter={() => { }}
                            delay={1250}
                            maxWidth={this.state.width}
                            maxHeight={this.state.height}
                            navlink={this.props.fixedNavLink}
                        >
                            <div className="only-image">
                                <div className="inner-shadow"></div>
                                <div className="movie-title">
                                    <p className="text-center">{imgdetail.name}</p>
                                </div>
                                <ImageFilter
                                    image={imgdetail.wallpapers.data[0].attachments.data[0].uri}
                                    filter={ [0, 0.9, 0, 0, 0,
                                        0, 0.9, 0, 0, 0,
                                        0, 0.9, 0, 0, 0,
                                        0, 0, 0, 0.6, 0,] } 
                                />
                            </div>
                        </PopoverStickOnHover>
                    </li>
                );
            });
            return <ul className="list-inline" key={chunksList.indexOf(chunkList)} id={chunksList.indexOf(chunkList)}>{result}</ul>
        });
    }
} 

const mapStateToProps = state => {
    return {
        items: state.items
    }
}
export default connect(mapStateToProps,{selectedImgLists})(RenderList);