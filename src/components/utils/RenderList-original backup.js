import React from 'react';
import PopoverStickOnHover from './ImageContprop';
import { selectedImgLists } from '../../actions';
import ImageFilter from 'react-image-filter';
import _ from 'lodash';
import $ from 'jquery'; //**********MUST NOT USE JQUERY IN REACT PROJECT***********//
import { connect } from 'react-redux';
import {Play} from '../../svgIcons';
import movie1 from '../../img/movie12.jpg';
import actor1 from '../../img/actor1.jpg';
import music3 from '../../img/music3.jpg';
import video1 from '../../img/video1.jpg';
import YoutubeVideo from '../utils/YoutubeVideoPlay'; 


let timeoutFunc, classname, ulclassname, 

month = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];

const Video = ( props ) => {
    return (
        <YoutubeVideo />
    )
   };

class RenderList extends React.Component {
    state = {
        width: 0,
        height: 0,
        displayVideo: false,
        videoId : ""
    }
    componentDidMount() {
        this.props.selectedImgLists(this.props.imgLists);
    }
    onMouseEnter = (item) => {
        let id = item.currentTarget.id,
        parent = item.currentTarget.parentNode.id;
        
        if(this.props.fixedNavLink === 'video' ){
            timeoutFunc = setTimeout(function(){
                if(document.getElementById(id) !== null){
                    document.getElementById(id).classList.add("video-item-hovered");
                    $('#'+parent).addClass('parentZ');
                    $('#'+parent+' #'+id).nextAll().addClass('next-sibling');
                    $('#'+parent+' #'+id).prevAll().addClass('prev-sibling');
                }
            }, 1000);
        }
        else{
            this.setState({ 
                width: this.refs.listItem.getBoundingClientRect().width, 
                height: this.refs.listItem.getBoundingClientRect().height
            });
            timeoutFunc = setTimeout(function(){
                if(document.getElementById(id) !== null){
                document.getElementById(id).classList.add("item-hovered");
                document.getElementById(parent).classList.add("parentZ");
                $('#'+parent+' #'+id).nextAll().addClass('next-sibling');
                $('#'+parent+' #'+id).prevAll().addClass('prev-sibling');
                }
            }, 1000);
        }
    }
    onMouseLeave = () => {
        //console.log(this.state.width)
        clearTimeout(timeoutFunc);
        $('ul').removeClass('parentZ');
        if(this.props.fixedNavLink === 'video'){
            $('ul li').removeClass('video-item-hovered next-sibling prev-sibling');
        }
        else{
            $('ul li').removeClass('item-hovered next-sibling prev-sibling');
        }
    }
    name = (name) => {
        if(this.props.fixedNavLink === 'movies' || this.props.fixedNavLink === 'cc' ){
            return name;
        }
        if(this.props.fixedNavLink === 'music'){
            return name.split("|")[1];
        }
    }
    image = (wallpapers, bannerImages) => {
        if(this.props.fixedNavLink === 'movies' ){
            return (((wallpapers) && (wallpapers.data.length)) ? wallpapers.data[0].attachments.data[0].uri : movie1);
        }
        else if(this.props.fixedNavLink === 'cc'){
            return (((wallpapers) && (wallpapers.data.length)) ? wallpapers.data[0].attachments.data[0].uri : actor1);
        }
        else if(this.props.fixedNavLink === 'music'){
            return ((bannerImages) && (bannerImages.data.length) ? bannerImages.data[0].attachments.data[0].uri : music3);
        }
        else if(this.props.fixedNavLink === 'video'){
            console.log()
            if(wallpapers){
                return `http://img.youtube.com/vi/${wallpapers.split("v=")[1]}/0.jpg`;
            }
            return video1;
        }
        
    }
    videoSectionSelector = (section) => {
        if((section === "MOVIE_VIDEO_EVENT_AUDIO_LAUNCH") || 
        (section === "MOVIE_VIDEO_EVENT_PRE_RELEASE") || 
        (section === "MOVIE_VIDEO_EVENT_PRESS_MEET") || 
        (section === "MOVIE_VIDEO_EVENT_OTHERS") || 
        (section === "ARTIST_VIDEO_EVENT_PERSONAL") || 
        (section === "ARTIST_VIDEO_EVENT_AUDIO_LAUNCH") || 
        (section === "ARTIST_VIDEO_EVENT_PERFORMANCE") || 
        (section === "ARTIST_VIDEO_EVENT_BRANDING") || 
        (section === "ARTIST_VIDEO_EVENT_AWARDS") || 
        (section === "ARTIST_VIDEO_EVENT_PAGE_3") || 
        (section === "ARTIST_VIDEO_EVENT_OTHERS") || 
        (section === "ARTIST_VIDEO_EVENT_PRESS_MEET")){
            return <p className="events">Events</p>;
        }
        else if((section === "MOVIE_VIDEO_SONG") || (section === "ARTIST_VIDEO_SONGS")){
            return <p className="songs">Songs</p>;
        }
        else if((section === "ARTIST_VIDEO_BEHIND_SCENES") || (section === "ARTIST_VIDEO_BEHIND_SCENES")){
            return <p className="behind-scenes">Behind scenes</p>;
        }
        else if((section === "ARTIST_VIDEO_SOCIAL")){
            return <p className="social">Social</p>;
        }
        else if((section === "MOVIE_VIDEO_TRAILER") || (section === "ARTIST_VIDEO_TRAILER")){
            return <p className="trailer">Trailer</p>;
        }
    }

    playVideo = (video) => {
     
      console.log(video);
    }

    videoRelease = (release) => {   
        let releaseArry = release.split("/");
        return <p className="video-release">{month[releaseArry[1]]} {releaseArry[0]} {releaseArry[2]}</p>;
    }
    render() {
        if(this.props.fixedNavLink === 'movies' || this.props.fixedNavLink === 'cc' ){
            classname = 'list-inline-item';
            ulclassname = 'list-inline';
        }
        if(this.props.fixedNavLink === 'music'){
            classname = "list-inline-item music-active";
            ulclassname = 'list-inline';
        }
        if(this.props.fixedNavLink === 'video'){
            classname = "list-inline-item video-active";
            ulclassname = 'list-inline video-ulclass';
        }
        const chunksList = _.chunk(this.props.imgLists, this.props.items);
        if(this.props.imgLists === undefined) {
            return <p className="text-center" style={{ color: "rgba(255, 255, 255, 0.4)"}}>None Found.</p>
        }
        else if(this.state.displayVideo){
            // <YoutubeVideo videoId={}/>
        }
        return chunksList.map((chunkList) => {
            //console.log(chunkList)
            const result = chunkList.map((imgdetail) => {
                return(
                    <li className={classname} ref="listItem" key={imgdetail.uuid} id={imgdetail.uuid} onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave} >
                        {(this.props.fixedNavLink !== 'video') ? (
                            <PopoverStickOnHover
                                component={React.cloneElement(this.props.children, {imgdetail: imgdetail.uuid})}
                                placement="auto"
                                onMouseEnter={() => { }}
                                delay={1220}
                                maxWidth={this.state.width}
                                maxHeight={this.state.height}
                                navlink={this.props.fixedNavLink}>

                                <div className="only-image">
                                    <div className="inner-shadow"></div>
                                    <div className="movie-title">
                                        <p className="text-center">{this.name(imgdetail.name)}</p>
                                    </div>
                                    <ImageFilter
                                        image={this.image(imgdetail.wallpapers, imgdetail.bannerImages)}
                                        filter={ [0, 0.9, 0, 0, 0,
                                            0, 0.9, 0, 0, 0,
                                            0, 0.9, 0, 0, 0,
                                            0, 0, 0, 0.6, 0,] } 
                                    />
                                </div>
                            </PopoverStickOnHover>
                        ) : (
                            console.log('image details are', imgdetail),
                            <div className="only-image" onClick={() => { this.playVideo(imgdetail) }}>
                                <div className="inner-shadow"></div>
                                
                                <div className="movie-title" >
                                    <div className="video-play-button text-center mb-3">
                                        <Play/>
                                        <p>{imgdetail.duration}</p>
                                    </div>
                                   
                                    {this.videoSectionSelector(imgdetail.section)}

                                    {/* <p className={imgdetail.genre}>
                                        {this.videoSectionSelector(imgdetail.section)}
                                    </p> */}
                                    {/* <YoutubeVideo /> */}
                                    {(imgdetail.name.length >45) ? <p className="video-title">{imgdetail.name.substr(0, 45)}...</p> : <p className="video-title">{imgdetail.name}</p>}
                                    
                                    <p className="video-title__hover mb-0 mr-4">{imgdetail.name}</p>
                                    {/* <p className="video-release">{imgdetail.release}</p> */}
                                    {this.videoRelease(imgdetail.release)}
                                </div>
                                <ImageFilter
                                    image={this.image(imgdetail.attachments.data[0].uri)}
                                    filter={ [0, 0.9, 0, 0, 0,
                                        0, 0.9, 0, 0, 0,
                                        0, 0.9, 0, 0, 0,
                                        0, 0, 0, 0.6, 0,] } 
                                />
                            </div>
                        )}
                    </li>
                )
                
            });
            return (
                    <ul className={ulclassname} key={chunksList.indexOf(chunkList)} id={chunksList.indexOf(chunkList)}>
                        {result.length ? result : "None found"}
                    </ul>
                            
                    );
        });
    }
} 

const mapStateToProps = state => {
    return {
        items: state.items
    }
}
export default connect(mapStateToProps,{selectedImgLists})(RenderList);