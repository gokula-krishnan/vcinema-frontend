import React from 'react';
import YouTube from 'react-youtube';
import { connect } from 'react-redux';
import { selectedImgLists } from '../../actions';

class YouTubeVideo extends React.Component {
  render() {
    const opts = {
      height: '390',
      width: '640',
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    };

    return (
      <YouTube
        videoId="2g811Eo7K8U"
        opts={opts}
        onReady={this._onReady}
      />
    );
  }

  componentDidMount() {
    this.props.selectedImgLists(this.props.videoId);

    console.log(this.props.selectedImgLists(this.props.videoId))
    }

  _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }
}

const mapStateToProps = state => {
    return {
        items: state.items
    }
}
export default connect(mapStateToProps,{selectedImgLists})(YouTubeVideo);