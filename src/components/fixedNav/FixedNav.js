import React from 'react';
import { Nav, NavDropdown, Navbar } from 'react-bootstrap';
import { connect } from 'react-redux';
import { selectFixedNavLink, selectHeader, selectFilter } from '../../actions';
import { NavLink, Redirect } from 'react-router-dom';
import './FixedNav.scss';
import logo from './logo.png';
//import { ReactComponent as Star} from '../../Vcinema svg icons/star.svg';
import { Feeds, Movies, Star, Music, Play, Trophy, Talent, Gallery, Vworld } from '../../svgIcons';

class FixedNav extends React.Component {
    
    renderNavLink = (val, SvgComponent) => {
       // console.log(val)
        const to = `/${val}`;
        return(
            <NavLink to ={to} onClick={ () => {
               
                this.props.selectFixedNavLink(to);
                this.props.selectHeader('#popular');
                this.props.selectFilter(false);
                } } >
                <SvgComponent />
                <br />
                {(val === "cc") ? "cast&crew" : val}
            </NavLink>
        );
    }      
    render() {
        return(
            // <Nav className="flex-column d-inline-flex">
            //     <NavLink activeClassName="" to="">
            //         <img src={logo} alt="logo" />
            //     </NavLink>
            //     {/* <NavDropdown title="Dropdown" id="nav-dropdown">
            //         <NavDropdown.Item eventKey="4.1">English</NavDropdown.Item>
            //         <NavDropdown.Item eventKey="4.2">Telugu</NavDropdown.Item>
            //     </NavDropdown> */}
            //     {this.renderNavLink( 'feeds', Feeds)}
            //     {this.renderNavLink( 'movies', Movies)}
            //     {this.renderNavLink( 'cc', Star)}
            //     {this.renderNavLink( 'music', Music)}
            //     {this.renderNavLink( 'video', Play)}
            //     {this.renderNavLink( 'gallery', Gallery)}
            //     {this.renderNavLink( 'awards', Trophy)}
            //     {/* {this.renderNavLink( 'talent', Talent)}
            //     {this.renderNavLink( 'v-world', Vworld)} */}
               
            //     {/* <NavLink to="/movies">CAST&CREW</NavLink>
            //     <NavLink to="/movies">MUSIC</NavLink>
            //     <NavLink to="/movies">VIDEOS</NavLink>
            //     <NavLink to="/movies">GALLERY</NavLink>
            //     <NavLink to="/movies">AWARDS</NavLink>
            //     <NavLink to="/movies">TALENT</NavLink>
            //     <NavLink to="/movies">V-WORLD</NavLink> */}
            //     {/* <Nav.Link href="/">Active</Nav.Link>
            //     <Nav.Link eventKey="link-1">Link</Nav.Link>
            //     <Nav.Link eventKey="link-2">Link</Nav.Link>
            //     <Nav.Link eventKey="disabled" disabled>
            //         Disabled
            //     </Nav.Link> */}
            // </Nav>



            <Navbar collapseOnSelect expand="lg" bg="transparent">
                <Navbar.Brand href="#home" className="d-md-none"><img src={logo} alt="logo" /></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" >
                    <div>
                        <span></span>
                    </div>
                </Navbar.Toggle>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="flex-column d-inline-flex nav">
                        <NavLink activeClassName="" to="">
                            <img src={logo} alt="logo" />
                        </NavLink>
                        {/* <NavDropdown title="Dropdown" id="nav-dropdown">
                            <NavDropdown.Item eventKey="4.1">English</NavDropdown.Item>
                            <NavDropdown.Item eventKey="4.2">Telugu</NavDropdown.Item>
                        </NavDropdown> */}
                        {this.renderNavLink( 'feeds', Feeds)}
                        {this.renderNavLink( 'movies', Movies)}
                        {this.renderNavLink( 'cc', Star)}
                        {this.renderNavLink( 'music', Music)}
                        {this.renderNavLink( 'video', Play)}
                        {this.renderNavLink( 'gallery', Gallery)}
                        {this.renderNavLink( 'awards', Trophy)}
                        {/* {this.renderNavLink( 'talent', Talent)}
                        {this.renderNavLink( 'v-world', Vworld)} */}
                    
                        {/* <NavLink to="/movies">CAST&CREW</NavLink>
                        <NavLink to="/movies">MUSIC</NavLink>
                        <NavLink to="/movies">VIDEOS</NavLink>
                        <NavLink to="/movies">GALLERY</NavLink>
                        <NavLink to="/movies">AWARDS</NavLink>
                        <NavLink to="/movies">TALENT</NavLink>
                        <NavLink to="/movies">V-WORLD</NavLink> */}
                        {/* <Nav.Link href="/">Active</Nav.Link>
                        <Nav.Link eventKey="link-1">Link</Nav.Link>
                        <Nav.Link eventKey="link-2">Link</Nav.Link>
                        <Nav.Link eventKey="disabled" disabled>
                            Disabled
                        </Nav.Link> */}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
  }    
}

export default connect(null, { selectFixedNavLink, selectHeader, selectFilter })(FixedNav);