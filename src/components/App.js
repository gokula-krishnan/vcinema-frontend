import React from 'react';
import FixedNav from './fixedNav';
import Movie from './content/Movie';
import CastAndCrew from './content/CastAndCrew';
import Music from './content/Music';
import Videos from './content/Videos';
import Gallery from './content/Gallery';
import Awards from './content/Awards';
import Feeds from './content/Feeds';
import MainContent from './middleContent/MainContent';
import RightSideBar from './rightSideBar/RightSideBar';
import { Route, BrowserRouter } from 'react-router-dom';
import { Gallerysvg } from '../svgIcons';
// import HeaderLeftMenu from './header/headerTopLeftMenu/HeaderLeftMenu';
import '../App.scss';

const mainContainerStyle = {
    backgroundColor: '#000',
    position: 'absolute',
    width: '100%',
    minHeight: '100%'
};

class App extends React.Component {

    render() {
        return (
            <div className="main-container" style={mainContainerStyle}>
                <BrowserRouter basename="/vcinema">
                    <FixedNav />
                    <>
                    <Route path="/feeds" exact component={Feeds} />
                        <Route path="/movies" exact component={Movie} />
                        <Route path="/cc" exact component={CastAndCrew} />
                        <Route path="/music" exact component={Music} />
                        <Route path="/video" exact component={Videos} />
                        <Route path="/gallery" exact component={Gallery} />
                        <Route path="/awards" exact component={Awards} />
                    </>
                </BrowserRouter>
                             
            </div>
        )
    }
}


export default App;