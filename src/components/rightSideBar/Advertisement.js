import React from 'react';
import './rightSideBar.scss';
import Advert from '../../img/rightSideBarImages/mpu1.jpg';

class Advertisement extends React.Component {

    render() {
        return (
            <div className="advt-container m-t-40">                
                <img src={Advert}></img>
            </div>
        );
    }
}

export default Advertisement;