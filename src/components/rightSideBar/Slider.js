import React from 'react';
import './rightSideBar.scss';
import { MovieIcon } from '../../svgIcons';
import Slider02 from './Slider2';
import "react-alice-carousel/lib/scss/alice-carousel.scss";

class RightPanel extends React.Component {

    render() {
        return (
            <div className="row">
                <div className="col-12 slider-container">
                    <div className="collection-detail">
                        <div className="d-flex align-items-center">
                            <span className="movie-icon"><MovieIcon></MovieIcon></span>
                            <h3>Upcoming Releases</h3>
                        </div>
                        <a href="#" className="view-all">View All</a>
                    </div>
                </div>
                <div className="col-12 slider">
                    <div className="">
                        <Slider02></Slider02>
                    </div>
                </div>
            </div>

        );
    }
}

export default RightPanel;