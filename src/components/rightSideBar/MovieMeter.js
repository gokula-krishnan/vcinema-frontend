import React from 'react';
import './rightSideBar.scss';
import MovieMeter01 from './../../img/rightSideBarImages/movie-meter01.jpg';
import MovieMeter02 from './../../img/rightSideBarImages/movie-meter02.jpg';
import MovieMeter03 from './../../img/rightSideBarImages/movie-meter03.jpg';
import MovieMeter04 from './../../img/rightSideBarImages/movie-meter04.jpg';
import MovieMeter05 from './../../img/rightSideBarImages/movie-meter05.jpg';
import uparrow from '../../img/rightSideBarImages/up-arrow.png';
import downarrow from '../../img/rightSideBarImages/down-arrow.png';

class MovieMeter extends React.Component {

    render() {
        return (
            <div className="movie-meter-wrapper">
                <div className="col-12">
                    <div className="collection-detail">                       
                        <h3 className="meter-header">Movie meter<small> (based on page views)</small></h3>
                    </div>
                    {/* <table>
                        <tbody>
                            <tr>
                                <td>
                                    <div className="">
                                        <img src={MovieMeter01}></img>
                                    </div>
                                </td>
                                <td>
                                    <div className="rank"><span>#1</span></div>
                                </td>
                                <td>
                                    <div className="movie-title"><span className="col-12">Saaho</span>
                                        <br></br><small className="col-12">( <i className="fa fa-fw">&#xf062</i>> 2)</small>
                                    </div>
                                </td>
                                <td>
                                    <div className="percentage"><span>13.0%</span></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div className="">
                                        <img src={MovieMeter01}></img>
                                    </div>
                                </td>
                                <td>
                                    <div className="rank"><span>#1</span></div>
                                </td>
                                <td>
                                    <div className="movie-title"><span className="col-12">Saaho</span>
                                        <br></br><small className="col-12">( 2)</small>
                                    </div>
                                </td>
                                <td>
                                    <div className="percentage"><span>13.0%</span></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div className="">
                                        <img src={MovieMeter01}></img>
                                    </div>
                                </td>
                                <td>
                                    <div className="rank"><span>#1</span></div>
                                </td>
                                <td>
                                    <div className="movie-title"><span className="col-12">Saaho</span>
                                        <br></br><small className="col-12">( 2)</small>
                                    </div>
                                </td>
                                <td>
                                    <div className="percentage"><span>13.0%</span></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div className="">
                                        <img src={MovieMeter01}></img>
                                    </div>
                                </td>
                                <td>
                                    <div className="rank"><span>#1</span></div>
                                </td>
                                <td>
                                    <div className="movie-title"><span className="col-12">Saaho</span>
                                        <br></br><small className="col-12">( 2)</small>
                                    </div>
                                </td>
                                <td>
                                    <div className="percentage"><span>13.0%</span></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div className="">
                                        <img src={MovieMeter01}></img>
                                    </div>
                                </td>
                                <td>
                                    <div className="rank"><span>#1</span></div>
                                </td>
                                <td>
                                    <div className="movie-title"><span className="col-12">Saaho</span>
                                        <br></br><small className="col-12">( 2)</small>
                                    </div>
                                </td>
                                <td>
                                    <div className="percentage"><span>13.0%</span></div>
                                </td>
                            </tr>
                    
                        </tbody>
                    </table> */}
                    <div className="table-responsive-sm meter-table">
                        <table className="table">
                            <tbody>
                                <tr>
                                    <td>
                                        <div className="representation-image">
                                            <img src={MovieMeter01}></img>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="rank"><span>#1</span></div>
                                    </td>
                                    <td>
                                        <div className="movie-title">
                                            <p>Saaho</p>
                                            <small className="d-flex align-items-center">
                                                <span>( </span>                                                
                                                <img src={uparrow}/> 2
                                                <span> )</span>
                                            </small>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="percentage"><span>40.2%</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="representation-image">
                                            <img src={MovieMeter02}></img>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="rank"><span>#2</span></div>
                                    </td>
                                    <td>
                                        <div className="movie-title">
                                            <p>Maharshi</p>
                                            <small className="d-flex align-items-center">
                                                <span>( </span>                                                
                                                <img src={uparrow}/> 34
                                                <span>)</span>
                                            </small>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="percentage"><span>22.7%</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="">
                                            <img src={MovieMeter03}></img>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="rank"><span>#3</span></div>
                                    </td>
                                    <td>
                                        <div className="movie-title">
                                            <p>iSmart Sankar</p>
                                            <small className="d-flex align-items-center">
                                                <span>( </span>                                                
                                                <img src={downarrow}/> 2
                                                <span>)</span>
                                            </small>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="percentage"><span>13.0%</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="">
                                            <img src={MovieMeter04}></img>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="rank"><span>#4</span></div>
                                    </td>
                                    <td>
                                        <div className="movie-title">
                                            <p>Saaho</p>
                                            <small className="d-flex align-items-center">
                                                <span>( </span>                                                
                                                <img src={uparrow}/> 245
                                                <span>)</span>
                                            </small>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="percentage"><span>8.4%</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="">
                                            <img src={MovieMeter05}></img>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="rank"><span>#5</span></div>
                                    </td>
                                    <td>
                                        <div className="movie-title">
                                            <p>Devi 2</p>
                                            <small className="d-flex align-items-center">
                                                <span>( </span>                                                
                                                <img src={uparrow}/> 14
                                                <span>)</span>
                                            </small>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="percentage"><span>3.1%</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" className="text-center"><a href="" className="view-all col-12 text-center">View All</a></td>
                                </tr>
                            </tbody>
                        </table>                        
                    </div>
                </div>
            </div>
        );
    }
}

export default MovieMeter;