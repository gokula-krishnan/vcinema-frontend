import React from 'react';
import './rightSideBar.scss';
import AdvertBg from '../../img/rightSideBarImages/mpu2.png';

class Advertisement02 extends React.Component {

    render() {
        return (
            <div className="advt-container m-t-40"> 
                <small className="float-right adv-text">Advertisement</small>                
                <img src={AdvertBg}></img>              
            </div>

        );
    }
}

export default Advertisement02;