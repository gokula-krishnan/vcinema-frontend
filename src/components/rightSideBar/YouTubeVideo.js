import React from 'react';
import './rightSideBar.scss';
import { PromoIcon, Play } from './../../svgIcons';
import youTubeLogo from './../../img/rightSideBarImages/youtube-logo.png';
import VideoBgImage from './../../img/rightSideBarImages/video.png';
import playIcon from '../../img/play-button.png';

var reviewStyle = {
    height:'170px',   
    backgroundImage: `url(${VideoBgImage})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundColor:'rgba(255, 255, 255, 0.1)'
};
class YouTubeVideo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            videoURL: 'http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4'
        }
    }
    render() {
        return (
            <div className="video-container">
                <header>
                    <div className="float-left youtube-logo"> <img src={youTubeLogo}/> </div>
                    <div className="float-right right-text"><PromoIcon width="39" height="14">PROMO</PromoIcon></div>
                </header>
                <div className="video-wrapper" style={reviewStyle}>
                    <video id="background-video" loop autoPlay>
                        <source src={this.state.videoURL} type="video/mp4" />
                        <source src={this.state.videoURL} type="video/ogg" />
                    </video>
                    <div className="shadow">
                        <button className="button   ">
                            <img src={playIcon}/>
                        </button>
                    </div>
                </div>
                <footer>
                    <a>BulReddy Video Song | Sita Telugu Movie</a>
                </footer>
            </div>
        );
    }
}

export default YouTubeVideo;