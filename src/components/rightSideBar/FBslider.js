import React from 'react';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import FBslider01 from '../../img/sliderImages/fb.jpg';
const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
};
class FB extends React.Component {

    render() {
        return (
            <div className="insta-feed no-padding fb-slider">
                <div className="col-12 no-padding slider">
                    <div className="">
                        <Carousel
                            swipeable={false}
                            draggable={false}
                            showDots={false}
                            responsive={responsive}
                            ssr={true} // means to render carousel on server-side.
                            infinite={true}
                            autoPlay={this.props.deviceType !== "mobile" ? false : false}
                            autoPlaySpeed={1000}
                            keyBoardControl={true}
                            customTransition="all .5"
                            transitionDuration={500}
                            containerClass="carousel-container"
                            removeArrowOnDeviceType={["tablet", "mobile"]}
                            deviceType={this.props.deviceType}
                            dotListClass="custom-dot-list-style"
                            itemClass="carousel-item-padding-40-px"
                        >
                            <div className="slider-body">
                                <div className="col-12 no-padding">
                                    <img src={FBslider01}></img>
                                    </div>   
                                    <a href="#" className="view-all text-center float-left col-12">View All</a>                            
                            </div>
                            <div className="slider-body">
                                <div className="col-12 no-padding">
                                    <img src={FBslider01}></img>
                                    </div>   
                                    <a href="#" className="view-all text-center float-left col-12">View All</a>                            
                            </div>
                        </Carousel>

                    </div>
                </div>
            </div>

        );
    }
}



export default FB;