import React from 'react';
import './rightSideBar.scss';
import PromoBg from '../../img/rightSideBarImages/promo-bg.png';
// var sectionStyle = {
//     height:'224px',   
//     backgroundImage: `url(${PromoBg})`,
//     backgroundPosition: 'center',
//     backgroundSize: 'cover',
//     backgroundRepeat: 'no-repeat'
// };
class Promo extends React.Component {

    render() {
        return (
            <div className="advt-container m-t-40 movie-promo feed-card">                
                 {/* <small className="promo-span">Promo</small> 
                 <footer>
                    <div className="float-left">
                    <h2>bellamkonda srinivas</h2>
                     <p>
                     <a href="">Actor </a> |  <a href="">Writer</a>  |  <a href=""> Producer</a></p>
                    </div>
                    <div className="float-right circle">
                    <span className="promo-rank">#21</span>
                    </div>
                     </footer> */}
                <img src={PromoBg} width="100%"/>
                <div className="shadow"></div>
                <small className="promo-span">Promo</small>
                <div className="d-flex align-items-center justify-content-between promo-details">
                     <div>
                        <h2>bellamkonda srinivas</h2>
                        <p>
                        <a href="">Actor </a> |  <a href="">Writer</a>  |  <a href=""> Producer</a></p>
                     </div>
                     <div>
                        <div className="circle">
                            <span className="promo-rank">#21</span>
                        </div>
                     </div>
                </div>
            </div>

        );
    }
}

export default Promo;