import React from 'react';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { MovieIcon } from '../../svgIcons';
import Movieclip01 from '../../img/sliderImages/polls-img.jpg';
import Yes from '../../img/sliderImages/yes.png';
import No from '../../img/sliderImages/no.png';
import poll from '../../img/rightSideBarImages/poll.png';
const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        slidesToSlide: 2, // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
};
class Slider1 extends React.Component {

    render() {
        return (
            <div className="polls-container">
                <div className="col-12 slider-container">
                    {/* <header>
                        <div className="float-left d-flex">
                            <span><MovieIcon></MovieIcon></span>
                            <h2>Polls</h2>
                        </div>
                        <a href="#" className="view-all">View All</a>
                    </header> */}
                    <div className="collection-detail">
                        <div className="d-flex align-items-center">
                            <span className="movie-icon"><img src={poll}/></span>
                            <h3>Polls</h3>
                        </div>
                        <a href="#" className="view-all">View All</a>
                    </div>
                </div>
                <div className="slider">
                    <Carousel
                        swipeable={true}
                        draggable={false}
                        showDots={false}
                        responsive={responsive}
                        ssr={true} // means to render carousel on server-side.
                        infinite={true}
                        autoPlay={this.props.deviceType !== "mobile" ? false : false}
                        autoPlaySpeed={1000}
                        keyBoardControl={true}
                        transitionDuration={500}
                        containerClass="carousel-container"
                        removeArrowOnDeviceType={["tablet", "mobile"]}
                        deviceType={this.props.deviceType}
                        dotListClass="custom-dot-list-style"
                        itemClass="carousel-item-padding-40-px"
                    >
                        <div className="slider-body row">
                            <div className="col-5 left-image ">
                                <img src={Movieclip01} className="x-auto"></img>
                                </div>
                                <div className="col-7 right-content">
                                    <p>Will Mahesh Babu's new movie 'Srimanthudu' gross Bahubali's box office collection?</p>
                                    <div className="action-btns">
                                    <span className="confirm-btn"><img src={Yes}></img><label>Yes</label></span>
                                    <span className="confirm-btn no-btn"><img src={No}></img><label>No</label></span>
                                    </div>
                                </div>
                            
                        </div>
                        <div className="slider-body">
                            <div className="col-5 float-left left-image">
                                <img src={Movieclip01} className="x-auto"></img>
                                </div>
                                <div className="col-7 float-left right-content">
                                    <p>Will Mahesh Babu's new movie 'S?</p>
                                    <div className="action-btns">
                                    <span className="confirm-btn"><img src={Yes}></img><label>Yes</label></span>
                                    <span className="confirm-btn no-btn"><img src={No}></img><label>No</label></span>
                                    </div>
                                </div>
                            
                        </div>

                    </Carousel>
                </div>
            </div>

        );
    }
}



export default Slider1;