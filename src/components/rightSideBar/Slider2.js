import React from 'react';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import movieclip01 from '../../img/sliderImages/1.jpg';
import movieclip02 from '../../img/sliderImages/2.jpg';


const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 2,
        slidesToSlide: 2, // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 2,
        slidesToSlide: 2, // optional, default to 1.
    },
};
class Slider02 extends React.Component {

    render() {
        return (

            <Carousel
                additionalTransfrom={0}
                arrows
                autoplay={this.props.deviceType !== "mobile" ? false : false}
                responsive={responsive}
                autoPlaySpeed={1000}
                transitionDuration={500}
                centerMode={false}
                containerClass="container-with-dots"
                removeArrowOnDeviceType={["tablet", "mobile"]}
                deviceType={this.props.deviceType}
                dotListClass=""
                draggable
                focusOnSelect={false}
                infinite
                itemClass=""
                keyBoardControl
                minimumTouchDrag={80}
                renderDotsOutside={false}
                responsive={responsive}
                swipeable={true}
            >
                <div>
                    {/* <img src={movieclip01}></img>
                    <div className="slider-footer">
                        <p>Maharshi</p>
                        <span>22 April 2019</span>
                    </div> */}
                    <div className="card bg-transparent slider-card">
                        <img className="card-img-top" src={movieclip01} alt="Card image cap"/>
                        <div className="card-body">
                            <h5 className="card-title">Maharshi</h5>
                            <span className="date small">22 April 2019</span>
                        </div>
                    </div>
                </div>
                <div>
                    {/* <img src={movieclip02}></img>
                    <div className="slider-footer">
                        <p>Saho</p>
                        <span>22 April 2019</span>
                    </div> */}
                    <div className="card bg-transparent slider-card">
                        <img className="card-img-top" src={movieclip02} alt="Card image cap"/>
                        <div className="card-body">
                            <h5 className="card-title">Saaho</h5>
                            <span className="date small">30 August 2019</span>
                        </div>
                    </div>
                </div>
                <div>
                    {/* <img src={movieclip02}></img>
                    <div className="slider-footer">
                        <p>Maharshi 2</p>
                        <span>22 April 2019</span>
                    </div> */}
                    <div className="card bg-transparent slider-card">
                        <img className="card-img-top" src={movieclip01} alt="Card image cap"/>
                        <div className="card-body">
                            <h5 className="card-title">maharshi 2</h5>
                            <span className="date small">22 April 2019</span>
                        </div>
                    </div>
                </div>
                <div>
                    {/* <img src={movieclip01}></img>
                    <div className="slider-footer">
                        <p>Saho 2</p>
                        <span>22 April 2019</span>
                    </div> */}
                    <div className="card bg-transparent slider-card">
                        <img className="card-img-top" src={movieclip02} alt="Card image cap"/>
                        <div className="card-body">
                            <h5 className="card-title">Saaho 2</h5>
                            <span className="date small">22 April 2019</span>
                        </div>
                    </div>
                </div>
            </Carousel>
        );
    }
}



export default Slider02;