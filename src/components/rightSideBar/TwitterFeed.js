import React from 'react';
import './rightSideBar.scss';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import TwitterBg from '../../img/sliderImages/twitter-base.png';
import TwitterFeed01 from '../../img/sliderImages/twitter-feed01.jpg';
import twittericon from '../../img/twitter.png';
const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
};

class TwitterFeed extends React.Component {

    render() {
        return (
            <div className="twitter-container mt-2 mt-md-4">
                <div className="twitter-posts">
                <div className="collection-detail">
                        <div className="d-flex align-items-center">
                            <span className="movie-icon"><img src={twittericon}/></span>
                            <h3>Twitter Feed</h3>
                        </div>
                        <a href="#" className="view-all">View All</a>
                    </div>
                
                <div className="slider">
                    <Carousel
                        swipeable={false}
                        draggable={false}
                        showDots={false}
                        responsive={responsive}
                        ssr={true} // means to render carousel on server-side.
                        infinite={true}
                        autoPlay={this.props.deviceType !== "mobile" ? false : false}
                        autoPlaySpeed={1000}
                        keyBoardControl={true}
                        transitionDuration={500}
                        containerClass="carousel-container"
                        removeArrowOnDeviceType={["tablet", "mobile"]}
                        deviceType={this.props.deviceType}
                        dotListClass="custom-dot-list-style"
                        itemClass="carousel-item-padding-40-px"
                    >
                        <div>                       
                            <p>Wonderful initiative by GHMC & TS govt. Tweeps lets put it to best use. Spread the word. Hope other Indian cities</p>
                            <div>
                                <img src={TwitterFeed01} className="x-auto float-left"></img>
                                <div className="author">
                                    <h4>Allu Sirish</h4>
                                    <span>3 Hrs ago</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p>Wonderful initiative by GHMC & TS govt. Tweeps lets put it to best use. Spread the word. Hope other Indian cities</p>
                            <div>
                                <img src={TwitterFeed01} className="x-auto float-left"></img>
                                <div className="author">
                                    <h4>Allu Arjum</h4>
                                    <span>9 Hrs ago</span>
                                </div>
                            </div>
                        </div>
                    </Carousel>
                </div>
                </div>
            </div>
        );
    }
}



export default TwitterFeed;