import React from 'react';
import './rightSideBar.scss';
import BgImage from '../../img/rightSideBarImages/play-win-bgimg.png';


var sectionStyle = {
    height:'224px',   
    backgroundImage: `url(${BgImage})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
};
class PlayAndWin extends React.Component {

    render() {
        return (
            <div className="advt-container m-t-40 play-win-container" style={sectionStyle}>                
                          <h2><span className="circle">?</span>Play & Win</h2>    
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                              </p> 
                              <footer>
                                  <small>Friends online</small>
                                  <div className="col-12 no-padding d-inline-flex">
                                      <ul>
                                          <li></li>
                                          <li></li>
                                          <li></li>
                                          <li></li>
                                      </ul>
                                      <button type="button">Play Now</button>
                                  </div>
                                  
                              </footer>
            </div>

        );
    }
}

export default PlayAndWin;