import React from 'react';
import './rightSideBar.scss';
import Slider from './Slider';
import YouTubeVideo from './YouTubeVideo';
import MovieMeter from './MovieMeter';
import Advertisement from './Advertisement';
import Slider1 from './Slider1';
import PlayAndWin from './PlayAndWin';
import Promo from './Promo';
import InstaFeed from './InstaFeed';
import TwitterFeed from './TwitterFeed';
import Advertisement02 from './Advertisement02';
import FB from './FBslider';

class RightSideBar extends React.Component {

    render() {
        return (
            <div className="right-sidebar">
                <Slider></Slider>
                <YouTubeVideo></YouTubeVideo>
                <MovieMeter></MovieMeter>
                <YouTubeVideo></YouTubeVideo>
                <MovieMeter></MovieMeter>
                <Advertisement></Advertisement>
                <Slider1></Slider1>
                <Advertisement></Advertisement> 
                <PlayAndWin></PlayAndWin>
                <Promo></Promo>
                <InstaFeed></InstaFeed>
                <Promo></Promo>
                <TwitterFeed></TwitterFeed>
                <Advertisement02></Advertisement02>
                <FB></FB>
            </div>
        );
    }
}

export default RightSideBar;