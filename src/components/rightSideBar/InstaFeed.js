import React from 'react';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import InstaFeed01 from '../../img/sliderImages/insta-feed01.png';
import image1 from '../../img/video6.jpg';
const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
};
class InstaFeed extends React.Component {

    render() {
        return (
            <div className="insta-feed insta-slider">
                <div className="slider-container">
                <div className="collection-detail">
                    <div className="d-flex align-items-center">
                        <h3>Insta Feed</h3>
                    </div>
                    <a href="#" className="view-all">View All</a>
                </div>
                    {/* <header>
                        <div className="float-left d-flex">
                            <h2 className="no-padding">Insta Feed</h2>
                        </div>
                        <a href="#" className="view-all">View All</a>
                    </header> */}
                </div>
                <div className="slider">
                    <div className="">
                        <Carousel
                            swipeable={false}
                            draggable={false}
                            showDots={false}
                            responsive={responsive}
                            ssr={true} // means to render carousel on server-side.
                            infinite={true}
                            autoPlay={this.props.deviceType !== "mobile" ? false : false}
                            autoPlaySpeed={1000}
                            keyBoardControl={true}
                            transitionDuration={500}
                            containerClass="carousel-container"
                            removeArrowOnDeviceType={["tablet", "mobile"]}
                            deviceType={this.props.deviceType}
                            dotListClass="custom-dot-list-style"
                            itemClass="carousel-item-padding-40-px"
                        >
                            {/* <div className="slider-body">
                                <div className="col-12 no-padding">
                                    <img src={InstaFeed01}></img>
                                    </div>                               
                            </div>
                            <div className="slider-body">
                                <div className="col-12 no-padding">
                                    <img src={InstaFeed01}></img>
                                    </div>                               
                            </div> */}
                            <div className="row insta-feeds">

                                <div className="column">
                                    <img src={InstaFeed01} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={image1} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={InstaFeed01} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={image1} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={InstaFeed01} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={image1} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={InstaFeed01} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={image1} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={InstaFeed01} width="100%"></img>
                                </div>                                                          
                            </div>

                            <div className="row insta-feeds">

                                <div className="column">
                                    <img src={image1} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={image1} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={InstaFeed01} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={image1} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={InstaFeed01} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={image1} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={InstaFeed01} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={image1} width="100%"></img>
                                </div>
                                <div className="column">
                                    <img src={InstaFeed01} width="100%"></img>
                                </div>                                                          
                            </div>
                        </Carousel>
                    </div>
                </div>
            </div>

        );
    }
}



export default InstaFeed;