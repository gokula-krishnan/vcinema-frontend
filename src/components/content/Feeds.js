import React from 'react';
import MainContent from '../middleContent/MainContent';
import RightSideBar from '../rightSideBar/RightSideBar';
import { Route, BrowserRouter } from 'react-router-dom';
import HeaderLeftMenu from '../header/headerTopLeftMenu/HeaderLeftMenu';

class Feeds extends React.Component {
    render() {
        return (
            <div>
                <HeaderLeftMenu></HeaderLeftMenu>
                <div className="container-fluid pt-80">                    
                    <div className="row">
                        <div className="col-12 col-md-9">
                            <MainContent></MainContent>
                        </div>
                        <div className="col-12 col-md-3 mt-0 mt-md-5">
                            <RightSideBar></RightSideBar>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default Feeds;