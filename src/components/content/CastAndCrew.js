 import React from 'react';
import './content.scss';
import Header from '../header';
import { windowWidth, NumberOfItems, ccAllStream, ccAwardStream, ccSearchFilter } from '../../actions';
import { connect } from 'react-redux';
import RenderList from '../utils/RenderList';
//import RenderDummy from '../utils/RenderDummy';
import CnCPop from '../utils/CnCPop';
//import CnCFilter from '../filters/CnCFilter';
import Filter2 from '../filters/Filter2';
import algoliasearch from 'algoliasearch/lite';
import InfiniteScroll from 'react-infinite-scroller';
import base from '../../api/base';

const style = {
    width: '93%',
    marginLeft: '7%'
}


const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');
const index = searchClient.initIndex('AZ_ARTIST_SEARCH');


class CastAndCrew extends React.Component {
    state = {
        item: null,
        dummyDataDetail: null,
        CCAllStream: [],
        CCAwardStream: [],
        hasMore: true,
        pageNum: null,
    }
    componentDidMount() {     
        for(var i=1; i<=21; i++) {
            base.get(`/artists?include=wallpapers&page=${i}`)
            .then(res => {
                let abc = res.data.data.filter( item => item.occupation.length )
                console.log('a')
                console.log(abc)
            })

        }
        this.props.ccAllStream('1')
        .then(res => this.setState({
            CCAllStream: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
        //this.props.selectFixedNavLink(this.props.location.pathname);
        this.props.windowWidth(window.innerWidth);      
        this.numOfItemInARow(window.innerWidth);
        window.addEventListener('resize', this.updateWindowDimensions);          
    }

    updateWindowDimensions = () => {
        this.props.windowWidth(window.innerWidth);        
        this.numOfItemInARow(this.props.width);
    }

    numOfItemInARow = (width) => {
        if( width <= 575.98 ) {
            this.props.NumberOfItems(1);
            return;
        } else if( (576 <= width) && (width <= 767.98) ) {
            this.props.NumberOfItems(3);            
            return;

        } else if( (768 <= width) && (width <= 991.98) ) {
            this.props.NumberOfItems(4);            
            return;

        } else if( (992 <= width) && (width <= 1199.98) ) {
            this.props.NumberOfItems(5);            
            return;

        } else if( (1200 <= width) ){
            this.props.NumberOfItems(6);            
            return;            
        }
    }
    fetchMoreData = (page) => {
         if(page < this.state.pageNum){
             this.props.ccAllStream(page)
             .then(res => this.setState({
                CCAllStream: [ ...this.state.CCAllStream, ...res.data.data ]
             }));          
         }
         //console.log(this.state.pageNum)
         if(page === this.state.pageNum){
             this.props.ccAllStream(page)
             .then(res => this.setState({
                CCAllStream: [ ...this.state.CCAllStream, ...res.data.data ],
                hasMore: false
            }))
            
         }
        }
    awarded = () => {
        this.props.ccAwardStream('1')
        .then(res => this.setState({ CCAwardStream: res.data }))  
    }
    sortList () {
        //console.log(this.state.CCAllStream)
        if( this.props.header === '#popular'){
            const imgLists = this.state.CCAllStream;
            
            return (
                <InfiniteScroll
                pageStart={1}
                loadMore={this.fetchMoreData}
                hasMore={this.state.hasMore}
                loader={<p></p>}
                initialLoad= {false}
            >
                <RenderList imgLists={imgLists} fixedNavLink="cc">
                        <CnCPop />
                    </RenderList>
            </InfiniteScroll>);
        } 

        if( this.props.header === '#awarded'){
            if(!this.state.CCAwardStream.length){
                this.awarded();
            }
            const imgLists = this.state.CCAwardStream;
                               
            return (<RenderList imgLists={imgLists} fixedNavLink="cc">
                        <CnCPop />
                    </RenderList>);
        }  

        if( this.props.header === '#stars'){
            return (<RenderList fixedNavLink="cc">
                        <CnCPop />
                    </RenderList>);
        } 
    }
    stats = (stats, res) => {
        if(stats === "STATS"){
            this.setState({ CCAwardStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages})                        
        }
        else if(stats === "NO OF MOVIES"){//Years Active Success Ratio Movies Per Year
            let response = res.data.data
                        .sort((a, b) => {
                            return a.movies.data.length - b.movies.data.length;
                        })
                        .reverse();
            this.setState({ CCAwardStream: response,
                pageNum: res.data.meta.pagination.total_pages})
        }
        else if(stats === "YEARS ACTIVE"){//Years Active Success Ratio Movies Per Year
            let response = res.data.data
                        .sort((a, b) => {
                            return a.years_active - b.years_active;
                        })
                        .reverse();
            this.setState({ CCAwardStream: response,
                pageNum: res.data.meta.pagination.total_pages})
        }
        else if(stats === "SUCCESS RATIO"){//Years Active Success Ratio Movies Per Year
            let response = res.data.data
                        .sort((a, b) => {
                            return a.success_ratio - b.success_ratio;
                        })
                        .reverse();
            this.setState({ CCAwardStream: response,
                pageNum: res.data.meta.pagination.total_pages})
        }
        else if(stats === "MOVIES PER YEAR"){//Years Active Success Ratio Movies Per Year
            let response = res.data.data
                        .sort((a, b) => {
                            return a.success_ratio - b.success_ratio;
                        })
                        .reverse();
            this.setState({ CCAwardStream: response,
                pageNum: res.data.meta.pagination.total_pages})
        }
        else{
            console.log(res)
        }
    }

    stats2 = (stats, res) => {
        if(stats === "STATS"){
            this.setState({ CCAllStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages})                        
        }
        else if(stats === "NO OF MOVIES"){//Years Active Success Ratio Movies Per Year
            let response = res.data.data
                        .sort((a, b) => {
                            return a.movies.data.length - b.movies.data.length;
                        })
                        .reverse();
            this.setState({ CCAllStream: response,
                pageNum: res.data.meta.pagination.total_pages})
        }
        else if(stats === "YEARS ACTIVE"){//Years Active Success Ratio Movies Per Year
            let response = res.data.data
                        .sort((a, b) => {
                            return a.years_active - b.years_active;
                        })
                        .reverse();
            this.setState({ CCAllStream: response,
                pageNum: res.data.meta.pagination.total_pages})
        }
        else if(stats === "SUCCESS RATIO"){//Years Active Success Ratio Movies Per Year
            let response = res.data.data
                        .sort((a, b) => {
                            return a.success_ratio - b.success_ratio;
                        })
                        .reverse();
            this.setState({ CCAllStream: response,
                pageNum: res.data.meta.pagination.total_pages})
        }
        else if(stats === "MOVIES PER YEAR"){//Years Active Success Ratio Movies Per Year
            let response = res.data.data
                        .sort((a, b) => {
                            return a.success_ratio - b.success_ratio;
                        })
                        .reverse();
            this.setState({ CCAllStream: response,
                pageNum: res.data.meta.pagination.total_pages})
        }
        else{
            console.log(res)
        }
    }
    popularFilter = (genre, age, year, zodiac, indus, stats, social, checkbox) => {
        const facetA = [], facetY = [], facetZ = [], facetI = [], facetC = [], uuidArry =[], profession = [], socialId=[],
        gender = {
            M: "MALE",
            F: "FEMALE"
        };

        if(zodiac.length){
            (zodiac.map ( each => facetZ.push(`sun_sign:${each}`)))
        }
        if(genre.length){
            (genre.map ( each => profession.push(each.toUpperCase())))
        }
        if(indus.length){
            (indus.map ( each => facetI.push(`industries:${each}`)))
        }
        if(checkbox.length){
            (checkbox.map ( each => facetC.push(`gender:${gender[each]}`)))//facetC.push(`gender:${each}`)
        }
        if((age[0] !== 0) && (age[1] !== 100)){
            for(var i=Math.round(age[0]); i<=Math.round(age[1]); i++){
                facetA.push(`age:${i} years`)
            }
        }
        if((year[0] !== 1930) && (year[1] !== 2019)){
            facetY.push(Math.round(year[0]), Math.round(year[1]))
        }
       
        index.search("", {
            "facets": "*,age,sun_sign,industries,gender",
            "facetFilters": [ facetA, facetC, facetZ, facetI ],
        },
        (err, { hits } = {}) => {
            if (err) throw err;  
            if(social !== 'SOCIAL'){
                if(social === "Facebook"){
                    hits.map(hit => hit.facebook.length ? uuidArry.push(hit.uuid) : null);
                }
                else if(social === "Twitter"){
                    hits.map(hit => hit.twitter.length ? uuidArry.push(hit.uuid) : null);

                }
                else if(social === "Instagram"){
                    hits.map(hit => hit.instagram.length ? uuidArry.push(hit.uuid) : null);
                }
            }else {
                hits.map( hit => uuidArry.push(hit.uuid))
            }
        });
        //console.log(profession)

        setTimeout(() => {  
            if(!facetC.length && !facetZ.length && !facetI.length && (age[0] === 0) && (age[1] === 100)){
                //console.log("now")
                let url=null;
                if(facetY.length){
                    url = `/artists?type=${profession.join()}&include=wallpapers,movies&from=${facetY[0]}&to=${facetY[1]}`
                }else{
                    url = `/artists?type=${profession.join()}&include=wallpapers,movies`;
                }
                this.props.ccSearchFilter(url)
                .then(res => this.stats2(stats, res))
            }
            else if(uuidArry.length){  
                let url=null;
                if(facetY.length){
                    url = `/artists?type=${profession.join()}&from=${facetY[0]}&to=${facetY[1]}&include=wallpapers,movies&azSearch=${uuidArry.join()}`
                }else{
                    url = `/artists?type=${profession.join()}&include=wallpapers,movies&azSearch=${uuidArry.join()}`;
                }
                this.props.ccSearchFilter(url)
                .then(res => this.stats2(stats, res))
            }
            else{
                this.setState({ CCAllStream: []})
            }
        }, 500) 
    }
    awardFilter = (genre, age, year, zodiac, indus, stats, social, checkbox) => {
        const facetA = [], facetY = [], facetZ = [], facetI = [], facetC = [], uuidArry =[], profession = [],
        gender = {
            M: "MALE",
            F: "FEMALE"
        };

        if(zodiac.length){
            (zodiac.map ( each => facetZ.push(`sun_sign:${each}`)))
        }
        if(genre.length){
            (genre.map ( each => profession.push(each.toUpperCase())))
        }
        if(indus.length){
            (indus.map ( each => facetI.push(`industries:${each}`)))
        }
        if(checkbox.length){
            (checkbox.map ( each => facetC.push(`gender:${gender[each]}`)))//facetC.push(`gender:${each}`)
        }
        if((age[0] !== 0) && (age[1] !== 100)){
            for(var i=Math.round(age[0]); i<=Math.round(age[1]); i++){
                facetA.push(`age:${i} years`)
            }
        }
        if((year[0] !== 1930) && (year[1] !== 2019)){
            facetY.push(Math.round(year[0]), Math.round(year[1]))
        }
        index.search("", {
            "facets": "*,age,sun_sign,industries,gender",
            "facetFilters": [ facetA, facetC, facetZ, facetI ],
        },
        (err, { hits } = {}) => {
            if (err) throw err;  
            if(social !== 'SOCIAL'){
                if(social === "Facebook"){
                    hits.map(hit => hit.facebook.length ? uuidArry.push(hit.uuid) : null);
                }
                else if(social === "Twitter"){
                    hits.map(hit => hit.twitter.length ? uuidArry.push(hit.uuid) : null);

                }
                else if(social === "Instagram"){
                    hits.map(hit => hit.instagram.length ? uuidArry.push(hit.uuid) : null);
                }
            }else {
                hits.map( hit => uuidArry.push(hit.uuid))
            }
        });
        //console.log(profession)

        setTimeout(() => {  
            //console.log()
            if(!facetC.length && !facetZ.length && !facetI.length && (age[0] === 0) && (age[1] === 100)){
                //console.log("now")
                let url=null;
                if(facetY.length){
                    url = `/artists/category/most_awarded?type=${profession.join()}&include=wallpapers,movies&from=${facetY[0]}&to=${facetY[1]}`
                }else{
                    url = `/artists/category/most_awarded?type=${profession.join()}&include=wallpapers,movies`;
                }
                this.props.ccSearchFilter(url)
                .then(res => this.stats(stats, res))
            }
            else if(uuidArry.length){  
                let url=null;
                if(facetY.length){
                    url = `/artists/category/most_awarded?type=${profession.join()}&from=${facetY[0]}&to=${facetY[1]}&include=wallpapers,movies&azSearch=${uuidArry.join()}`
                }else{
                    url = `/artists/category/most_awarded?type=${profession.join()}&include=wallpapers,movies&azSearch=${uuidArry.join()}`;
                }
                this.props.ccSearchFilter(url)
                .then(res => this.stats(stats, res))
            }
        }, 500) 
    }
    contentChange = ({ genre, age, year, zodiac, indus, stats, social, checkbox } ) => {
       if(this.props.header === '#popular'){
        this.popularFilter(genre, age, year, zodiac, indus, stats, social, checkbox);
        }
        else if( this.props.header === '#awarded'){
            this.awardFilter(genre, age, year, zodiac, indus, stats, social, checkbox)
        }
        
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    render() {       
        return (        
            <div className="flex-column d-inline-flex" style={style}>
                <Header activeLink='cc' />

                {/* { this.props.filterState ? <CnCFilter checkItemsLength={this.contentChange} /> : null }  */}
                { this.props.filterState ? <Filter2 checkItemsLength={this.contentChange} /> : null }
                
                {/* <CnCFilter checkItemsLength={this.contentChange}/> */}
                {/* { this.props.filterState ? <Filter2 checkItemsLength={this.contentChange} filterCallback={this.dummyDataDetailUpdate}/> : null }                  */}
                {/* <Filter2 checkItemsLength={this.contentChange} filterCallback={this.dummyDataDetailUpdate}/> */}

                <div className="image-cont container-fluid text-light">
                    {/* {this.state.item ? this.filterSortList() : this.sortList() } */}
                    {this.sortList()}
                    {/* {this.state.dummyDataDetail ? this.filterDummySortList() : this.sortList() } */}
                    {/* {this.sortList()} */}
                </div>

                {/* <div className="image-cont container-fluid text-light">
                    {this.sortList()}
                </div> */}
            </div>
        )
    }
      
}



const mapStateToProps = state => {
    return{
        items: state.items,
        width: state.width,
        castImg: state.castImg,
        header: state.selectedHeader,
        filterState: state.selectedFilter,
        fetchstream: state.fetchstream,
        CCAll: state.CCall
    }
}



export default connect(mapStateToProps, { windowWidth, NumberOfItems, ccSearchFilter, ccAllStream, ccAwardStream })(CastAndCrew);