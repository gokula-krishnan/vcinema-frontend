import React, { Suspense, lazy} from 'react';
import './content.scss';
import Header from '../header';
import { windowWidth, NumberOfItems, selectFixedNavLink,musicSearchFilter,musicAwardedFilter, musicAllStream, musicAwardPost, musicRelStream, musicAwardStream } from '../../actions';
import { connect } from 'react-redux';
// import RenderList from '../utils/RenderList';
import MusicPop from '../utils/MusicPop';
import Filter3 from '../filters/Filter3';
import algoliasearch from 'algoliasearch/lite';
import InfiniteScroll from 'react-infinite-scroller';

const RenderList = React.lazy(() => import('../utils/RenderList'));

const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');
const index = searchClient.initIndex('AZ_ARTIST_SEARCH');

const style = {
    width: '93%',
    marginLeft: '7%'
}


class Music extends React.Component {
    state= {
        item: null,
        musicAllStream: [],
        musicRelStream: [],
        musicAwardStream: [],
        hasMore: true,
        pageNum: null
    }
    componentDidMount() {  
        //this.props.selectFixedNavLink(this.props.location.pathname);
        this.props.musicAllStream('1')
        .then(res => this.setState({
            musicAllStream: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
        this.props.windowWidth(window.innerWidth);      
        this.numOfItemInARow(window.innerWidth);
        window.addEventListener('resize', this.updateWindowDimensions);  
       
    }
    releases = () => {  
            this.props.musicRelStream('1')
            .then(res => this.setState({ musicRelStream: res.data }))   
    }
    awarded = () => {
        this.props.musicAwardStream()
        .then(res => {
                    this.props.musicAwardPost(res)
                    .then(res => this.setState({ musicAwardStream: res.data }))
                    .catch(err => console.warn(err))
        }
        ) 
            
    }
    updateWindowDimensions = () => {
        this.props.windowWidth(window.innerWidth);        
        this.numOfItemInARow(this.props.width);
    }

    numOfItemInARow = (width) => {
        if( width <= 575.98 ) {
            this.props.NumberOfItems(1);//1
            return;
        } else if( (576 <= width) && (width <= 767.98) ) {
            this.props.NumberOfItems(2);//2
            return;

        } else if( (768 <= width) && (width <= 991.98) ) {
            this.props.NumberOfItems(3);//3
            return;

        } else if( (992 <= width) && (width <= 1199.98) ) {
            this.props.NumberOfItems(4);//3
            return;

        } else if( (1200 <= width) ){
            this.props.NumberOfItems(5);//4
            return;            
        }
    }

    sortList () {

        if( this.props.header === '#popular'){
            const imgLists = this.state.musicAllStream;
            
            return (
                <InfiniteScroll
                    pageStart={1}
                    loadMore={this.fetchMoreData}
                    hasMore={this.state.hasMore}
                    loader={<p></p>}
                    initialLoad= {false}
                >
                    <Suspense fallback={<div>Loading...</div>}>
                    <RenderList imgLists={imgLists} fixedNavLink="music">
                        <MusicPop />
                    </RenderList>
                    </Suspense> 
                </InfiniteScroll>);
        }

        if( this.props.header === '#soon'){
            if(!this.state.musicRelStream.length){
            this.releases();
            }
            const imgLists = this.state.musicRelStream;

            return (<RenderList imgLists={imgLists} fixedNavLink="music">
                        <MusicPop />
                    </RenderList>);
        }

        if( this.props.header === '#awarded'){
            if(!this.state.musicAwardStream.length){
                this.awarded();
            }
            const imgLists = this.state.musicAwardStream;

            return (
                <RenderList imgLists={imgLists} fixedNavLink="music">
                    <MusicPop />
                </RenderList>);
        }

        if( this.props.header === '#stars' || this.props.header === '#radio'){
            return (
                <RenderList fixedNavLink="music">
                    <MusicPop />
                </RenderList>);
        }
    }

    popularFilter = ({chips, rating, year}) => {

        console.log(chips)
        const facetY = [], chipsUuid = [];
        chips.map(chip => 
            index.search({
              query: chip
              },
              (err, { hits } = {}) => {
                if (err) throw err;   
                //console.log(hits[0].uuid)
                chipsUuid.push(hits[0].uuid); 
              }
            )
        );

        if((year[0] !== 1930) && (year[1] !== 2019)){
            facetY.push(Math.round(year[0]), Math.round(year[1]))
        }
        setTimeout(() => {  
            if(chipsUuid.length && facetY.length){       
                //console.log('chips1')
            this.props.musicSearchFilter(`/albums?cc=${chipsUuid.join()}&include=bannerImages&from=${facetY[0]}&to=${facetY[1]}`)
            .then(res => this.setState({ musicAllStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
            else if(chipsUuid.length){
                //console.log('chips2')
                this.props.musicSearchFilter(`/albums?cc=${chipsUuid.join()}&include=bannerImages`)
                .then(res => this.setState({ musicAllStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
            else if(facetY.length){
                //console.log('chips3')
                this.props.musicSearchFilter(`/albums?cc=${chipsUuid.join()}&include=bannerImages&from=${facetY[0]}&to=${facetY[1]}`)
                .then(res => this.setState({ musicAllStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
        }, 500) 
         
    }

    soonFilter = ({chips, rating, year}) => {
        const facetY = [], chipsUuid = [];

        // this.setState({
        //     item: chips.length
        // });
        chips.map(chip => 
            index.search({
              query: chip
              },
              (err, { hits } = {}) => {
                if (err) throw err;   
                chipsUuid.push(hits[0].uuid); 
              }
            )
        );

        if((year[0] !== 1930) && (year[1] !== 2019)){
            facetY.push(Math.round(year[0]), Math.round(year[1]))
        }
        setTimeout(() => {  
            if(chipsUuid.length && facetY.length){       
            this.props.musicSearchFilter(`/albums/category/releases?cc=${chipsUuid.join()}&include=bannerImages&from=${facetY[0]}&to=${facetY[1]}`)
            .then(res => this.setState({ musicRelStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
            else if(chipsUuid.length){
                this.props.musicSearchFilter(`/albums/category/releases?cc=${chipsUuid.join()}&include=bannerImages`)
                .then(res => this.setState({ musicRelStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
            else if(facetY.length){
                this.props.musicSearchFilter(`/albums/category/releases?cc=${chipsUuid.join()}&include=bannerImages&from=${facetY[0]}&to=${facetY[1]}`)
                .then(res => this.setState({ musicRelStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
        }, 500) 
         
    }
    awardFilter = ({chips, rating, year}) => {
        const facetY = [], chipsUuid = [];

        console.log(chips)

        // this.setState({
        //     item: chips.length
        // });
        chips.map(chip => 
            index.search({
              query: chip
              },
              (err, { hits } = {}) => {
                if (err) throw err;   
                chipsUuid.push(hits[0].uuid); 
              }
            )
        );

        if((year[0] !== 1930) && (year[1] !== 2019)){
            facetY.push(Math.round(year[0]), Math.round(year[1]))
        }
        setTimeout(() => {  
            if(chipsUuid.length && facetY.length){       
            this.props.musicAwardedFilter(`/albums/id/items?cc=${chipsUuid.join()}&include=bannerImages&from=${facetY[0]}&to=${facetY[1]}`)
            .then(res => this.setState({ musicAwardStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
            else if(chipsUuid.length){
                this.props.musicAwardedFilter(`/albums/id/items?cc=${chipsUuid.join()}&include=bannerImages`)
                .then(res => this.setState({ musicAwardStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
            else if(facetY.length){
                this.props.musicAwardedFilter(`/albums/id/items?cc=${chipsUuid.join()}&include=bannerImages&from=${facetY[0]}&to=${facetY[1]}`)
                .then(res => this.setState({ musicAwardStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
        }, 500) 
         
    }
    contentChange = (items) => {
       
        if(this.props.header === '#popular'){
            this.popularFilter(items);
        }
        else if( this.props.header === '#soon'){
            this.soonFilter(items)
        }
        else if( this.props.header === '#awarded'){
            this.awardFilter(items)
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    render() {       
        return (        
            <div className="flex-column d-inline-flex" style={style}>
                <Header activeLink='music' />

                {/* { this.props.filterState ? <MusicFilter  checkItemsLength={this.contentChange} /> : null }  */}
                {/* <MusicFilter checkItemsLength={this.contentChange}/> */}
                {/* { this.props.filterState ? <Filter3 checkItemsLength={this.contentChange} filterCallback={this.dummyDataDetailUpdate}/> : null }                  */}
                { this.props.filterState ?<Filter3 checkItemsLength={this.contentChange} filterCallback={this.dummyDataDetailUpdate}/> : null }

                <div className="image-cont container-fluid text-light">
                {/* {this.sortList()} */}
                    {this.state.item ? this.filterSortList() : this.sortList() }
                    {/* {this.state.dummyDataDetail ? this.filterDummySortList() : this.sortList() } */}

                </div>
            </div>
        )
    }
      
}



const mapStateToProps = state => {
    return{
        items: state.items,
        width: state.width,
        musicImg: state.musicImg,
        header: state.selectedHeader,
        filterState: state.selectedFilter,
        fetchstream: state.fetchstream
    }
}



export default connect(mapStateToProps, { windowWidth, NumberOfItems,musicSearchFilter,musicAwardedFilter, selectFixedNavLink, musicAwardPost, musicAllStream, musicRelStream, musicAwardStream })(Music);