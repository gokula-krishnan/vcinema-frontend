import React, { Suspense, lazy} from 'react';
import './content.scss';
import { windowWidth, NumberOfItems, galleryAllStream, galleryStills, galleryPosters, galleryEvents,
galleryBehindScenes, galleryVintage, galleryWallpapers} from '../../actions';
import Header from '../header';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import GalleryFilter from '../filters/GalleryFilter';

// import { galleryStills } from '../../actions';
// import GalleryRender from '../utils/GalleryRender';
// import { Gallery } from '../../svgIcons';


const style = {
    width: '93%',
    marginLeft: '7%'
}

const GalleryRender = React.lazy(() => import('../utils/GalleryRender'));

class Gallery extends React.Component {


    state = {
        pageNum :null,
        galleryAllStream : [],
        galleryStillsstream : [],
        galleryPosterstream : [],
        galleryWallpapers : [],
        galleryEvents : [],
        galleryBehindScenes : [],
        galleryVintage : [],
        hasMore: true,
    }

    componentDidMount() {  
        this.props.galleryAllStream('1')
        // .then(res => res.data.data)
        // .then(json => {
        //     console.log(json);
        //     json.map(obj => this.setState({ galleryAllStream: Object.values(obj) }))
        .then(res => this.setState({
            galleryAllStream: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
        // })
        this.props.windowWidth(window.innerWidth);      
        this.numOfItemInARow(window.innerWidth);
        window.addEventListener('resize', this.updateWindowDimensions);  
    }

    getGalleryStills = () => {

        this.props.galleryStills('1')
        .then(res => this.setState({
            galleryStillsstream: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        })
        );
    }

    getGalleryPosters = () => {
        console.log(this.state.galleryPosters)
        this.props.galleryPosters('1')
        .then(res => this.setState({
            galleryPosterstream: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        })
        
        );
    }

    getWallpapers = () => {
        console.log(this.state.galleryPosters)
        this.props.galleryWallpapers('1')
        .then(res => this.setState({
            galleryWallpapers: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        })
        
        );
    }
    getEvents = () => {
        console.log(this.state.galleryPosters)
        this.props.galleryEvents('1')
        .then(res => this.setState({
            galleryEvents: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        })
        
        );
    }

    getBehindScenes = () => {
        console.log(this.state.galleryPosters)
        this.props.galleryBehindScenes('1')
        .then(res => this.setState({
            galleryBehindScenes: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        })
        
        );
    }

    getVintage = () => {
        console.log(this.state.galleryPosters)
        this.props.galleryVintage('1')
        .then(res => this.setState({
            galleryVintage: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        })
        
        );
    }

    numOfItemInARow = (width) => {
        if( width <= 575.98 ) {
            this.props.NumberOfItems(1);//1
            return;
        } else if( (576 <= width) && (width <= 767.98) ) {
            this.props.NumberOfItems(2);//2
            return;

        } else if( (768 <= width) && (width <= 991.98) ) {
            this.props.NumberOfItems(3);//3
            return;

        } else if( (992 <= width) && (width <= 1199.98) ) {
            this.props.NumberOfItems(3);//3
            return;

        } else if( (1200 <= width) ){
            this.props.NumberOfItems(4);//4
            return;            
        }
    }


    fetchMoreData = (page) => {
        console.log('more') 
            if(page < this.state.pageNum){
                this.props.galleryAllStream(page)
                .then(res => this.setState({
                    galleryAllStream: [ ...this.state.galleryAllStream, ...res.data.data ]
                })) 
            }
            else if(page === this.state.pageNum){
                this.props.galleryAllStream(page)
                .then(res => this.setState({
                    galleryAllStream: [ ...this.state.galleryAllStream, ...res.data.data ]
                }))
    
                this.setState({
                    hasMore: false
                });
            }        
    }


    popularFilter = (items, search, stream) => {


    }

      contentChange = (items, search, stream) => {
        //console.log('cont')
        if(this.props.header === '#popular'){
            this.popularFilter(items, search, stream);
        }
        // else if( this.props.header === '#soon'){
        //     this.soonFilter(items, search)
        // }
        // else if( this.props.header === '#awarded'){
        //     this.awardFilter(items, search, stream)
        // }
        // else if( this.props.header === '#full'){
        //     this.fullFilter(items, search, stream)
        // }
        
    }

    sortList () {

        if( this.props.header === '#popular'){


            let imgLists = this.state.galleryAllStream;
            let arrLists = [];

            for(var i=0; i<imgLists.length;i++){

                if(imgLists[i].imgTypeDetails.data.length){
                                     
                    if( imgLists[i].attachments.data.length){
                        arrLists.push({ src : imgLists[i].attachments.data[0].uri, width: 3, height :2, uuid : imgLists[i].uuid, id:imgLists[i].imgid,
                            section : imgLists[i].section, type : imgLists[i].type, name : imgLists[i].imgTypeDetails.data[0].name})
   
                    }
                  
                }
            }
            console.log(arrLists)
          
            return (
                <InfiniteScroll
                    pageStart={1}
                    loadMore={this.fetchMoreData}
                    hasMore={this.state.hasMore}
                    loader={<p></p>}
                    initialLoad= {false}
                >
                    <Suspense fallback={<div>Loading...</div>}>
                        <GalleryRender imgLists={arrLists}  fixedNavLink="gallery">
                        </GalleryRender>
                    </Suspense>
                </InfiniteScroll>
                    
                    );
        }

        else if( this.props.header === '#posters'){
            if(!this.state.galleryPosterstream.length){
                this.getGalleryPosters();
            }
            let imgLists = this.state.galleryPosterstream;
            
            console.log(imgLists);
            let arrLists = [];

            for(var i=0; i<imgLists.length;i++){
                if(imgLists[i].imgTypeDetails.data.length){
                    arrLists.push({ src : imgLists[i].attachments.data[0].uri, width: 3, height :2, uuid : imgLists[i].uuid, 
                        id:imgLists[i].imgid, section : imgLists[i].section, type : imgLists[i].type, name : imgLists[i].imgTypeDetails.data[0].name})
                }
            }
            console.log(arrLists)
          
            return (
                <Suspense fallback={<div>Loading...</div>}>
            <GalleryRender imgLists={arrLists}  fixedNavLink="gallery">
                    </GalleryRender>
                    </Suspense>);
        }
        else if(this.props.header === '#wallpapers'){
            if(!this.state.galleryWallpapers.length){
                this.getWallpapers();
            }
            
            var imgLists = this.state.galleryWallpapers;
        }
        else if(this.props.header === '#events'){
            if(!this.state.galleryEvents.length){
            this.getEvents();
            }
            var imgLists = this.state.galleryEvents;
        }
        else if(this.props.header === '#behindscene'){
            if(!this.state.galleryBehindScenes.length){
            this.getBehindScenes();
            }
            var imgLists = this.state.galleryBehindScenes;
        }
        else if(this.props.header === '#vintage'){
            if(!this.state.galleryVintage.length){
            this.getVintage();
            }
            var imgLists = this.state.galleryVintage;
        }   
        else if(this.props.header === '#stills'){   
            if(!this.state.galleryStillsstream.length){
            this.getGalleryStills();
            }
            var imgLists = this.state.galleryStillsstream;
        }

        let arrLists = [];

        for(var i=0; i<imgLists.length;i++){
            if(imgLists[i].imgTypeDetails.data.length){
                arrLists.push({ src : imgLists[i].attachments.data[0].uri, width: 3, height :2, uuid : imgLists[i].uuid, 
                    id:imgLists[i].imgid, section : imgLists[i].section, type : imgLists[i].type, name : imgLists[i].imgTypeDetails.data[0].name})
            }
        }
        console.log(arrLists)
      
        return (
            <Suspense fallback={<div>Loading...</div>}>
                <GalleryRender imgLists={arrLists}  fixedNavLink="gallery">
                </GalleryRender>
            </Suspense>);
       
    }


    render() {
        return (
            console.log('filterstate details are',this.props.filterState),
            <div className="flex-column d-inline-flex" style={style}>
                <Header activeLink='gallery' />
               
                { this.props.filterState ? <GalleryFilter  checkItemsLength={this.contentChange} update={this.selectedUuidUpdate}/> : null }
                <div className="image-cont container-fluid text-light">
                    {this.sortList()}
                </div>
            </div>
        )
    }



 }


 const mapStateToProps = state => {
    return{
        items: state.items,
        width: state.width,
        header: state.selectedHeader,
        filterState: state.selectedFilter,
    }
}

Gallery.propTypes = { 
    items: PropTypes.number,
    width: PropTypes.number,
    header: PropTypes.string.isRequired,
    filterState: PropTypes.bool.isRequired,
}

 export default connect(mapStateToProps, {windowWidth, NumberOfItems, galleryAllStream, galleryPosters, galleryStills, galleryWallpapers, galleryBehindScenes, galleryEvents, galleryVintage})(Gallery);