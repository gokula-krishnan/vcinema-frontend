import React from 'react';
import './content.scss';
import Header from '../header';
import { windowWidth, NumberOfItems, videoAllStream, videoSongs, videoBehindScenes, videoEvents, videoShortFilms,videoSocial, videoMiscellaneous, videoSearchFilter } from '../../actions';
import { connect } from 'react-redux';
import RenderList from '../utils/RenderList';
import VideoFilter from '../filters/VideoFilter';
import MultiSearch from '../filters/videoMultiSearch';
import YoutubeVideo from '../utils/YoutubeVideoPlay'; 
import PropTypes from 'prop-types';


const style = {
    width: '93%',
    marginLeft: '7%'
}


class Video extends React.Component {
    state= {
        videoAllStream: [],
        videoSongs : [],
        videoBehindScenes :[],
        videoEvents : [],
        videoShortFilms : [],
        videoMiscellaneous :[],
        videoSocial : [],
        pageNum : null,
        selectedUuid :null
       
    }

    componentDidMount() {  
        this.props.videoAllStream('1')
        .then(res => this.setState({
            videoAllStream: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));

        this.props.windowWidth(window.innerWidth);      
        this.numOfItemInARow(window.innerWidth);
        window.addEventListener('resize', this.updateWindowDimensions);  
    }

    getVideoSongs () {

        this.props.videoSongs('1')
        .then(res => this.setState({
            videoSongs: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
    }
    
    getVideoEvents () {
        this.props.videoEvents('1')
        .then(res => this.setState({
            videoEvents: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
    }

    getVideoBehindScenes () {
        this.props.videoBehindScenes('1')
        .then(res => this.setState({
            videoBehindScenes: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
    }

    getVideoSocial () {
        this.props.videoSocial('1')
        .then(res => this.setState({
            videoSocial: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
    }

    getVideoShortFilms () {
        this.props.videoShortFilms('1')
        .then(res => this.setState({
            videoShortFilms: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
    }

    getVideoMiscellaneous () {
        this.props.videoMiscellaneous('1')
        .then(res => this.setState({
            videoMiscellaneous: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
    }

    popularFilter = (items, search, stream) => { 

     
        var items = JSON.stringify(items);
        this.props.videoSearchFilter(`/video?cc=${search}&date=${items}`)
        .then(function(res){
            console.log(res)
            return res;

        })
        .then(function(res){
            console.log(res)
                // this.setState({ videoAllStream: res.data.data,
                // pageNum: res.data.meta.pagination.total_pages})
        })

    }

    contentChange = (items, search, stream) => {
      console.log('cont')
        if(this.props.header === '#popular'){
            this.popularFilter(items, search, stream);
        }
      
        
    }

    selectedUuidUpdate = (responseHits) => {
        console.log(responseHits)
        this.setState({ selectedUuid: responseHits });
    }
    updateWindowDimensions = () => {
        this.props.windowWidth(window.innerWidth);        
        this.numOfItemInARow(this.props.width);
    }

    numOfItemInARow = (width) => {
        if( width <= 575.98 ) {
            this.props.NumberOfItems(1);//1
            return;
        } else if( (576 <= width) && (width <= 767.98) ) {
            this.props.NumberOfItems(2);//2
            return;

        } else if( (768 <= width) && (width <= 991.98) ) {
            this.props.NumberOfItems(3);//3
            return;

        } else if( (992 <= width) && (width <= 1199.98) ) {
            this.props.NumberOfItems(3);//3
            return;

        } else if( (1200 <= width) ){
            this.props.NumberOfItems(4);//4
            return;            
        }
    }


    sortList () {

        if( this.props.header === '#popular'){
            const imgLists = this.state.videoAllStream;
            return (<RenderList imgLists={imgLists}  fixedNavLink="video">
                    </RenderList>);
        }

        if( this.props.header === '#songs'){
            if(!this.state.videoSongs.length){
                this.getVideoSongs()
            }
            const imgLists = this.state.videoSongs;          
            return (<RenderList imgLists={imgLists} fixedNavLink="video">
                        
                    </RenderList>);
        }

        if( this.props.header === '#events'){
            if(!this.state.videoEvents.length){
                this.getVideoEvents()
            }
            const imgLists = this.state.videoEvents;          
            return (<RenderList imgLists={imgLists} fixedNavLink="video">
                    </RenderList>);
        }

        if( this.props.header === '#behindscene'){
            if(!this.state.videoBehindScenes.length){
                this.getVideoBehindScenes()
            }
            const imgLists = this.state.videoBehindScenes;          
            return (<RenderList imgLists={imgLists} fixedNavLink="video">
                    </RenderList>);
        }

        if( this.props.header === '#social'){
            if(!this.state.videoSocial.length){
                this.getVideoSocial()
            }
            const imgLists = this.state.videoSocial;          
            return (<RenderList imgLists={imgLists} fixedNavLink="video">
                    </RenderList>);
        }

        if( this.props.header === '#shortfilms'){
            if(!this.state.videoShortFilms.length){
                this.getVideoShortFilms()
            }
            const imgLists = this.state.videoShortFilms;          
            return (<RenderList imgLists={imgLists} fixedNavLink="video">
                    </RenderList>);
        }

        if( this.props.header === '#miscellaneous'){
            if(!this.state.videoMiscellaneous.length){
                this.getVideoMiscellaneous()
            }
            const imgLists = this.state.videoMiscellaneous;          
            return (<RenderList imgLists={imgLists} fixedNavLink="video">
                    </RenderList>);
        }

        if( this.props.header === '#stars' || this.props.header === '#radio'){
            return (
                <RenderList fixedNavLink="video">
                </RenderList>);
        }
    }


    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }


    render() {
        return (
            console.log('filterstate details are',this.props.filterState),
            <div className="flex-column d-inline-flex" style={style}>
                <Header activeLink='video' />
                 {/* { this.props.filterState ?<MultiSearch/> : null } */}

                { this.props.filterState ? <VideoFilter  checkItemsLength={this.contentChange} update={this.selectedUuidUpdate}/> : null }
                <div className="image-cont container-fluid text-light">
                    {this.sortList()}
                </div>
            </div>
        )
    }
      
}

const mapStateToProps = state => {
    return{
        items: state.items,
        width: state.width,
        videoImg: state.videoImg,
        header: state.selectedHeader,
        videoAll: state.videoAll,
        filterState: state.selectedFilter,
    }
}

Video.propTypes = { 
    items: PropTypes.number,
    width: PropTypes.number,
    header: PropTypes.string.isRequired,
    filterState: PropTypes.bool.isRequired,
}

export default connect(mapStateToProps, { windowWidth, NumberOfItems, videoAllStream, videoSongs, videoEvents, videoBehindScenes, videoSocial, videoShortFilms, videoMiscellaneous, videoSearchFilter })(Video);