import React, { Suspense, lazy} from 'react';
import './content.scss';
import Header from '../header';
//import Filter from '../filters/Filter';
import MoviesFilter from '../filters/MoviesFilter';
import { windowWidth, NumberOfItems, selectedImgLists, 
    fetchStreams, movieAllStream , movieRelStream, 
    movieFullStream, movieAwardStream, movieSearchFilter, 
    movieAwardPost, movieAwardedFilter} from '../../actions';
import { connect } from 'react-redux';
// import RenderList from '../utils/RenderList';
//import RenderDummy from '../utils/RenderDummy';
//import FilterServices from '../utils/FilterServices';
import MoviePop from '../utils/MoviePop';
//import base from '../../api/base';
import InfiniteScroll from 'react-infinite-scroller';
import PropTypes from 'prop-types';
import algoliasearch from 'algoliasearch/lite';

const style = {
    width: '93%',
    marginLeft: '7%'
}
//let activeHeader = null;

const RenderList = React.lazy(() => import('../utils/RenderList'));

const searchClient = algoliasearch('WKNVJIHPWZ', '7012b29c8518228c184abf5894c2aeb2');
const index = searchClient.initIndex('AZ_ARTIST_SEARCH');
const mvIndex = searchClient.initIndex('AZ_MOVIE_SEARCH');

class Movie extends React.Component {

    state= {
        item: null,
        movieAllStream: [],
        movieReleases: [],
        movieAward: [],
        movieFullmovies: [],
        hasMore: true,
        pageNum: null,
        selectedUuid: null
    }
    componentDidMount() {
        //this.movieAwardPosta();
       // activeHeader = this.props.header;
      
        this.props.movieAllStream('1')
        .then(res => this.setState({
            movieAllStream: res.data.data,
            pageNum: res.data.meta.pagination.total_pages
        }));
        //.then(res => this.setState({movieAllStream: res.data}))
        this.props.windowWidth(window.innerWidth);      
        this.numOfItemInARow(window.innerWidth);
        window.addEventListener('resize', this.updateWindowDimensions);  
        
    }
    
    releases = () => {  
        //if(!this.state.movieReleases.length){
            this.props.movieRelStream('1')
            .then(res => this.setState({ movieReleases: res.data }))   
            .catch(err => console.warn(err))        
        //}
    }
    awarded = () => {
        this.props.movieAwardStream()
        .then(res => {
                    this.props.movieAwardPost(res)
                    .then(res => this.setState({ movieAward: res.data }))
                    .catch(err => console.warn(err))
        }
        )    
    }
    fullmovies = () => {
       // console.log('this??')
            this.props.movieFullStream('1')
            .then(res => this.setState({ movieFullmovies: res.data,
                pageNum: res.meta.pagination.total_pages }))  
    }
    updateWindowDimensions = () => {
        this.props.windowWidth(window.innerWidth);        
        this.numOfItemInARow(this.props.width);
    }

    numOfItemInARow = (width) => {

        if( width <= 575.98 ) {
            this.props.NumberOfItems(1);
            return;
        } else if( (576 <= width) && (width <= 767.98) ) {
            this.props.NumberOfItems(3);            
            return;

        } else if( (768 <= width) && (width <= 991.98) ) {
            this.props.NumberOfItems(4);            
            return;

        } else if( (992 <= width) && (width <= 1199.98) ) {
            this.props.NumberOfItems(5);            
            return;

        } else if( (1200 <= width) ){
            this.props.NumberOfItems(6);            
            return;            
        }
    }

    fetchMoreData = (page) => {     
        console.log('more') 
            if(page < this.state.pageNum){
                this.props.movieAllStream(page)
                .then(res => this.setState({
                    movieAllStream: [ ...this.state.movieAllStream, ...res.data.data ]
                })) 
            }
            else if(page === this.state.pageNum){
                this.props.movieAllStream(page)
                .then(res => this.setState({
                    movieAllStream: [ ...this.state.movieAllStream, ...res.data.data ]
                }))
    
                this.setState({
                    hasMore: false
                });
            }        
    }
    fetchMore = (page) => {
            if(page < this.state.pageNum){
                this.props.movieFullStream(page)
                .then(res => this.setState({
                    movieFullmovies: [ ...this.state.movieFullmovies, ...res.data ]
                })) 
            }
            else if(page === this.state.pageNum){
                this.props.movieFullStream(page)
                .then(res => this.setState({
                    movieFullmovies: [ ...this.state.movieFullmovies, ...res.data ]
                }))
    
                this.setState({
                    hasMore: false
                });
            }
    }

    sortList = () => {
        if((this.props.header === '#popular')){//(activeHeader !== this.props.header) && 
            const imgLists = this.state.movieAllStream;
            console.log('pop')
            return (
                <InfiniteScroll
                    pageStart={1}
                    loadMore={this.fetchMoreData}
                    hasMore={this.state.hasMore}
                    loader={<p></p>}
                    initialLoad= {false}
                >

                    <Suspense fallback={<div>Loading...</div>}>
                        <RenderList imgLists={imgLists} fixedNavLink="movies">
                            <MoviePop />
                        </RenderList>
                    </Suspense> 
                </InfiniteScroll>
            );
        }

        else if( this.props.header === '#soon'){
            if(!this.state.movieReleases.length){
                this.releases();
            }
            const imgLists = this.state.movieReleases;
            return (
                <RenderList imgLists={imgLists} fixedNavLink="movies">
                    <MoviePop />
                </RenderList>);
        }

        else if( this.props.header === '#awarded'){
            if(!this.state.movieAward.length){
                this.awarded();
            }
            const imgLists = this.state.movieAward.reverse();
            return (
                <RenderList imgLists={imgLists} fixedNavLink="movies">
                    <MoviePop />
                </RenderList>);
        }

        else if( this.props.header === '#full'){
            if(!this.state.movieFullmovies.length){
                this.fullmovies();
            }
            const imgLists = this.state.movieFullmovies;
            return (
                <InfiniteScroll
                    pageStart={1}
                    loadMore={this.fetchMore}
                    hasMore={this.state.hasMore}
                    loader={<p></p>}
                    initialLoad= {false}
                >
                    <RenderList imgLists={imgLists} fixedNavLink="movies">
                        <MoviePop />
                    </RenderList>
                </InfiniteScroll>);
        }

        else if( this.props.header === '#stars') {
            return (
                <RenderList fixedNavLink="movies">
                    <MoviePop />
                </RenderList>);
        }
    }
    popularFilter = (items, search, stream) => {
        //console.log('pop')
        const uuidArry = [], chipsUuid = [], genreFacet = [], certificate = [], slider = [];
        let streamOnArray=null;

        this.setState({
            item: items.length + search.length
        });

        search.map(chip => 
            index.search({
              query: chip
              },
              (err, { hits } = {}) => {
                if (err) throw err;   
                chipsUuid.push(hits[0].uuid); 
              }
            )
        );
        const itemsComp = () => {
            items.map ( item => {
                if(item.items && (item.attribute === "genre")){
                    (item.currentRefinement.map ( each => genreFacet.push(`genre:${each}`)))
                }
                else if(item.items && (item.attribute === "certificate")){
                    (item.currentRefinement.map ( each => certificate.push(`certificate:${each}`)))
                }
                else if(item.attribute === "year"){
                    (slider.push(`year > ${item.currentRefinement.min}` , `year < ${item.currentRefinement.max}`))
                }
                return true;
            })
        }
        const streamCheck = () => {
            if(stream === "YOUTUBE"){
                streamOnArray = ["full_movie_link.uuid: "];
            }
            else if(stream === "HOTSTAR"){
                streamOnArray = ["external_source_fullmovie:Hotstar"];
            }
        }
        if(search.length && items.length && ( stream !== "STREAMING ON")){
            itemsComp();
            streamCheck();
            mvIndex.search("", {
                "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
                "facetFilters": genreFacet.concat(certificate, streamOnArray),
                "numericFilters": slider
            },
            (err, { hits } = {}) => {
                if (err) throw err;  
                hits.map(hit => uuidArry.push(hit.uuid));
            });
            setTimeout(() => {  
                if(chipsUuid.length && uuidArry.length){       
                this.props.movieSearchFilter(`/movies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAllStream: res.data.data,
                    pageNum: res.data.meta.pagination.total_pages}))
                }
            }, 500) 
        }
        else if(search.length && items.length){           
            itemsComp();
            mvIndex.search("", {
                "facets": "*,year,genre,certificate",
                "facetFilters": genreFacet.concat(certificate),
                "numericFilters": slider
            },
            (err, { hits } = {}) => {
                if (err) throw err;  
                hits.map(hit => uuidArry.push(hit.uuid));
            });
    
            setTimeout(() => {
                if(chipsUuid.length && uuidArry.length){ 
                this.props.movieSearchFilter(`/movies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAllStream: res.data.data,
                    pageNum: res.data.meta.pagination.total_pages}))
                }
            }, 500)              
        }

        else if(search.length && (stream !== "STREAMING ON")){  
            mvIndex.search("", {
                "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
                "facetFilters": genreFacet.concat(certificate, streamOnArray)
            },
            (err, { hits } = {}) => {
                if (err) throw err;  
                hits.map(hit => uuidArry.push(hit.uuid));
            });
    
            setTimeout(() => {
                if(chipsUuid.length && uuidArry.length){ 
                this.props.movieSearchFilter(`/movies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAllStream: res.data.data,
                    pageNum: res.data.meta.pagination.total_pages}))
                }
            }, 500)              
        }

        else if((stream !== "STREAMING ON") && items.length){           
            itemsComp();
            mvIndex.search("", {
                "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
                "facetFilters": genreFacet.concat(certificate, streamOnArray),
                "numericFilters": slider
            },
            (err, { hits } = {}) => {
                if (err) throw err;  
                hits.map(hit => uuidArry.push(hit.uuid));
            });
    
            setTimeout(() => {
                if(chipsUuid.length && uuidArry.length){ 
                this.props.movieSearchFilter(`/movies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAllStream: res.data.data,
                    pageNum: res.data.meta.pagination.total_pages}))
                }
            }, 500)              
        }
    
        else if(search.length){
            setTimeout(() => {
                if(chipsUuid.length){ 
                this.props.movieSearchFilter(`/movies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=`)
                .then(res => this.setState({ movieAllStream: res.data.data,
                    pageNum: res.data.meta.pagination.total_pages}))}
            }, 500)            
        }
        else if(items.length){   
            itemsComp();

            mvIndex.search({
                "hitsPerPage": 18,
                "page": 0,
                "facets": "*,year,genre,certificate",
                "facetFilters": genreFacet.concat(certificate),
                "numericFilters": slider
                },
                (err, { hits } = {}) => {
                    if (err) throw err; 
                    hits.map(hit => uuidArry.push(hit.uuid));
                });
        
            setTimeout(() => { 
                this.props.movieSearchFilter(`/movies?include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAllStream: res.data.data,
                    pageNum: res.data.meta.pagination.total_pages}))
            }, 500) 
        }
        else if(( stream !== "STREAMING ON")){
            streamCheck();
            mvIndex.search("", {
                "facets": "*,year,genre,certificate, full_movie_link.uuid,external_source_fullmovie",
                "facetFilters": streamOnArray
            },
            (err, { hits } = {}) => {
                if (err) throw err; 
                //console.log(hits)
               hits.map(hit => uuidArry.push(hit.uuid));
            });
            setTimeout(() => {    
                this.props.movieSearchFilter(`/movies?include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAllStream: res.data.data,
                    pageNum: res.data.meta.pagination.total_pages}))
            }, 500) 
        }
        else{
            this.props.movieAllStream('1')
            .then(res => this.setState({
                movieAllStream: res.data.data,
                pageNum: res.data.meta.pagination.total_pages
            }));
        }
    }
    soonFilter = (items, search) => {
        ///console.log('soon')
            const uuidArry = [], chipsUuid = [], genreFacet = [], certificate = [];
    
            this.setState({
                item: items.length + search.length
            });
    
            search.map(chip => 
                index.search({
                  query: chip
                  },
                  (err, { hits } = {}) => {
                    if (err) throw err;   
                    chipsUuid.push(hits[0].uuid); 
                  }
                )
            );
            const itemsComp = () => {
                items.map ( item => {
                    if(item.items && (item.attribute === "genre")){
                        (item.currentRefinement.map ( each => genreFacet.push(`genre:${each}`)))
                    }
                    else if(item.items && (item.attribute === "certificate")){
                        (item.currentRefinement.map ( each => certificate.push(`certificate:${each}`)))
                    }
                    return true;
                })
            }
            if(search.length && items.length){           
                itemsComp();
                mvIndex.search("", {
                    "facets": "*,year,genre,certificate",
                    "facetFilters": genreFacet.concat(certificate)
                },
                (err, { hits } = {}) => {
                    if (err) throw err;  
                    hits.map(hit => uuidArry.push(hit.uuid));
                });
        
                setTimeout(() => {                    
                    this.props.movieSearchFilter(`/movies/category/releases?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                    .then(res => this.setState({ movieReleases: res.data.data}))
                }, 500)              
            }   
        
            else if(search.length){
                setTimeout(() => {
                    this.props.movieSearchFilter(`/movies/category/releases?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=`)
                    .then(res => this.setState({ movieReleases: res.data.data}))
                }, 500)            
            }
            else if(items.length){   
                itemsComp();    
                mvIndex.search({
                    "facets": "*,year,genre,certificate",
                    "facetFilters": genreFacet.concat(certificate)
                    },
                    (err, { hits } = {}) => {
                        if (err) throw err; 
                        hits.map(hit => uuidArry.push(hit.uuid));
                    });
            
                setTimeout(() => {
                    this.props.movieSearchFilter(`/movies/category/releases?include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                    .then(res => this.setState({ movieReleases: res.data.data}))
                }, 500) 
            }
            else{
                this.releases()
            }
        
    }
    awardFilter = (items, search, stream) => {
       // console.log('awa')
        const uuidArry = [], chipsUuid = [], genreFacet = [], certificate = [], slider = [];
        let streamOnArray=null;

        this.setState({
            item: items.length + search.length
        });

        search.map(chip => 
            index.search({
              query: chip
              },
              (err, { hits } = {}) => {
                if (err) throw err;   
                chipsUuid.push(hits[0].uuid); 
              }
            )
        );
        const itemsComp = () => {
            items.map ( item => {
                if(item.items && (item.attribute === "genre")){
                    (item.currentRefinement.map ( each => genreFacet.push(`genre:${each}`)))
                }
                else if(item.items && (item.attribute === "certificate")){
                    (item.currentRefinement.map ( each => certificate.push(`certificate:${each}`)))
                }
                else if(item.attribute === "year"){
                    (slider.push(`year > ${item.currentRefinement.min}` , `year < ${item.currentRefinement.max}`))
                }
                return true;
            })
        }
        const streamCheck = () => {
            if(stream === "YOUTUBE"){
                streamOnArray = ["full_movie_link.uuid: "];
            }
            else if(stream === "HOTSTAR"){
                streamOnArray = ["external_source_fullmovie:Hotstar"];
            }
        }
        if(search.length && items.length && ( stream !== "STREAMING ON")){
            itemsComp();
            streamCheck();
            mvIndex.search("", {
                "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
                "facetFilters": genreFacet.concat(certificate, streamOnArray),
                "numericFilters": slider
            },
            (err, { hits } = {}) => {
                if (err) throw err;  
                hits.map(hit => uuidArry.push(hit.uuid));
            });
            setTimeout(() => {    
                           
                this.props.movieAwardedFilter(`/movies/id/items?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAward: res.data.data}))
            }, 500) 
        }
        else if(search.length && items.length){           
            itemsComp();
            mvIndex.search("", {
                "facets": "*,year,genre,certificate",
                "facetFilters": genreFacet.concat(certificate),
                "numericFilters": slider
            },
            (err, { hits } = {}) => {
                if (err) throw err;  
                hits.map(hit => uuidArry.push(hit.uuid));
            });
    
            setTimeout(() => {
                this.props.movieAwardedFilter(`/movies/id/items?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAward: res.data.data}))
            }, 500)              
        }

        else if(search.length && (stream !== "STREAMING ON")){  
            mvIndex.search("", {
                "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
                "facetFilters": genreFacet.concat(certificate, streamOnArray)
            },
            (err, { hits } = {}) => {
                if (err) throw err;  
                hits.map(hit => uuidArry.push(hit.uuid));
            });
    
            setTimeout(() => {
                this.props.movieAwardedFilter(`/movies/id/items?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAward: res.data.data}))
            }, 500)              
        }

        else if((stream !== "STREAMING ON") && items.length){           
            itemsComp();
            mvIndex.search("", {
                "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
                "facetFilters": genreFacet.concat(certificate, streamOnArray),
                "numericFilters": slider
            },
            (err, { hits } = {}) => {
                if (err) throw err;  
                hits.map(hit => uuidArry.push(hit.uuid));
            });
    
            setTimeout(() => {
                this.props.movieAwardedFilter(`/movies/id/items?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAward: res.data.data}))
            }, 500)              
        }
    
        else if(search.length){
            setTimeout(() => {
                this.props.movieAwardedFilter(`/movies/id/items?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=`)
                .then(res => this.setState({ movieAward: res.data.data}))
            }, 500)            
        }
        else if(items.length){   
            itemsComp();

            mvIndex.search({
                "hitsPerPage": 18,
                "page": 0,
                "facets": "*,year,genre,certificate",
                "facetFilters": genreFacet.concat(certificate),
                "numericFilters": slider
                },
                (err, { hits } = {}) => {
                    if (err) throw err; 
                    hits.map(hit => uuidArry.push(hit.uuid));
                });
        
            setTimeout(() => {
                this.props.movieAwardedFilter(`/movies/id/items?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAward: res.data.data}))
            }, 500) 
        }
        else if(( stream !== "STREAMING ON")){
            streamCheck();
            mvIndex.search("", {
                "facets": "*,year,genre,certificate, full_movie_link.uuid,external_source_fullmovie",
                "facetFilters": streamOnArray
            },
            (err, { hits } = {}) => {
                if (err) throw err; 
               // console.log(hits)
               hits.map(hit => uuidArry.push(hit.uuid));
            });
            setTimeout(() => {
                this.props.movieAwardedFilter(`/movies/id/items?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieAward: res.data.data}))
            }, 500) 
        }
        else{
            this.awarded()
        }
    }
    fullFilter = (items, search, stream) => {
       // console.log('full')
       //console.log('pop')
       const uuidArry = [], chipsUuid = [], genreFacet = [], certificate = [], slider = [];
       let streamOnArray=null;

       this.setState({
           item: items.length + search.length
       });

       search.map(chip => 
           index.search({
             query: chip
             },
             (err, { hits } = {}) => {
               if (err) throw err;   
               chipsUuid.push(hits[0].uuid); 
             }
           )
       );
       const itemsComp = () => {
           items.map ( item => {
               if(item.items && (item.attribute === "genre")){
                   (item.currentRefinement.map ( each => genreFacet.push(`genre:${each}`)))
               }
               else if(item.items && (item.attribute === "certificate")){
                   (item.currentRefinement.map ( each => certificate.push(`certificate:${each}`)))
               }
               else if(item.attribute === "year"){
                   (slider.push(`year > ${item.currentRefinement.min}` , `year < ${item.currentRefinement.max}`))
               }
               return true;
           })
           return true;
       }
       const streamCheck = () => {
           if(stream === "YOUTUBE"){
               streamOnArray = ["full_movie_link.uuid: "];
           }
           else if(stream === "HOTSTAR"){
               streamOnArray = ["external_source_fullmovie:Hotstar"];
           }
       }
       if(search.length && items.length && ( stream !== "STREAMING ON")){
           //console.log('one')
           itemsComp();
           streamCheck();
           mvIndex.search("", {
               "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
               "facetFilters": genreFacet.concat(certificate, streamOnArray),
               "numericFilters": slider
           },
           (err, { hits } = {}) => {
               if (err) throw err;  
               hits.map(hit => uuidArry.push(hit.uuid));
           });
           setTimeout(() => {
            if(chipsUuid.length && uuidArry.length){         
               this.props.movieSearchFilter(`/movies/category/fullmovies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
               .then(res => this.setState({ movieFullmovies: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
           }, 500) 
       }
       else if(search.length && items.length){           
           //console.log('two')
           itemsComp();
           mvIndex.search("", {
               "facets": "*,year,genre,certificate",
               "facetFilters": genreFacet.concat(certificate),
               "numericFilters": slider
           },
           (err, { hits } = {}) => {
               if (err) throw err;  
               hits.map(hit => uuidArry.push(hit.uuid));
           });
   
           setTimeout(() => {
               if(chipsUuid.length){
                this.props.movieSearchFilter(`/movies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
                .then(res => this.setState({ movieFullmovies: res.data.data,
                    pageNum: res.data.meta.pagination.total_pages}))
               }
           }, 500)              
       }

       else if(search.length && (stream !== "STREAMING ON")){  
           //console.log('three')
           mvIndex.search("", {
               "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
               "facetFilters": genreFacet.concat(certificate, streamOnArray)
           },
           (err, { hits } = {}) => {
               if (err) throw err;  
               hits.map(hit => uuidArry.push(hit.uuid));
           });
   
           setTimeout(() => {
            if(chipsUuid.length){
               this.props.movieSearchFilter(`/movies/category/fullmovies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
               .then(res => this.setState({ movieFullmovies: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
           }, 500)              
       }

       else if((stream !== "STREAMING ON") && items.length){           
           //console.log('four')
           itemsComp();
           mvIndex.search("", {
               "facets": "*,year,genre,certificate,full_movie_link.uuid,external_source_fullmovie",
               "facetFilters": genreFacet.concat(certificate, streamOnArray),
               "numericFilters": slider
           },
           (err, { hits } = {}) => {
               if (err) throw err;  
               hits.map(hit => uuidArry.push(hit.uuid));
           });
   
           setTimeout(() => {
            if(chipsUuid.length && uuidArry.length){ 
               this.props.movieSearchFilter(`/movies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=${uuidArry.join()}`)
               .then(res => this.setState({ movieFullmovies: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
           }, 500)              
       }
   
       else if(search.length){
        //console.log('five')
           setTimeout(() => {
               if(chipsUuid.length){
                this.props.movieSearchFilter(`/movies/category/fullmovies?cc=${chipsUuid.join()}&include=wallpapers,professions&azSearch=`)
                .then(res => this.setState({ movieFullmovies: res.data.data,
                    pageNum: res.data.meta.pagination.total_pages}))     
                .catch(err => console.warn(err))              
               }
           }, 500)            
       }
       else if(items.length){   
          // console.log('six')
           itemsComp();

           mvIndex.search({
               "hitsPerPage": 18,
               "page": 0,
               "facets": "*,year,genre,certificate",
               "facetFilters": genreFacet.concat(certificate),
               "numericFilters": slider
               },
               (err, { hits } = {}) => {
                   if (err) throw err; 
                   hits.map(hit => uuidArry.push(hit.uuid));
               });
       
           setTimeout(() => { 
            if(uuidArry.length){
               this.props.movieSearchFilter(`/movies/category/fullmovies?include=wallpapers,professions&azSearch=${uuidArry.join()}`)
               .then(res => this.setState({ movieFullmovies: res.data.data,
                pageNum: res.data.meta.pagination.total_pages}))
            }
           }, 500) 
       }
       else if(( stream !== "STREAMING ON")){
          // console.log('seven')
           streamCheck();
           mvIndex.search("", {
               "facets": "*,year,genre,certificate, full_movie_link.uuid,external_source_fullmovie",
               "facetFilters": streamOnArray
           },
           (err, { hits } = {}) => {
               if (err) throw err; 
               //console.log(hits)
              hits.map(hit => uuidArry.push(hit.uuid));
           });
           setTimeout(() => {    
            if(uuidArry.length){
               this.props.movieSearchFilter(`/movies/category/fullmovies?include=wallpapers,professions&azSearch=${uuidArry.join()}`)
               .then(res => this.setState({ movieFullmovies: res.data.data}))
            }
           }, 500) 
       }
       else{
           //console.log('fullmovies')
           this.fullmovies();
       }
    }

    contentChange = (items, search, stream) => {
        //console.log('cont')
        if(this.props.header === '#popular'){
            this.popularFilter(items, search, stream);
        }
        else if( this.props.header === '#soon'){
            this.soonFilter(items, search)
        }
        else if( this.props.header === '#awarded'){
            this.awardFilter(items, search, stream)
        }
        else if( this.props.header === '#full'){
            this.fullFilter(items, search, stream)
        }
        
    }

    selectedUuidUpdate = (responseHits) => {
        this.setState({ selectedUuid: responseHits });
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    render() {    
        return (        
            <div className="flex-column d-inline-flex" style={style}>

                <Header activeLink='movies'/>

                {/* { this.props.filterState ? <MoviesFilter  checkItemsLength={this.contentChange} update={this.selectedUuidUpdate} filterString={this.filterString}/> : null }  */}
                { this.props.filterState ? <MoviesFilter  checkItemsLength={this.contentChange} update={this.selectedUuidUpdate}/> : null }
                {/* <MoviesFilter checkItemsLength={this.contentChange} update={this.selectedUuidUpdate}/>  */}

                {/* { this.props.filterState ? <Filter checkItemsLength={this.contentChange} filterCallback={this.dummyDataDetailUpdate}/> : null }                  */}
                {/* <Filter checkItemsLength={this.contentChange} filterCallback={this.dummyDataDetailUpdate}/> */}
                {/* <MoviesFilter  checkItemsLength={this.contentChange} update={this.selectedUuidUpdate}/>  */}
                <div className="image-cont container-fluid text-light">
                    {/* {(this.state.item || this.state.selectedUuid) ? this.filterSortList() : this.sortList() } */}
                    {/* {(activeHeader !== this.props.header) ? this.sortList() : null } */}
                    {/* {this.state.dummyDataDetail ? this.filterDummySortList() : this.sortList() } */}
                    {this.sortList()}
                </div>

            </div>
        )
    }
      
}



const mapStateToProps = state => {
    return{
        items: state.items,
        width: state.width,
        header: state.selectedHeader,
        filterState: state.selectedFilter,

       // movieAll: state.movieAll,
       // movieRel: state.movieRel,
       // movieAward: state.movieAward,
        //movieFull: state.movieFull
    }
}

Movie.propTypes = {
    windowWidth: PropTypes.func.isRequired,
    NumberOfItems: PropTypes.func.isRequired,
    selectedImgLists: PropTypes.func.isRequired,
    movieAllStream: PropTypes.func.isRequired,
    movieRelStream: PropTypes.func.isRequired,
    movieFullStream: PropTypes.func.isRequired,
    movieAwardStream: PropTypes.func.isRequired,

    items: PropTypes.number,
    width: PropTypes.number,
    header: PropTypes.string.isRequired,
    filterState: PropTypes.bool.isRequired,

    //movieAll: PropTypes.object,
    //movieRel: PropTypes.object,
    //movieAward: PropTypes.object,
   // movieFull: PropTypes.object
}

export default connect(mapStateToProps, { windowWidth, NumberOfItems, movieAwardedFilter, movieSearchFilter, movieAwardPost, fetchStreams, selectedImgLists, movieAllStream, movieRelStream, movieFullStream, movieAwardStream })(Movie);