import React from 'react';
import './content.scss';
import Header from '../header';
import { windowWidth,awardAllStream, NumberOfItems, awardIndividual, selectHeader } from '../../actions';
import { connect } from 'react-redux';
import {Button} from 'react-bootstrap';
//import PopoverStickOnHover from '../utils/ImageContprop';
import ImageFilter from 'react-image-filter';
//import filmfare from '../../img/filmfare.jpg';
import nandi from '../../img/nandi.jpg';
//import siima from '../../img/siima.jpg';
import AwardsRender from '../utils/AwardsRender';
import PropTypes from 'prop-types';
import Slider from "react-slick";
import MoviesFilter from '../filters/MoviesFilter';

const style = {
    width: '92%',
    marginLeft: '7%'
}

function SliderNextArrow(props) {
    const { className, style, onClick } = props;
    if(onClick == null){
        return(null);
    }
    return (
        <div className="award-next-btn" onClick={onClick}></div>
    );
  }

  function SliderPrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className="award-prev-btn" onClick={onClick}></div>
    );
  }
  
class Awards extends React.Component {
    state = {
        awardsStream: null,
        awardImg: null,
        filmfareStream: [],
        nandiStream:[],
        siimaStream: [],
        iifaStream: [],
        natStream: [],
        selectedUuid: null,
        activeItem: -1,
    }

    componentDidMount(){
        this.props.awardAllStream()
        .then(res => {
            this.setState({ awardsStream: res.data.data})
        })
        this.props.windowWidth(window.innerWidth);      
        this.numOfItemInARow(window.innerWidth);
        window.addEventListener('resize', this.updateWindowDimensions);  
    }
    updateWindowDimensions = () => {
        this.props.windowWidth(window.innerWidth);        
        this.numOfItemInARow(this.props.width);
    }

    numOfItemInARow = (width) => {

        if( width <= 575.98 ) {
            this.props.NumberOfItems(1);
            return;
        } else if( (576 <= width) && (width <= 767.98) ) {
            this.props.NumberOfItems(2);            
            return;

        } else if( (768 <= width) && (width <= 991.98) ) {
            this.props.NumberOfItems(3);            
            return;

        } else if( (992 <= width) && (width <= 1199.98) ) {
            this.props.NumberOfItems(4);            
            return;

        } else if( (1200 <= width) ){
            this.props.NumberOfItems(4);            
            return;            
        }
    }

    onMouseEnter = (item) => {
        let id = item.currentTarget.id;

       console.log(document.getElementById(id).childNodes[0].childNodes[1].childNodes[0]);
        let childelement;
       
        childelement = document.getElementById(id).childNodes[0].childNodes[1].childNodes[0];
        childelement.classList.add("custom-visible");


    }
    onMouseLeave = (item) => {
        let id = item.currentTarget.id;
        let childelement;
 
        childelement = document.getElementById(id).childNodes[0].childNodes[1].childNodes[0];
        childelement.classList.remove('custom-visible');
    }
    

    unorderedList = (lists, img, name) => {
        let ulId = name.replace(/\s/g, "_");
        var settings = {
            dots: false,
            infinite: false,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            initialSlide: 0,
            nextArrow: <SliderNextArrow />,
            prevArrow: <SliderPrevArrow />,
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  initialSlide: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
          };

        return(
            // <ul 
            // className='list-inline award-ulclass' 
            // id={ulId} 
            // onMouseEnter={ e => {
            //     e.preventDefault();
            //     let id = e.currentTarget.getAttribute('id');
            //     document.getElementById(id).classList.add("showBtns");
            // }}
            // onMouseLeave={ e => {
            //     e.preventDefault();
            //     let id = e.currentTarget.getAttribute('id');
            //     document.getElementById(id).classList.remove("showBtns");
            // }}
            
            // >
            //     {lists.map( imgdetail => {
            //         let year = imgdetail.date.split('/');
            //         return (
            //             <li className="list-inline-item award-active" ref="listItem" key={imgdetail.uuid} id={imgdetail.uuid} >
            //                 <div className="only-image">
            //                     <div className="movie-title">
            //                         {imgdetail.series ? <p>{imgdetail.series}<sup>th</sup></p> : null}
            //                         <h5 className="text-uppercase mb-0">{imgdetail.name}</h5>
            //                         <h2>{year[2]}</h2>
            //                     </div>
            //                     <ImageFilter
            //                         image={img ? img : nandi}
            //                         filter={ 
            //                             [0, 0.9, 0, 0, 0,
            //                             0, 0.9, 0, 0, 0,
            //                             0, 0.9, 0, 0, 0,
            //                             0, 0, 0, 0.2, 0,] 
            //                         }
            //                     />
            //                 </div>
            //             </li>
            //         )})
            //     }
            // </ul>
           
                    <Slider {...settings}
                    id={ulId} 
                    >
                    {lists.map( imgdetail => {
                            let year = imgdetail.date.split('/');
                            return (
                                <li className="list-inline-item award-active" ref="listItem"
                                key={imgdetail.uuid} id={imgdetail.uuid} 
                                onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave} 
                                >
                                    <div className="only-image position-relative">
                                        <div className="movie-title custom-awards-movie-title">
                                            {imgdetail.series ? <p>{imgdetail.series}<sup>th</sup></p> : null}
                                            <h5 className="text-uppercase mb-0">{imgdetail.name}</h5>
                                            <h2>{year[2]}</h2>
                                        </div>
                                        <ImageFilter
                                            image={img ? img : nandi}
                                            filter={ 
                                                [0, 0.9, 0, 0, 0,
                                                0, 0.9, 0, 0, 0,
                                                0, 0.9, 0, 0, 0,
                                                0, 0, 0, 0.2, 0,] 
                                            }
                                        />
                                    </div>
                                </li>
                            )})
                        }
                           <li className="list-inline-item award-active" ref="listItem" 
                           onClick={(e) => this.clickHandler(e, name)} id={name}
                           onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave} 
                           >
                                    <div className="only-image position-relative">
                                        <div className="movie-title custom-awards-movie-title-view-all">
                                            <h5 className="text-uppercase mb-0">VIEW ALL</h5>
                                            <h2></h2>
                                        </div>
                                        <ImageFilter id={name}
                                            image={img ? img : nandi}
                                            filter={
                                                [0, 0.9, 0, 0, 0,
                                                0, 0.9, 0, 0, 0,
                                                0, 0.9, 0, 0, 0,
                                                0, 0, 0, 0.2, 0,] 
                                            }
                                        />
                                    </div>
                                </li>
                    </Slider>
         

        )
    }
    clickHandler = (e, award) => {
        e.preventDefault();
        console.log(award)
        if(award.name === "Filmfare" || award === "Filmfare"){
            this.props.selectHeader("#filmfare");
        }
        else if(award.name === "SIIMA" || award === "SIIMA"){
            this.props.selectHeader("#siima");
        }
        else if(award.name === "Nandi" || award === "Nandi"){
            this.props.selectHeader("#nandi");
        }
        else if(award.name === "IIFA Utsavam" || award === "IIFA Utsavam"){
            this.props.selectHeader("#iifa");
        }
        else if(award.name === "National Film Awards" || award === "National Film Awards"){
            this.props.selectHeader("#national");
        }
    }
    sortList (awardsStream) {
        if( this.props.header === '#popular'){

                return (
                    <>
                        { awardsStream ? awardsStream.map( award => 
                            award.awards.data.length ? (
                            <div className="award-class mb-3" key={award.uuid}>
                                <div className="row">
                                    <h5 className="mb-3 text-uppercase col-10">{award.name}</h5>    
                                    <div className="col-2 text-right">
                                        <Button 
                                            className="p-0" 
                                            variant="outline-info" 
                                            onClick={(e) => this.clickHandler(e, award)}
                                        >
                                            VIEW ALL
                                        </Button>
                                    </div>
                                </div>

                                {this.unorderedList(award.awards.data, award.listing.data.uri, award.name)}

                                {/* <li className="list-inline-item award-active" ref="listItem" >
                                    <div className="only-image">
                                        <div className="movie-title">
                                            {imgdetail.series ? <p>{imgdetail.series}<sup>th</sup></p> : null}
                                            <h5 className="text-uppercase mb-0">VIEW ALL</h5>
                                            <h2></h2>
                                        </div>
                                        <ImageFilter
                                           
                                            filter={
                                                [0, 0.9, 0, 0, 0,
                                                0, 0.9, 0, 0, 0,
                                                0, 0.9, 0, 0, 0,
                                                0, 0, 0, 0.2, 0,] 
                                            }
                                        />
                                    </div>
                                </li> */}

                                {/* <div className="award-prev-btn"></div>
                                <div className="award-next-btn"></div> */}

                               
                            </div>) : null
                           
                        ) : null }

                    </>
                )
        }
        else if( this.props.header === '#filmfare'){
            let imgLists, 
            img = this.state.awardsStream
            .filter( (obj) => (obj.name === "Filmfare") ? obj : null);

            if(!this.state.filmfareStream.length){
                imgLists = this.state.awardsStream
                            .filter( (obj) =>(obj.name === "Filmfare") ? obj.uuid : null);

                
                this.props.awardIndividual(imgLists[0].uuid)
                .then(res => {
                    this.setState({ filmfareStream: res.data.data})
                })
            }
            
            return (
                <AwardsRender imgLists={this.state.filmfareStream} img={img[0].listing.data.uri} class="filmFareUl"/>
            )
        }
        else if( this.props.header === '#nandi'){
            let imgLists, 
            img = this.state.awardsStream
            .filter( (obj) => (obj.name === "Nandi") ? obj : null);
            
            if(!this.state.nandiStream.length){
                imgLists = this.state.awardsStream
                            .filter( (obj) =>(obj.name === "Nandi") ? obj.uuid : null);

                
                this.props.awardIndividual(imgLists[0].uuid)
                .then(res => {
                    this.setState({ nandiStream: res.data.data})
                })
            }
            
            return (
                <AwardsRender imgLists={this.state.nandiStream} img={img[0].listing.data.uri} class="nandiUl"/>
            )
        }
        else if( this.props.header === '#siima'){
            let imgLists, 
            img = this.state.awardsStream
            .filter( (obj) => (obj.name === "SIIMA") ? obj : null);
            
            if(!this.state.siimaStream.length){
                imgLists = this.state.awardsStream
                            .filter( (obj) =>(obj.name === "SIIMA") ? obj.uuid : null);

                
                this.props.awardIndividual(imgLists[0].uuid)
                .then(res => {
                    this.setState({ siimaStream: res.data.data})
                })
            }
            
            return (
                <AwardsRender imgLists={this.state.siimaStream} img={img[0].listing.data.uri} class="siimaUl"/>
            )
        }
        else if( this.props.header === '#iifa'){
            let imgLists, 
            img = this.state.awardsStream
            .filter( (obj) => (obj.name === "IIFA Utsavam") ? obj : null);
            
            if(!this.state.iifaStream.length){
                imgLists = this.state.awardsStream
                            .filter( (obj) =>(obj.name === "IIFA Utsavam") ? obj.uuid : null);

                
                this.props.awardIndividual(imgLists[0].uuid)
                .then(res => {
                    this.setState({ iifaStream: res.data.data})
                })
            }
            
            return (
                <AwardsRender imgLists={this.state.iifaStream} img={img[0].listing.data.uri} class="iifaUl"/>
            )
        }
        else if( this.props.header === '#national'){
            let imgLists, 
            img = this.state.awardsStream
            .filter( (obj) => (obj.name === "National Film Awards") ? obj : null);
            
            if(!this.state.natStream.length){
                imgLists = this.state.awardsStream
                            .filter( (obj) =>(obj.name === "National Film Awards") ? obj.uuid : null);

                
                this.props.awardIndividual(imgLists[0].uuid)
                .then(res => {
                    this.setState({ natStream: res.data.data})
                })
            }
            
            return (
                <AwardsRender imgLists={this.state.natStream} img={img[0].listing.data.uri} class="NatUl"/>
            )
        }
    }

    popularFilter = (items, search, stream) => { 

    }

    contentChange = (items, search, stream) => {
        //console.log('cont')
        if(this.props.header === '#popular'){
            this.popularFilter(items, search, stream);
        }
        else if( this.props.header === '#soon'){
            this.soonFilter(items, search)
        }
        else if( this.props.header === '#awarded'){
            this.awardFilter(items, search, stream)
        }
        else if( this.props.header === '#full'){
            this.fullFilter(items, search, stream)
        }
        
    }
    selectedUuidUpdate = (responseHits) => {
        this.setState({ selectedUuid: responseHits });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    render() {
        let { awardsStream } = this.state;
        var settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            initialSlide: 0,
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  initialSlide: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
          };
        return (
            <div className="flex-column d-inline-flex" style={style}>
                <Header activeLink='awards' />
              
                {/* { this.props.filterState ? <MoviesFilter  checkItemsLength={this.contentChange} update={this.selectedUuidUpdate}/> : null } */}
                <div className="image-cont container-fluid text-light">
                    {this.sortList(awardsStream)}
                </div>
            </div>
        )
    }
}



const mapStateToProps = state => {
    return{
        items: state.items,
        width: state.width,
        header: state.selectedHeader,
        awardsImg: state.awardsImg,
        filterState: state.selectedFilter,

    }
}

Awards.propTypes = {
    windowWidth: PropTypes.func.isRequired,

    items: PropTypes.number,
    width: PropTypes.number,
    header: PropTypes.string.isRequired,
    filterState: PropTypes.bool.isRequired,

    //movieAll: PropTypes.object,
    //movieRel: PropTypes.object,
    //movieAward: PropTypes.object,
   // movieFull: PropTypes.object
}

export default connect(mapStateToProps, { windowWidth, awardAllStream, selectHeader, NumberOfItems, awardIndividual })(Awards);

 // sortingFunc = (name) => {
    //     return (
    //         this.props.awardsImg
    //         .filter((imgDetail) => {
    //             return (imgDetail.name === name);
    //         })
    //         .sort((a, b) => {
    //             return a.year - b.year;
    //         })
    //         .reverse()
    //     )
    // }

    
                // const filmfareLists = this.sortingFunc("filmfare awards");

                // const nandiLists = this.sortingFunc("nandi awards");

                // const siimaLists = this.sortingFunc("south Indian international movie awards");

                // const iifaLists = this.sortingFunc("iifa");

                // const nationalLists = this.sortingFunc("national film awards");

                        
/*
                        <div className="award-class mb-3">
                            <h5 className="mb-3 text-uppercase">Nandi Awards</h5>
                            <div className="award-prev-btn"></div>

                                {this.unorderedList(nandiLists, nandi)}

                            <div className="award-next-btn"></div>
                        </div>


                        <div className="award-class mb-3">
                            <h5 className="mb-3 text-uppercase">SIIMA (South Indian Iintenational Movie Awards)</h5>
                            <div className="award-prev-btn"></div>

                                {this.unorderedList(siimaLists, siima)}

                             <div className="award-next-btn"></div>
                        </div>


                        <div className="award-class mb-3">
                            <h5 className="mb-3 text-uppercase">IIFA (International Indian Film Academy Awards)</h5>
                            <div className="award-prev-btn"></div>

                                {this.unorderedList(iifaLists, nandi)}

                            <div className="award-next-btn"></div>
                        </div>

                        <div className="award-class mb-3">
                            <h5 className="mb-3 text-uppercase">national film Awards</h5>
                            <div className="award-prev-btn"></div>

                                {this.unorderedList(nationalLists, nandi)}

                            <div className="award-next-btn"></div>
                        </div>
                     */


    //  sortingFunc = (name) => {
    //     return (
    //         this.props.awardsImg
    //         .filter((imgDetail) => {
    //             return (imgDetail.name === name);
    //         })
    //         .sort((a, b) => {
    //             return a.year - b.year;
    //         })
    //         .reverse()
    //     )
    //  }

    //  lists = (lists, img) => {
    //      return (lists.map( imgdetail => {
    //         return (<li className="list-inline-item award-active" ref="listItem" key={imgdetail.uuid} id={imgdetail.uuid} >
    //             <div className="only-image">
    //             <div className="movie-title">
    //                 {imgdetail.series ? <p>{imgdetail.series}<sup>th</sup></p> : null}
    //                 <h5 className="text-uppercase">{imgdetail.name}</h5>
    //                 <h2>{imgdetail.year}</h2>
    //                 </div>
    //                 <ImageFilter
    //                     image={img}
    //                     filter={ [0, 0.9, 0, 0, 0,
    //                         0, 0.9, 0, 0, 0,
    //                         0, 0.9, 0, 0, 0,
    //                         0, 0, 0, 0.2, 0,] }
    //                 />
    //             </div>
    //     </li>)
    //     }))
    //  }

    // sortList () {
    //     if( this.props.header === '#popular'){
    //         const filmfareLists = this.sortingFunc("filmfare awards");

    //         const nandiLists = this.sortingFunc("nandi awards");

    //         const siimaLists = this.sortingFunc("south Indian international movie awards");

    //         const iifaLists = this.sortingFunc("iifa");

    //         const nationalLists = this.sortingFunc("national film awards");

    //         return (
    //             <>
    //                 <ul className='list-inline award-ulclass'>
    //                     {/* {filmfareLists.map( imgdetail => {
    //                         return (
    //                             <li className="list-inline-item award-active" ref="listItem" key={imgdetail.uuid} id={imgdetail.uuid} >
    //                                 <div className="only-image">
    //                                     <div className="movie-title">
    //                                         {imgdetail.series ? <p>{imgdetail.series}<sup>th</sup></p> : null}
    //                                         <h5 className="text-uppercase">{imgdetail.name}</h5>
    //                                         <h2>{imgdetail.year}</h2>
    //                                     </div>
    //                                     <ImageFilter
    //                                         image={filmfare}
    //                                         filter={ [0, 0.9, 0, 0, 0,
    //                                             0, 0.9, 0, 0, 0,
    //                                             0, 0.9, 0, 0, 0,
    //                                             0, 0, 0, 0.2, 0,] }
    //                                     />
    //                                 </div>
    //                             </li>
    //                         )})
    //                     } */}
    //                 </ul>

    //                 <ul className='list-inline award-ulclass'>
    //                 {/* {nandiLists.map( imgdetail => {
    //                         return (
    //                             <li className="list-inline-item award-active" ref="listItem" key={imgdetail.uuid} id={imgdetail.uuid} >
    //                                 <div className="only-image">
    //                                     <div className="movie-title">
    //                                         {imgdetail.series ? <p>{imgdetail.series}<sup>th</sup></p> : null}
    //                                         <h5 className="text-uppercase">{imgdetail.name}</h5>
    //                                         <h2>{imgdetail.year}</h2>
    //                                     </div>
    //                                     <ImageFilter
    //                                         image={filmfare}
    //                                         filter={ [0, 0.9, 0, 0, 0,
    //                                             0, 0.9, 0, 0, 0,
    //                                             0, 0.9, 0, 0, 0,
    //                                             0, 0, 0, 0.2, 0,] }
    //                                     />
    //                                 </div>
    //                             </li>
    //                         )
    //                     })} */}
    //                 </ul>

    //             </>
    //         );
    //     }

    // }





//}
