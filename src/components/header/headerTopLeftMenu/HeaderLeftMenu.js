import React from 'react';
import './headerLeftMenu.scss';

class HeaderLeftMenu extends React.Component {

    render() {
        return (
            <div className="container-fluid">
                <div className="col-12 header-top-menu">
                    <ul className="d-inline-flex">
                        <li><a className="active">All </a></li>
                        <li><a>News</a></li>
                        <li><a>Features</a></li>
                        <li><a>VC Exclusives</a>  </li>
                        <li><a>Trailers</a> </li>
                        <li><a>Reviews</a> </li>
                        <li><a>Interviews</a> </li>
                        <li><a> Video </a></li>
                        <li><a>Photos</a></li>
                    </ul>
                </div>
            </div>
        );
    }
}


export default HeaderLeftMenu;