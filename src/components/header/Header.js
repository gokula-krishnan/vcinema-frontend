import './Header.scss';
import React from 'react';
import { connect } from 'react-redux';
import { Navbar, Nav, Button } from 'react-bootstrap';
import { selectHeader, selectFilter } from '../../actions';
import { Search, Avatar, Filter } from '../../svgIcons';
import PropTypes from 'prop-types';

const headerLink = (fixedNavlink, selectedHeader) => {
    console.log(fixedNavlink)
    if(fixedNavlink === "movies"){
        return (
            <>
                <Nav.Link href="#popular">Most Popular</Nav.Link>
                <Nav.Link eventKey="#soon" href="#soon">Upcoming Releases</Nav.Link>
                <Nav.Link eventKey="#awarded" href="#awarded">most awarded</Nav.Link>
                <Nav.Link eventKey="#full" href="#full">full movies</Nav.Link>
                <Nav.Link eventKey="#stars" href="#stars">my star's</Nav.Link>
            </>
        )
    }
    else if(fixedNavlink === "cc"){
        return (
            <>
                <Nav.Link href="#popular">Most Popular</Nav.Link>
                <Nav.Link eventKey="#awarded" href="#awarded">most awarded</Nav.Link>
                {/* <Nav.Link eventKey="#full" href="#full" >Compare</Nav.Link> */}
                <Nav.Link eventKey="#stars" href="#stars">my stars</Nav.Link>
            </>
        )
    }
    else if(fixedNavlink === "music"){
        return (
            <>
                <Nav.Link href="#popular">Most Popular</Nav.Link>
                <Nav.Link eventKey="#soon" href="#soon">New Releases</Nav.Link>
                <Nav.Link eventKey="#awarded" href="#awarded">Most Awarded</Nav.Link>
                <Nav.Link eventKey="#radio" href="#radio" >Radio</Nav.Link>
                <Nav.Link eventKey="#stars" href="#stars">My star's</Nav.Link>
            </>
        )
    }

    else if(fixedNavlink === "video"){
        return (
            <>
                <Nav.Link href="#popular">Most Popular</Nav.Link>
                <Nav.Link eventKey="#songs" href="#songs">Songs</Nav.Link>
                <Nav.Link eventKey="#events" href="#events">Events</Nav.Link>
                <Nav.Link eventKey="#behindscene" href="#behindscene" >Behind Scenes</Nav.Link>
                <Nav.Link eventKey="#social" href="#social">Social</Nav.Link>
                <Nav.Link eventKey="#shortfilms" href="#shortfilms">Short films</Nav.Link>
                <Nav.Link eventKey="#miscellaneous" href="#miscellaneous">Miscellaneous</Nav.Link>
                <Nav.Link eventKey="#stars" href="#stars">My Star's</Nav.Link>
            </>
        )
    }

    else if(fixedNavlink === "gallery"){
        return (
            <>
                <Nav.Link href="#popular">Popular</Nav.Link>
                <Nav.Link eventKey="#stills" href="#songs">Stills</Nav.Link>
                <Nav.Link eventKey="#posters" href="#posters">Posters</Nav.Link>
                <Nav.Link eventKey="#wallpapers" href="#wallpapers" >Wallpapers</Nav.Link>
                <Nav.Link eventKey="#events" href="#events">Events</Nav.Link>
                <Nav.Link eventKey="#behindscene" href="#behindscene">Behind Scenes</Nav.Link>
                <Nav.Link eventKey="#vintage" href="#vintage">Vintage</Nav.Link>
                <Nav.Link eventKey="#instagram" href="#instagram">Instagram</Nav.Link>
                <Nav.Link eventKey="#stars" href="#stars">My Star's</Nav.Link>
            </>
        )
    }

    else if(fixedNavlink === "awards") {
        console.log(selectedHeader)
        return (
            <>
                <Nav.Link href="#popular" className={(selectedHeader === "#popular") ? "active" : "inactive"}>All</Nav.Link>
                <Nav.Link eventKey="#filmfare" href="#filmfare" className={(selectedHeader === "#filmfare") ? "active" : null}>filmfare</Nav.Link>
                <Nav.Link eventKey="#nandi" href="#nandi" className={(selectedHeader === "#nandi") ? "active" : null}>nandi</Nav.Link>
                <Nav.Link eventKey="#siima" href="#siima" className={(selectedHeader === "#siima") ? "active" : null}>siima</Nav.Link>
                <Nav.Link eventKey="#iifa" href="#iifa" className={(selectedHeader === "#iifa") ? "active" : null}>iifa</Nav.Link>
                <Nav.Link eventKey="#national" href="#national" className={(selectedHeader === "#national") ? "active" : null}>national film awards</Nav.Link>
            </>
        )
    }
    else if (fixedNavlink === "feeds") {
        alert()
        return(
            <></>
        )
    }
    else{
        alert("null")
    }
}


const Header = ({selectHeader, selectFilter, activeLink, filterState, selectedHeader}) => {

    return (
        <>
            <Navbar variant="dark" className="pt-3">
                
                <Nav defaultActiveKey="#popular" onSelect={ selectedKey =>  (selectedHeader !== selectedKey) ? selectHeader(selectedKey) : null }>
                    {headerLink(activeLink, selectedHeader)}
                </Nav>


                { activeLink != 'awards'?  
                    <div className="col-2 text-left filter-btn">
                        <Button onClick={() => selectFilter(!filterState) } className={filterState ? "filter-active" : null }>
                            <Filter />
                        </Button>
                    </div> 
                : null }


                <div className="col-2 ml-auto text-right right-part">
                    <Search width="10%" fill="#fff"/>
                    <Avatar width="10%" fill="#fff"/>
                </div>

            </Navbar>
        </>
    )
}

Header.propTypes = {
    filterState: PropTypes.bool.isRequired,
    selectHeader: PropTypes.func.isRequired,
    selectFilter: PropTypes.func.isRequired,
    activeLink: PropTypes.string.isRequired,
    selectedHeader: PropTypes.string.isRequired,

  };
const mapStateToProps = state =>{
    console.log(state);
    return{
        selectedHeader: state.selectedHeader,
        filterState: state.selectedFilter,
    }
} 

export default connect(mapStateToProps, { selectHeader, selectFilter })(Header);