import React from 'react';
import {Eclipse} from '../../svgIcons';

const Spinner = props => {
    return(
        <div className="Spinner">
            <Eclipse/>
        </div>
    );
}

export default Spinner;