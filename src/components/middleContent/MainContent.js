import React from 'react';
import './MainContent.scss';
import GridHalf from './grid/GridHalf';
import GridFull from './grid/GridFull';
import Advertise from './grid/Advertise';

class MainContent extends React.Component {

    render() {
        return (
            <div className="main-wrapper">            
               <GridHalf></GridHalf>
               <GridFull></GridFull>
               <Advertise></Advertise>
               <GridHalf></GridHalf>
               <GridHalf></GridHalf>
               <Advertise></Advertise>
               <GridHalf></GridHalf>
               <GridFull></GridFull>
               <Advertise></Advertise>
               <GridHalf></GridHalf>
               <GridHalf></GridHalf>
               <Advertise></Advertise>
               <GridHalf></GridHalf>
               <GridFull></GridFull>
               <Advertise></Advertise>
               <GridHalf></GridHalf>
               <GridHalf></GridHalf>
               <Advertise></Advertise>
            </div>            
        );
    }
}

export default MainContent;