import React from 'react';
import './grid.scss';
import Advt01 from '../../../img/advt01.png';

// var sectionStyle = {
//     height:'100px',   
//     backgroundImage: `url(${Advt01})`,
//     backgroundPosition: 'center',
//     backgroundSize: 'cover',
//     backgroundRepeat: 'no-repeat',    
// };

class Advertise extends React.Component {

    render() {
        return (
            <div className="row avertise-sec mb-4 mb-md-2">
                <div className="col-12 col-sm-12 col-md-12">
                    <img src={Advt01} width="100%"/>
                </div>
            </div>
        );
    }
}
export default Advertise;