import React from 'react';
import './grid.scss';
import { InterviewRibbon } from '../../../svgIcons';
import {Ribbonred} from '../../../svgIcons';
import Interview01 from '../../../img/video5.jpg';

var sectionStyle = {
    height:'499px',   
    backgroundImage: `url(${Interview01})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
};

class GridFull extends React.Component {

    render() {
        return (
            <div className="row">
                <div className="col-sm-12 mb-3 mb-md-0">
                    <div className="feed-card feed-card_full">
                        <img src={Interview01} width="100%"/>
                        <div className="shadow"></div>
                        <div className="feed-card_text">
                            <span className="ribbon interview"></span>
                            <span className="source interview">Interview</span>
                            <h3 className="feed-tile">Allu arjun interview: “I was initially nervous to hold my son”</h3>
                            <span className="date">Wednesday, April 04, 2018 - 12:14</span>
                        </div>
                    </div>                                
                </div>
                {/* <div className="col-sm-12 feed-card feed-card_full">
                    <img src="http://localhost:3000/static/media/interview02.08be610c.png" width="100%"  alt=""/>
                    <div className="feed-card_text">
                        <span className="ribbon interview"></span>
                        <span className="source interview">Interview</span>
                        <h3 className="feed-tile">Allu arjun interview: “I was initially nervous to hold my son”</h3>
                        <span className="date">Wednesday, April 04, 2018 - 12:14</span>
                    </div>
                </div> */}
                {/* <div className="col-12 col-sm-12 col-md-12" style={sectionStyle}>
                    <section className="inner-text full-grid-inner-text">
                    <span className="ribbon"> <InterviewRibbon /></span>
                    <span><Ribbonred/></span>
                        <span className="source red">Interview</span>
                        <h3>Allu arjun interview: “I was initially nervous to hold my son”</h3>
                        <small className="date">Wednesday, April 04, 2018 - 12:14</small>
                    </section>
                </div> */}
         </div>
        );
    }
}
export default GridFull;