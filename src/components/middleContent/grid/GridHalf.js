import React from 'react';
import './grid.scss';
import { NewsRibbon, ReviewRibbon } from '../../../svgIcons';
import movieclip from '../../../img/movieclip01.png';
import movieclip02 from '../../../img/movieclip02.png';
import image1 from '../../../img/video6.jpg';
import image2 from '../../../img/video3.jpg';


class GridHalf extends React.Component {

    render() {
        return (
            <div className="row">
                <div className="col-sm-12 col-md-6 pr-md-0 mb-3 mb-md-0">
                    <div className="feed-card">
                        <img src={image1} width="100%"/>
                        <div className="shadow"></div>
                        <div className="feed-card_text">
                            <span className="ribbon news"></span>
                            <span className="source news">News</span>
                            <h3 className="feed-tile">'Rangasthalam' in 200 cr club?</h3>
                            <span className="date">Wednesday, April 04, 2018 - 12:14</span>
                        </div>
                    </div>                                
                </div>
                <div className="col-sm-12 col-md-6 feed-card pl-md-0 mb-3 mb-md-0">
                    <div className="feed-card">
                        <img src={image2} width="100%"/>
                        <div className="shadow"></div>
                        <div className="feed-card_text">
                            <span className="ribbon vc-exclusive"></span>
                            <span className="source vc-exclusive">Vcinema Exclusive</span>
                            <h3 className="feed-tile">Bharat ane nenu moview review</h3>
                            <span className="date">Wednesday, April 04, 2018 - 12:14</span>
                        </div>
                    </div>  
                </div>
            {/* <div className="col-12 col-sm-12 col-md-6" style={sectionStyle}>
                <section className="inner-text">
                   <span className="ribbon"> <NewsRibbon /></span>
                    <span className="source red">News</span>
                    <h3>'Rangasthalam' in 200 cr club?</h3>
                    <small className="date">Wednesday, April 04, 2018 - 12:14</small>
                </section>
            </div> */}

            
             {/* <div className="col-12 col-sm-12 col-md-6" style={reviewStyle}>
             <section className="inner-text">
                <span className="ribbon"> <ReviewRibbon /></span>
                 <span className="source aqua">V Cinema Exclusive</span>
                 <h3>'Rangasthalam' in 200 cr club?</h3>
                 <small className="date">Wednesday, April 04, 2018 - 12:14</small>
             </section>
         </div> */}
         </div>
         
        );
    }
}
export default GridHalf;