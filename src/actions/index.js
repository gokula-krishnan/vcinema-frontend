import base from '../api/base';
import middleware from '../api/middleware';

let mostawarded= null, musicAwards=null;


export const movieStatic = (url) => async dispatch => {
    const response = await base.get(url);
    dispatch({ type: "MOVIE_STATIC", payload: response.data });
}
////////////////////////////////////////////////////////
////////////////////////MOVIE API //////////////////////

export const fetchStreams = (str) => async dispatch => {
    const response = await base.get(`/movies?include=wallpapers,professions&azSearch=${str}`);
   // dispatch({ type: "FETCH_STREAMS", payload: response.data });
    //console.log(response);
    return response;
}

export const movieSearchFilter = (url) => async dispatch => {
    const response = await base.get(url);
    //console.log(response)
    return response;
    
}

export const movieAwardedFilter = (url) => async dispatch => {
    const response = await base.post(url, {q: mostawarded});
    return response;    
}

export const movieAllStream = (num) => async dispatch => {
    const response = await base.get(`/movies?include=wallpapers&page=${num}`);
    //dispatch({ type: "FETCH_MOVIE_ALL", payload: response });
    return response;
}

export const movieRelStream = (num) => async dispatch => {  
    const response = await base.get(`/movies/category/releases?include=wallpapers&page=${num}`);
    //console.log(response)
    //dispatch({ type: "FETCH_MOVIE_REL", payload: response });
    return response.data;
}

export const movieAwardPost = (res) => async dispatch => {
    const response = await base.post(`/movies/id/items?include=wallpapers`, {q: res});
    return response.data;
}

export const movieAwardStream = () => async dispatch => {
    const response = await base.get('/movies/category/most_awarded');
    mostawarded = response.data;
    return response.data;
}

export const movieFullStream = (num) => async dispatch => {
    const response = await base.get(`/movies/category/fullmovies?include=wallpapers&page=${num}`);
    //dispatch({ type: "FETCH_MOVIE_FULL", payload: response });
    return response.data;
}
// export const movieAwardStream = (num) => async dispatch => {
//     const response = await base.get(`/movies/category/most_awarded?include=wallpapers&page=${num}`);
//     dispatch({ type: "FETCH_MOVIE_AWARD", payload: response });
//     return response.data;
// }
//////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////cC API //////////////////////

export const ccAllStream = (num) => async dispatch => {
    const response = await base.get(`/artists?include=wallpapers&page=${num}`);
    //dispatch({ type: "FETCH_CC_ALL", payload: response });
    return response;
}

export const ccAwardStream = (num) => async dispatch => {
    const response = await base.get(`/artists/category/most_awarded?include=wallpapers&page=${num}`);
   // dispatch({ type: "FETCH_CC_AWARD", payload: response });
    return response.data;
}

export const ccSearchFilter = (url) => async dispatch => {
    console.log(url);
    const response = await base.get(url);
    //console.log(response.data.meta)
    return response;
    
}
export const ccOptions = () => async dispatch => {
    const response = await base.get('/options?option=artistProfessions&key=true');
    return response;
}
//////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////MUSIC API //////////////////////
export const musicSearchFilter = (url) => async dispatch => {
    const response = await base.get(url);
    return response;    
}

export const musicAwardedFilter = (url) => async dispatch => {
    const response = await base.post(url, {q: musicAwards});
    return response;    
}
export const musicAllStream = (num) => async dispatch => {
    const response = await base.get(`/albums?include=bannerImages&page=${num}`);
    return response;
}

export const musicRelStream = (num) => async dispatch => {  
    const response = await base.get(`/albums/category/releases?include=bannerImages&page=${num}`);
    return response.data;
}

export const musicAwardStream = () => async dispatch => {
    const response = await base.get(`/albums/category/most_awarded?include=bannerImages`);
    musicAwards = response.data;
    return response.data;
}

export const musicAwardPost = (res) => async dispatch => {
    const response = await base.post(`/albums/id/items?include=bannerImages`, {q: res});    
    return response.data;
}

/*  Music Middleware APIs */

export const albumFilter = (res) => async dispatch => {

    const response = await base.post(`/albums/id/items?include=bannerImages`, {q: res});    
    return response.data;
}







//////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////VIDEO API //////////////////////

export const videoAllStream = (num) => async dispatch => {
   
    const response = await base.get(`/videos?page=${num}`);
   // dispatch({ type: "FETCH_VIDEO_ALL", payload: response });
   console.log(response)
    return response;
}

export const videoSongs = (num) => async dispatch => {
    const response = await base.get(`/videos?section=songs&page=${num}`);
   // dispatch({ type: "FETCH_VIDEO_ALL", payload: response });
   console.log(response);
    return response;
}
export const videoEvents = (num) => async dispatch => {
    const response = await base.get(`/videos?section=events&page=${num}`);
   // dispatch({ type: "FETCH_VIDEO_ALL", payload: response });
    return response;
}

export const videoBehindScenes = (num) => async dispatch => {
    const response = await base.get(`/videos?section=behind scenes&page=${num}`);
   // dispatch({ type: "FETCH_VIDEO_ALL", payload: response });
    return response;
}

export const videoSocial = (num) => async dispatch => {
    const response = await base.get(`/videos?section=social&page=${num}`);
   // dispatch({ type: "FETCH_VIDEO_ALL", payload: response });
    return response;
}

export const videoShortFilms = (num) => async dispatch => {
    const response = await base.get(`/videos?fe=SHORT_FILM&page=${num}`);
   // dispatch({ type: "FETCH_VIDEO_ALL", payload: response });
    return response;
}

export const videoMiscellaneous = (num) => async dispatch => {
    const response = await base.get(`/videos?section=others&page=${num}`);
   // dispatch({ type: "FETCH_VIDEO_ALL", payload: response });
    return response;
}


export const videoSearchFilter = (url) => async dispatch => {
    console.log(url);

    const response = await middleware.get(url);
    //console.log(response)
    return response;
    
    
}

export const videoDetails = (type, id, section) => async dispatch => {
    const response = await base.get(`/videos?imgid=${id}&section=${section}&type=${type}&include=imgTypeDetails`);
    console.log(response)
    return response;
}
//////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////VIDEO API //////////////////////

export const awardAllStream = () => async dispatch => {
    const response = await base.get(`/award-categories?include=awards`);
    //dispatch({ type: "FETCH_VIDEO_ALL", payload: response });
    return response;
}

export const awardIndividual = (uuid) => async () => {
    const response = await base.get(`/awards?byCategory=${uuid}&include=cover,primaryImageBanners,photos,videos`);
    return response;
}

//////////////////////////////////////////////////
//////////////////////////////////////////////////



//////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////GALLERY API //////////////////////



export const galleryAllStream = (num) => async dispatch => {
    const response = await base.get(`/photos?include=imgTypeDetails&page=${num}`);
    console.log(response)
    return response;
}

export const galleryPosters = (num) => async dispatch => {
    const response = await base.get(`/photos?include=imgTypeDetails&section=posters&page=${num}`);
    console.log(response)
    return response;
}

export const galleryWallpapers = (num) => async dispatch => {
    const response = await base.get(`/photos?include=imgTypeDetails&section=wallpapers&page=${num}`);
    console.log(response)
    return response;
}

export const galleryEvents = (num) => async dispatch => {
    const response = await base.get(`/photos?include=imgTypeDetails&section=events&page=${num}`);
    console.log(response)
    return response;
}

export const galleryBehindScenes = (num) => async dispatch => {
    const response = await base.get(`/photos?include=imgTypeDetails&section=behind scenes&page=${num}`);
    console.log(response)
    return response;
}

export const galleryVintage = (num) => async dispatch => {
    const response = await base.get(`/photos?include=imgTypeDetails&section=vintage&page=${num}`);
    console.log(response)
    return response;
}

export const galleryStills = (num) => async dispatch => {
    const response = await base.get(`/photos?include=imgTypeDetails&section=stills&page=${num}`);
    console.log(response)
    return response;
}
//details page
export const galleryDetails = (type, id, section) => async dispatch => {
    const response = await base.get(`/photos?imgid=${id}&section=${section}&type=${type}&include=imgTypeDetails`);
    console.log(response)
    return response;
}

//////////////////////////////////////////////////
//////////////////////////////////////////////////



export const selectFixedNavLink = navlink => {
    return{
        type: 'NAVLINK_SELECTED',
        payload: navlink
    }
}


export const selectHeader = header => {
    return{
        type: 'HEADER_SELECTED',
        payload: header
    }
}

export const showFilter = filter => {

    console.log(filter)
    return{
        type: 'SHOW_FILTER',
        payload: filter
    }
}


export const selectFilter = filter => {
    console.log(filter)
    return{
        type: 'FILTER_SELECTED',
        payload: filter
    }
}

export const windowWidth = width => {
    return{
        type: 'WINDOW_WIDTH',
        payload: width
    }
}

export const NumberOfItems = items => {
    return{
        type: 'NUMBER_OF_ITEMS',
        payload: items
    }
}

export const selectedImgLists = imgLists => {
    return{
        type: 'SELECT_IMG_LISTS',
        payload: imgLists
    }
}