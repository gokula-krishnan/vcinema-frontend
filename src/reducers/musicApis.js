

export const fetchMusicAll = ( album = null, action) => {
    if(action.type === 'FETCH_MUSIC_ALL') {
        //console.log(action.payload.data)
        return action.payload.data;
    }
    return album;
}

export const fetchMusicAward = ( album = null, action) => {
    if(action.type === 'FETCH_MUSIC_AWARD') {
        return action.payload.data;
    }
    return album;
}

export const fetchMusicRel = ( album = null, action) => {
    if(action.type === 'FETCH_MUSIC_REL') {
        //console.log(action.payload.data)
        return action.payload.data;
    }
    return album;
}