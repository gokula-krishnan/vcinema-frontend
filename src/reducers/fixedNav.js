


export const selectFixedNavlinkReducer = ( selectedNavlink = '/movies', action) => {
    if(action.type === 'NAVLINK_SELECTED') {
        return action.payload;
    }
    return selectedNavlink;
}
