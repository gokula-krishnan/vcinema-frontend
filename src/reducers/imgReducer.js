
export const movieImgReducer = () => {
    return [
        { id: "110", castid: "1110", cast: ["Chiranjeevi", "Allu Arjun"], moviePoster: "/static/media/movie12.c2e713a0.jpg", title: "ABCD", release: '24 March 2000', awards: "5", rating: "2.1", movieLink: "yes" , genre: "action", certificate: "U"},
        { id: "111", castid: "1111", cast: ["Nikhil", "Chiranjeevi"], moviePoster: "/static/media/movie15.ea9f230c.jpg", title: "Saho", release: '29 January 2019', awards: "12", rating: "3.3", movieLink: "yes" , genre: "romance", certificate: "A"},
        { id: "112", castid: "1112", cast: ["Jr. NTR"], moviePoster: "/static/media/movie10.e82dbc31.jpg", title: "Bahubali", release: '2 December 2020', awards: "0", rating: "0", movieLink: "no" , genre: "comedy", certificate: "All"},
        { id: "113", castid: "1113", cast: ["Samantha"], moviePoster: "/static/media/movie11.d5c9cccf.jpg", title: "Arya", release: '5 March 2010', awards: "6", rating: "4.8", movieLink: "yes" , genre: "drama", certificate: "U/A"},
        { id: "114", castid: "1114", cast: ["brahmanandam", "Allu Arjun"], moviePoster: "/static/media/movie12.c2e713a0.jpg", title: "Arya 2", release: '24 April 2023', awards: "0", rating: "0", movieLink: "no" , genre: "romance", certificate: "S"},
        { id: "115", castid: "1115", cast: ["Ram Charan", "Chiranjeevi"], moviePoster: "/static/media/movie13.43976528.jpg", title: "Arundhati", release: '5 March 2011', awards: "3", rating: "1.0", movieLink: "no" , genre: "comedy", certificate: "A"},
        { id: "116", castid: "1116", cast: ["Allu Arjun"], moviePoster: "/static/media/movie14.48502054.jpg", title: "Bahubali 2", release: '7 February 2012', awards: "4", rating: "1.5", movieLink: "yes" , genre: "drama", certificate: "U/A"},
        { id: "117", castid: "1117", cast: ["Naga Chaithanya"], moviePoster: "/static/media/movie15.ea9f230c.jpg", title: "Arjun Reddy", release: '16 June 2018', awards: "0", rating: "4.5", movieLink: "yes" , genre: "action", certificate: "All"},
        { id: "118", castid: "1118", cast: ["Prabhas"], moviePoster: "/static/media/movie16.540c9e70.jpg", title: "Ega", release: '2 October 2001', awards: "0", rating: "2.8", movieLink: "no" , genre: "drama", certificate: "U"},
        { id: "119", castid: "1119", cast: ["Rana", "Chiranjeevi"], moviePoster: "/static/media/movie7.b06de8e8.jpg", title: "Enthiran 2", release: '24 November 2002', awards: "6", rating: "3.2", movieLink: "no" , genre: "action", certificate: "A"},
        { id: "120", castid: "1120", cast: ["Anushka", "Allu Arjun"], moviePoster: "/static/media/movie9.5fa5cccf.jpg", title: "Aparichitudu", release: '24 March 2005', awards: "3", rating: "3.1", movieLink: "no" , genre: "action", certificate: "U"},
        { id: "121", castid: "1120", cast: ["Kajol", "Arya"], moviePoster: "/static/media/movie11.d5c9cccf.jpg", title: "Brundavanam", release: '24 July 2008', awards: "8", rating: "3.9", movieLink: "yes" , genre: "comedy", certificate: "All"},
        { id: "122", castid: "1122", cast: ["Rakul Preeth"], moviePoster: "/static/media/movie5.bdfa8cac.jpg", title: "Vinaya Vidheya Rama",  release: '24 March 2016', awards: "1", rating: "4.0", movieLink: "no" , genre: "drama", certificate: "U/A"},
        { id: "123", castid: "1123", cast: ["Sonu Sood", "Arya"], moviePoster: "/static/media/movie6.c855c241.jpg", title: "Darling", release: '24 March 2001', awards: "9", rating: "3.1", movieLink: "yes" , genre: "action", certificate: "U"},
        { id: "124", castid: "1124", cast: ["Anupama", "Arya"], moviePoster: "/static/media/movie7.b06de8e8.jpg", title: "Mirchi", release: '2 September 2008', awards: "10", rating: "2.7", movieLink: "no" , genre: "drama", certificate: "All"},
        { id: "125", castid: "1125", cast: ["Ramya Krishna"], moviePoster: "/static/media/movie8.950bbc23.jpg", title: "Rudrama Devi", release: '24 March 2017', awards: "15", rating: "1.8", movieLink: "no" , genre: "comedy", certificate: "U"},
        { id: "126", castid: "1126", cast: ["Arya", "Allu Arjun"], moviePoster: "/static/media/movie9.5fa5cccf.jpg", title: "Karthikeya", release: '24 June 2018', awards: "6", rating: "4.5", movieLink: "yes" , genre: "action", certificate: "U/A"},
        { id: "127", castid: "1127", cast: ["Shruthi Hasan"], moviePoster: "/static/media/movie14.48502054.jpg", title: "Srimanthudu", release: '24 November 2019', awards: "0", rating: "0", movieLink: "no" , genre: "action", certificate: "S"},
        { id: "128", castid: "1128", cast: ["Kamal Hasan"], moviePoster: "/static/media/movie12.c2e713a0.jpg", title: "Race gurram", release: '24 July 2020', awards: "0", rating: "0", movieLink: "no" , genre: "action", certificate: "S"},
        { id: "129", castid: "1129", cast: ["Nagarjuna", "Allu Arjun"], moviePoster: "/static/media/movie13.43976528.jpg", title: "Drushyam", release: '24 December 2022', awards: "0", rating: "0", movieLink: "no", genre: "action" , certificate: "S"}
    ];
}


export const castImgReducer = () => {
    return [
        { id: "1110", moviePoster: "/static/media/actor1.57be63ee.jpg", title: "Chiranjeevi", awards: "5", ranking: "2", movieLink: "yes", proffession: "Hero", age: "28", year: "1980", zodiac: "Aries", industries: "Kannda" , gender: "X"},
        { id: "1111", moviePoster: "/static/media/actor2.bba800ef.jpg", title: "Nikhil", awards: "4", ranking: "3", movieLink: "yes", proffession: "Hero", age: "40", year: "1990", zodiac: "Gemini", industries: "Telugu" , gender: "F"},
        { id: "1112", moviePoster: "/static/media/actor3.b2f190e2.jpg", title: "Jr. NTR", awards: "3", ranking: "1", movieLink: "no", proffession: "Heroine", age: "32", year: "1988", zodiac: "Tourus", industries: "Hindi" , gender: "M"},
        { id: "1113", moviePoster: "/static/media/actor4.72df0ebe.jpg", title: "Samantha", awards: "2", ranking: "4", movieLink: "yes", proffession: "Director", age: "35", year: "1970", zodiac: "Cancer", industries: "Tamil" , gender: "F"},
        { id: "1114", moviePoster: "/static/media/actor5.024f2944.jpg", title: "brahmanandam", awards: "1", ranking: "7", movieLink: "no", proffession: "Director", age: "38", year: "1988", zodiac: "Aries", industries: "Hindi" , gender: "M"},
        { id: "1115", moviePoster: "/static/media/actor6.6602c12f.jpg", title: "Ram Charan",  awards: "2", ranking: "8", movieLink: "no", proffession: "Heroine", age: "32", year: "1996", zodiac: "Gemini", industries: "Kannda" , gender: "F"},
        { id: "1116", moviePoster: "/static/media/actor7.b5160359.jpg", title: "Allu Arjun",  awards: "5", ranking: "6", movieLink: "yes", proffession: "Music Director", age: "40", year: "1999", zodiac: "Gemini", industries: "Kannda" , gender: "F"},
        { id: "1117", moviePoster: "/static/media/actor1.57be63ee.jpg", title: "Naga Chaithanya", awards: "4", ranking: "5", movieLink: "yes", proffession: "Hero", age: "42", year: "1994", zodiac: "Libra", industries: "Hindi" , gender: "F"},
        { id: "1118", moviePoster: "/static/media/actor2.bba800ef.jpg", title: "Prabhas", awards: "3", ranking: "8", movieLink: "no", proffession: "Director", age: "52", year: "1984", zodiac: "Tourus", industries: "Telugu" , gender: "M"},
        { id: "1119", moviePoster: "/static/media/actor3.b2f190e2.jpg", title: "Rana", awards: "1", ranking: "10", movieLink: "no", proffession: "Villain", age: "60", year: "1980", zodiac: "Cancer", industries: "Kannda" , gender: "F"},
        { id: "1120", moviePoster: "/static/media/actor4.72df0ebe.jpg", title: "Anushka", awards: "0", ranking: "31", movieLink: "no", proffession: "Heroine", age: "66", year: "1982", zodiac: "Aries", industries: "Telugu" , gender: "F"},
        { id: "1121", moviePoster: "/static/media/actor5.024f2944.jpg", title: "Kajol", awards: "0", ranking: "19", movieLink: "yes", proffession: "Music Director", age: "56", year: "1979", zodiac: "Gemini", industries: "Tamil" , gender: "M"},
        { id: "1122", moviePoster: "/static/media/actor7.b5160359.jpg", title: "Rakul Preeth", awards: "2", ranking: "14", movieLink: "no", proffession: "Hero", age: "46", year: "1974", zodiac: "Gemini", industries: "Tamil" , gender: "F"},
        { id: "1123", moviePoster: "/static/media/actor6.6602c12f.jpg", title: "Sonu Sood", awards: "4", ranking: "11", movieLink: "yes", proffession: "Villain", age: "55", year: "1972", zodiac: "Tourus", industries: "Kannda" , gender: "M"},
        { id: "1124", moviePoster: "/static/media/actor1.57be63ee.jpg", title: "Anupama", awards: "3", ranking: "17", movieLink: "no", proffession: "Heroine", age: "50", year: "1977", zodiac: "Cancer", industries: "Malyalam", gender: "M" },
        { id: "1125", moviePoster: "/static/media/actor2.bba800ef.jpg", title: "Ramya Krishna", awards: "3", ranking: "18", movieLink: "no", proffession: "Villain", age: "40", year: "1978", zodiac: "Gemini", industries: "English" , gender: "F"},
        { id: "1126", moviePoster: "/static/media/actor3.b2f190e2.jpg", title: "Arya", awards: "5", ranking: "15", movieLink: "yes", proffession: "Hero", age: "30", year: "1983", zodiac: "Gemini", industries: "English" , gender: "X"},
        { id: "1127", moviePoster: "/static/media/actor4.72df0ebe.jpg", title: "Shruthi Hasan", awards: "5", ranking: "1", movieLink: "no", proffession: "Hero", age: "43", year: "1986", zodiac: "Gemini", industries: "English" , gender: "X"},
        { id: "1128", moviePoster: "/static/media/actor5.024f2944.jpg", title: "Kamal Hasan", awards: "1", ranking: "2", movieLink: "no", proffession: "Villain", age: "51", year: "1988", zodiac: "Libra", industries: "Malyalam" , gender: "F"},
        { id: "1129", moviePoster: "/static/media/actor6.6602c12f.jpg", title: "Nagarjuna", awards: "2", ranking: "4", movieLink: "no", proffession: "Villain", age: "34", year: "1989", zodiac: "Libra" , industries: "Malyalam", gender: "M"}
    ];
}


export const musicImgReducer = () => {
    return [
        { id: "110", moviePoster: "/static/media/music4.4266430c.jpg", awards: "1", rating: "2", title: "ABCD", release: '24 March 2019', cast: ["Chiranjeevi", "Samantha", "Arya"] },
        { id: "111", moviePoster: "/static/media/music2.f88692a8.jpg", awards: "5", rating: "2", title: "Arya", release: '24 March 2001', cast: ["Allu Arjun", "Samantha"] },
        { id: "112", moviePoster: "/static/media/music3.8d231c3d.jpg", awards: "4", rating: "4", title: "Drushyam", release: '24 March 2002', cast: ["Allu Arjun", "Arya"] },
        { id: "113", moviePoster: "/static/media/music4.4266430c.jpg", awards: "1", rating: "3", title: "Arya 2", release: '24 March 2004', cast: ["Arya", "Samantha"] },
        { id: "114", moviePoster: "/static/media/music4.4266430c.jpg", awards: "2", rating: "2.5", title: "Bahubali", release: '24 March 2020', cast: ["Chiranjeevi", "Prabhas"] },
        { id: "115", moviePoster: "/static/media/music2.f88692a8.jpg", awards: "5", rating: "2", title: "Rudramadevi", release: '24 March 1995', cast: ["Chiranjeevi"] },
        { id: "116", moviePoster: "/static/media/music3.8d231c3d.jpg", awards: "2", rating: "4", title: "Arundhathi", release: '24 March 1999', cast: ["Chiranjeevi", "Prabhas"] },
        { id: "117", moviePoster: "/static/media/music4.4266430c.jpg", awards: "3", rating: "2", title: "Mirchi", release: '4 January 2019', cast: ["Chiranjeevi", "Samantha"] },
        { id: "118", moviePoster: "/static/media/music4.4266430c.jpg", awards: "1", rating: "3", title: "Amruthvarshini", release: '2 April 2019', cast: ["Chiranjeevi", "Prabhas"] },
        { id: "119", moviePoster: "/static/media/music2.f88692a8.jpg", awards: "5", rating: "2", title: "Galipata", release: '24 February 2019', cast: ["Chiranjeevi", "Arya"] },
        { id: "120", moviePoster: "/static/media/music3.8d231c3d.jpg", awards: "2", rating: "4", title: "Pancharangi", release: '24 March 2000', cast: ["Arya"] },
        { id: "121", moviePoster: "/static/media/music4.4266430c.jpg", awards: "3", rating: "4", title: "Mungaaru male", release: '24 March 2018', cast: ["Chiranjeevi", "Arya"] },
        { id: "121", moviePoster: "/static/media/music4.4266430c.jpg", awards: "2", rating: "3", title: "One Direction", release: '24 March 2004', cast: ["Arya", "Samantha"] },
        { id: "122", moviePoster: "/static/media/music2.f88692a8.jpg", awards: "5", rating: "2", title: "BTS", release: '24 March 2006', cast: ["Allu Arjun"] },
        { id: "123", moviePoster: "/static/media/music3.8d231c3d.jpg", awards: "4", rating: "2", title: "Black Pink", release: '24 March 2010', cast: ["Chiranjeevi", "Samantha"] },
        { id: "124", moviePoster: "/static/media/music4.4266430c.jpg", awards: "3", rating: "3", title: "Backstreet Boys", release: '24 March 2012', cast: ["Allu Arjun"] },
        { id: "125", moviePoster: "/static/media/music4.4266430c.jpg", awards: "1", rating: "2", title: "Jonas Brothers", release: '24 March 2015', cast: ["Arya", "Prabhas"] },
        { id: "126", moviePoster: "/static/media/music2.f88692a8.jpg", awards: "2", rating: "2", title: "Imagine Dragons", release: '24 March 2014', cast: ["Arya"] },
        { id: "127", moviePoster: "/static/media/music3.8d231c3d.jpg", awards: "1", rating: "2", title: "Twice", release: '24 March 1992', cast: ["Chiranjeevi"] },
        { id: "128", moviePoster: "/static/media/music4.4266430c.jpg", awards: "5", rating: "2.5", title: "Kung fu panda", release: '24 March 1993', cast: ["Allu Arjun", "Samantha"] },
        { id: "129", moviePoster: "/static/media/music4.4266430c.jpg", awards: "2", rating: "2.5", title: "Captain Marvel", release: '24 March 1991', cast: ["Chiranjeevi", "Arya"] },
        { id: "130", moviePoster: "/static/media/music4.4266430c.jpg", awards: "1", rating: "2.5", title: "The Gifted", release: '1 March 2019', cast: ["Chiranjeevi", "Prabhas"] },
    ]
}


export const videoImgReducer = () => {
    return [
        { id: "110", moviePoster: "/static/media/video1.2f5d8176.jpg", title: "making of bahubali", duration: "5:45", genre: "songs" , release: '1 March 2019'},
        { id: "111", moviePoster: "/static/media/video2.a359ba3f.jpg", title: "temper One more time", duration: "4:45", genre: "events" , release: '1 March 2000'},
        { id: "112", moviePoster: "/static/media/video3.cf3818a6.jpg", title: "the late comers", duration: "3:45", genre: "behind scenes", release: '1 March 1995'},
        { id: "113", moviePoster: "/static/media/video4.52a6950b.jpg", title: "Allu arjun's engagement", duration: "2:45", genre: "short films" , release: '1 March 1994'},
        { id: "114", moviePoster: "/static/media/video5.295ff238.jpg", title: "temper two more time", duration: "1:45", genre: "social", release: '1 March 2001'},
        { id: "115", moviePoster: "/static/media/video6.753dde6e.jpg", title: "making of arya",  duration: "2:45", genre: "behind scenes", release: '1 March 2002'},
        { id: "116", moviePoster: "/static/media/video7.53207c11.jpg", title: "the late night show",  duration: "5:45", genre: "songs" , release: '1 March 2005'},
        { id: "117", moviePoster: "/static/media/video1.2f5d8176.jpg", title: "Naga Chaithanya", duration: "4:45", genre: "social" , release: '1 March 2011'},
        { id: "118", moviePoster: "/static/media/video2.a359ba3f.jpg", title: "the late show", duration: "3:45", genre: "behind scenes", release: '1 March 2015'},
        { id: "119", moviePoster: "/static/media/video3.cf3818a6.jpg", title: "temper three more time", duration: "1:45", genre: "events", release: '1 March 2018'},
        { id: "120", moviePoster: "/static/media/video4.52a6950b.jpg", title: "making of rudramadevi", duration: "0:45", genre: "behind scenes", release: '1 March 1990'},
        { id: "121", moviePoster: "/static/media/video5.295ff238.jpg", title: "the late night", duration: "0:45", genre: "miscellaneous" , release: '1 March 1950'},
        { id: "122", moviePoster: "/static/media/video7.53207c11.jpg", title: "Allu arjun's marriage", duration: "2:45", genre: "social", release: '12 March 1980'},
        { id: "123", moviePoster: "/static/media/video6.753dde6e.jpg", title: "making of bahubali2", duration: "4:45", genre: "short films" , release: '1 March 1985'},
        { id: "124", moviePoster: "/static/media/video1.2f5d8176.jpg", title: "jr.ntr's engagement", duration: "3:45", genre: "events", release: '1 March 2020'},
        { id: "125", moviePoster: "/static/media/video2.a359ba3f.jpg", title: "making of endgame", duration: "3:45", genre: "behind scenes", release: '1 March 2016'},
        { id: "126", moviePoster: "/static/media/video3.cf3818a6.jpg", title: "temper four more time", duration: "5:45", genre: "short films" , release: '1 March 2014'},
        { id: "127", moviePoster: "/static/media/video4.52a6950b.jpg", title: "making of Avengers", duration: "5:45", genre: "miscellaneous", release: '1 March 2010'},
        { id: "128", moviePoster: "/static/media/video5.295ff238.jpg", title: "making of Iron man", duration: "1:45", genre: "songs", release: '1 March 2009'},
        { id: "129", moviePoster: "/static/media/video6.753dde6e.jpg", title: "temper five more time", duration: "2:45", genre: "social", release: '1 March 1996'}
    ];
}



export const awardsImgReducer = () => {
    return [
        { uuid: "511", name: "filmfare awards", year: "2011", series: "60"},
        { uuid: "512", name: "filmfare awards", year: "2012", series: "61"},
        { uuid: "513", name: "filmfare awards", year: "2013", series: "62"},
        { uuid: "514", name: "filmfare awards", year: "2014", series: "63"},
        { uuid: "515", name: "filmfare awards", year: "2015", series: "64"},
        { uuid: "516", name: "filmfare awards", year: "2016", series: "65"},
        { uuid: "517", name: "filmfare awards", year: "2017", series: "66"},
        { uuid: "518", name: "filmfare awards", year: "2018", series: "67"},
        { uuid: "519", name: "filmfare awards", year: "2019", series: "68"},
        { uuid: "520", name: "filmfare awards", year: "2010", series: "59"},
        { uuid: "521", name: "nandi awards", year: "2010"},
        { uuid: "522", name: "nandi awards", year: "2011"},
        { uuid: "523", name: "nandi awards", year: "2012"},
        { uuid: "524", name: "nandi awards", year: "2013"},
        { uuid: "525", name: "nandi awards", year: "2014"},
        { uuid: "526", name: "nandi awards", year: "2015"},
        { uuid: "527", name: "nandi awards", year: "2016"},
        { uuid: "528", name: "nandi awards", year: "2017"},
        { uuid: "529", name: "nandi awards", year: "2018"},
        { uuid: "530", name: "nandi awards", year: "2019"},
        { uuid: "531", name: "south Indian international movie awards", year: "2010", series: "1"},
        { uuid: "532", name: "south Indian international movie awards", year: "2011", series: "2"},
        { uuid: "533", name: "south Indian international movie awards", year: "2012", series: "3"},
        { uuid: "534", name: "south Indian international movie awards", year: "2013", series: "4"},
        { uuid: "535", name: "south Indian international movie awards", year: "2014", series: "5"},
        { uuid: "536", name: "south Indian international movie awards", year: "2015", series: "6"},
        { uuid: "537", name: "south Indian international movie awards", year: "2016", series: "7"},
        { uuid: "538", name: "south Indian international movie awards", year: "2017", series: "8"},
        { uuid: "539", name: "south Indian international movie awards", year: "2018", series: "9"},
        { uuid: "540", name: "south Indian international movie awards", year: "2019", series: "10"},
        { uuid: "541", name: "iifa", year: "2010"},
        { uuid: "542", name: "iifa", year: "2011"},
        { uuid: "543", name: "iifa", year: "2012"},
        { uuid: "544", name: "iifa", year: "2013"},
        { uuid: "545", name: "iifa", year: "2014"},
        { uuid: "546", name: "iifa", year: "2015"},
        { uuid: "547", name: "iifa", year: "2016"},
        { uuid: "548", name: "iifa", year: "2017"},
        { uuid: "549", name: "iifa", year: "2018"},
        { uuid: "550", name: "iifa", year: "2019"},
        { uuid: "551", name: "national film awards", year: "2010"},
        { uuid: "552", name: "national film awards", year: "2011"},
        { uuid: "553", name: "national film awards", year: "2012"},
        { uuid: "554", name: "national film awards", year: "2013"},
        { uuid: "555", name: "national film awards", year: "2014"},
        { uuid: "556", name: "national film awards", year: "2015"},
        { uuid: "557", name: "national film awards", year: "2016"},
        { uuid: "558", name: "national film awards", year: "2017"},
        { uuid: "559", name: "national film awards", year: "2018"},
        { uuid: "560", name: "national film awards", year: "2019"},
    ];    
}
