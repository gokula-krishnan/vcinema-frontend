//import _ from 'lodash';

export const fetchStreamReducer = (state= null, action) => {
    switch (action.type){
        case 'FETCH_STREAMS':
        //console.log(action.payload)
            return action.payload.data;//{ ...state, ..._.mapKeys(action.payload, 'id') };        
        default: 
            return state;
    }
}

export const movieStaticReducer = (state= null, action) => {
    switch (action.type){
        case 'MOVIE_STATIC':
            return action.payload.movieGenres;//{ ...state, ..._.mapKeys(action.payload, 'id') };        
        default: 
            return state;
    }
}

// export const fetchSrtistStreamReducer = (state= null, action) => {
//     switch (action.type){
//         case 'FETCH_ARTIST_STREAMS':
//         //console.log(action.payload)
//             return action.payload.data;//{ ...state, ..._.mapKeys(action.payload, 'id') };        
//         default: 
//             return state;
//     }
// }
export const windowWidthReducer = ( windowWidth = null, action) => {
    if(action.type === 'WINDOW_WIDTH') {
        return action.payload;
    }
    return windowWidth;
}

export const NumberOfItemsReducer = ( items = null, action) => {
    if(action.type === 'NUMBER_OF_ITEMS') {
        return action.payload;
    }
    return items;
}

export const imgListsReducer = ( imglist = null, action ) => {
    if(action.type === 'SELECT_IMG_LISTS'){
        return action.payload;
    }
    return imglist;
}

// export const fetchStreamReducer = ( list = null, action) => {
//     if(action.type === 'FETCH_STREAMS'){
//         return action.apyload;
//     }
//     return list;
// }