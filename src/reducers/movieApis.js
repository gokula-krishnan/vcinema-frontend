
export const fetchMovieAll = ( movie = null, action) => {
    if(action.type === 'FETCH_MOVIE_ALL') {
        return action.payload.data;
    }
    return movie;
}

export const fetchMovieRel = ( movie = null, action) => {
    if(action.type === 'FETCH_MOVIE_REL') {
        return action.payload.data;
    }
    return movie;
}

export const fetchMovieAward = ( movie = null, action) => {
    if(action.type === 'FETCH_MOVIE_AWARD') {
        return action.payload.data;
    }
    return movie;
}

export const fetchMovieFull = ( movie = null, action) => {
    if(action.type === 'FETCH_MOVIE_FULL') {
        return action.payload.data;
    }
    return movie;
}