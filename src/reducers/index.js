import { combineReducers } from 'redux';
import { selectedHeaderReducer, selectedFilterReducer } from './header';
import { selectFixedNavlinkReducer } from './fixedNav';
import { movieImgReducer, castImgReducer, musicImgReducer, videoImgReducer, awardsImgReducer } from './imgReducer';
import { windowWidthReducer, NumberOfItemsReducer, imgListsReducer, fetchStreamReducer, movieStaticReducer } from './items';
//import { fetchMovieAll, fetchMovieRel, fetchMovieAward, fetchMovieFull } from './movieApis';
import {fetchCcAll, fetchCcAward} from './ccApis';
import {fetchMusicAll, fetchMusicAward, fetchMusicRel} from './musicApis';
import { fetchVideoAll } from './videoApis';
//import { castImgReducer } from './castImgReducer'

export default combineReducers({
    selectedHeader: selectedHeaderReducer,
    selectedFilter: selectedFilterReducer,
    selectedFixedNavlink: selectFixedNavlinkReducer,

    width: windowWidthReducer,
    items: NumberOfItemsReducer,

   // imgdetails: movieImgReducer,   
    //castImg: castImgReducer,
   // musicImg: musicImgReducer,
    //videoImg: videoImgReducer,
    //imgLists: imgListsReducer, 
    //awardsImg: awardsImgReducer,
    //fetchstream: fetchStreamReducer,
    //movieStaticCont: movieStaticReducer,

   // movieAll : fetchMovieAll,
   // movieRel: fetchMovieRel,
   // movieAward: fetchMovieAward,
   // movieFull: fetchMovieFull,

    //CCall: fetchCcAll,
    //ccAward: fetchCcAward,

    //musicAll: fetchMusicAll,
   // musicAward: fetchMusicAward,
    //musicRel: fetchMusicRel,

   // videoAll: fetchVideoAll
});