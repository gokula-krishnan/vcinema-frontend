export const selectedHeaderReducer = ( selectedHeader = '#popular', action) => {
    if(action.type === 'HEADER_SELECTED') {
        return action.payload;
    }
    return selectedHeader;

}

export const selectedFilterReducer = ( selectedFilter = false, action) => {
    if(action.type === 'FILTER_SELECTED') {
        return action.payload;
    }
    return selectedFilter;

}
