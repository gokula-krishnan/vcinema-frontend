
export const fetchCcAll = ( cc = null, action) => {
    if(action.type === 'FETCH_CC_ALL') {
        return action.payload.data;
    }
    return cc;
}

export const fetchCcAward = ( cc = null, action) => {
    if(action.type === 'FETCH_CC_AWARD') {
        return action.payload.data;
    }
    return cc;
}